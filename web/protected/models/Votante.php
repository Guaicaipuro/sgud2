<?php

/**
 * This is the model class for table "gpersonal.votante".
 *
 * The followings are the available columns in table 'gpersonal.votante':
 * @property integer $id
 * @property integer $cedula
 * @property string $direccion_residencia
 * @property string $doble_nacionalidad
 * @property string $email
 * @property string $estado_civil
 * @property string $fecha_nacimiento
 * @property string $nacionalidad
 * @property string $primer_apellido
 * @property string $primer_nombre
 * @property string $segundo_apellido
 * @property string $segundo_nombre
 * @property string $sexo
 * @property string $telefono_celular
 * @property string $telefono_residencia
 * @property string $zona_postal_residencia
 * @property integer $usuario_id
 * @property string $registro
 * @property string $estatus
 * @property integer $usuario_ini_id
 * @property integer $fecha_ini
 * @property integer $fecha_act
 * @property integer $usuario_act_id
 * @property integer $estado_id
 * @property integer $municipio_id
 * @property integer $parroquia_id
 * @property integer $circuito_id
 * @property integer $ente_id
 * @property string $centro_votacion
 * @property string $cod_circuito
 * @property int $cod_estado_cne
 * @property string $twitter
 *
 * The followings are the available model relations:
 * @property UsergroupsUser $usuario
 * @property UsergroupsUser $usuarioIni
 * @property UsergroupsUser $usuarioAct
 * @property Estado $estado
 * @property Municipio $municipio
 * @property Parroquia $parroquia
 */
class Votante extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'gpersonal.votante';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
				array('cedula, primer_apellido, primer_nombre', 'required'),
				array('cedula, usuario_id, usuario_ini_id, usuario_act_id, estado_id, municipio_id, parroquia_id, circuito_id, ente_id', 'numerical', 'integerOnly'=>true),
				array('direccion_residencia', 'length', 'max'=>100),
				array('doble_nacionalidad, estado_civil, nacionalidad, sexo, registro, estatus', 'length', 'max'=>1),
				array('email', 'length', 'max'=>60),
				array('primer_apellido, primer_nombre, segundo_apellido, segundo_nombre', 'length', 'max'=>25),
				array('telefono_celular, telefono_residencia', 'length', 'max'=>15),
				array('zona_postal_residencia', 'length', 'max'=>6),
				array('cod_circuito', 'length', 'max'=>4),
				array('estatus', 'in', 'range'=>array('A', 'I', 'E'), 'allowEmpty'=>false, 'strict'=>true,),
				array('usuario_ini_id', 'default', 'value'=>Yii::app()->user->id, 'on'=>'insert'),
				array('usuario_act_id', 'default', 'value'=>Yii::app()->user->id, 'on'=>'update'),
				array('estado_id,municipio_id,parroquia_id,telefono_celular,email','required','on'=>'contactoVotante'),
				array('email', 'email'),
				array('centro_votacion', 'safe'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
				array('id, cedula, direccion_residencia, doble_nacionalidad, email, estado_civil, fecha_nacimiento, nacionalidad, primer_apellido, primer_nombre, segundo_apellido, segundo_nombre, sexo, telefono_celular, telefono_residencia, zona_postal_residencia, usuario_id, registro, estatus, usuario_ini_id, fecha_ini, fecha_act, usuario_act_id, estado_id, municipio_id, parroquia_id, circuito_id, ente_id, centro_votacion, cod_circuito', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
				'usuario' => array(self::BELONGS_TO, 'UserGroupsUser', 'usuario_id'),
				'usuarioIni' => array(self::BELONGS_TO, 'UserGroupsUser', 'usuario_ini_id'),
				'usuarioAct' => array(self::BELONGS_TO, 'UserGroupsUser', 'usuario_act_id'),
				'estado' => array(self::BELONGS_TO, 'Estado', 'estado_id'),
				'municipio' => array(self::BELONGS_TO, 'Municipio', 'municipio_id'),
				'parroquia' => array(self::BELONGS_TO, 'Parroquia', 'parroquia_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
				'id' => 'ID',
				'cedula' => 'Cedula',
				'direccion_residencia' => 'Direccion Residencia',
				'doble_nacionalidad' => 'Doble Nacionalidad',
				'email' => 'Email',
				'estado_civil' => 'Estado Civil',
				'fecha_nacimiento' => 'Fecha Nacimiento',
				'nacionalidad' => 'Nacionalidad',
				'primer_apellido' => 'Primer Apellido',
				'primer_nombre' => 'Primer Nombre',
				'segundo_apellido' => 'Segundo Apellido',
				'segundo_nombre' => 'Segundo Nombre',
				'sexo' => 'Sexo',
				'telefono_celular' => 'Telefono Celular',
				'telefono_residencia' => 'Telefono Residencia',
				'zona_postal_residencia' => 'Zona Postal Residencia',
				'usuario_id' => 'Usuario',
				'registro' => 'Registro',
				'estatus' => 'Estatus',
				'usuario_ini_id' => 'Usuario Ini',
				'fecha_ini' => 'Fecha Ini',
				'fecha_act' => 'Fecha Act',
				'usuario_act_id' => 'Usuario Act',
				'estado_id' => 'Estado',
				'municipio_id' => 'Municipio',
				'parroquia_id' => 'Parroquia',
				'circuito_id' => 'Circuito',
				'ente_id' => 'Ente',
				'centro_votacion' => 'Centro Votacion',
				'cod_circuito' => 'Cod Circuito',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		if(is_numeric($this->id)) $criteria->compare('id',$this->id);
		if(is_numeric($this->cedula)) $criteria->compare('cedula',$this->cedula);
		if(strlen($this->direccion_residencia)>0) $criteria->compare('direccion_residencia',$this->direccion_residencia,true);
		if(strlen($this->doble_nacionalidad)>0) $criteria->compare('doble_nacionalidad',$this->doble_nacionalidad,true);
		if(strlen($this->email)>0) $criteria->compare('email',$this->email,true);
		if(strlen($this->estado_civil)>0) $criteria->compare('estado_civil',$this->estado_civil,true);
		if(Utiles::isValidDate($this->fecha_nacimiento, 'y-m-d')) $criteria->compare('fecha_nacimiento',$this->fecha_nacimiento);
		// if(strlen($this->fecha_nacimiento)>0) $criteria->compare('fecha_nacimiento',$this->fecha_nacimiento,true);
		if(strlen($this->nacionalidad)>0) $criteria->compare('nacionalidad',$this->nacionalidad,true);
		if(strlen($this->primer_apellido)>0) $criteria->compare('primer_apellido',$this->primer_apellido,true);
		if(strlen($this->primer_nombre)>0) $criteria->compare('primer_nombre',$this->primer_nombre,true);
		if(strlen($this->segundo_apellido)>0) $criteria->compare('segundo_apellido',$this->segundo_apellido,true);
		if(strlen($this->segundo_nombre)>0) $criteria->compare('segundo_nombre',$this->segundo_nombre,true);
		if(strlen($this->sexo)>0) $criteria->compare('sexo',$this->sexo,true);
		if(strlen($this->telefono_celular)>0) $criteria->compare('telefono_celular',$this->telefono_celular,true);
		if(strlen($this->telefono_residencia)>0) $criteria->compare('telefono_residencia',$this->telefono_residencia,true);
		if(strlen($this->zona_postal_residencia)>0) $criteria->compare('zona_postal_residencia',$this->zona_postal_residencia,true);
		if(is_numeric($this->usuario_id)) $criteria->compare('usuario_id',$this->usuario_id);
		if(strlen($this->registro)>0) $criteria->compare('registro',$this->registro,true);
		if(in_array($this->estatus, array('A', 'I', 'E'))) $criteria->compare('estatus',$this->estatus,true);
		if(is_numeric($this->usuario_ini_id)) $criteria->compare('usuario_ini_id',$this->usuario_ini_id);
		if(is_numeric($this->fecha_ini)) $criteria->compare('fecha_ini',$this->fecha_ini);
		if(is_numeric($this->fecha_act)) $criteria->compare('fecha_act',$this->fecha_act);
		if(is_numeric($this->usuario_act_id)) $criteria->compare('usuario_act_id',$this->usuario_act_id);
		if(is_numeric($this->estado_id)) $criteria->compare('estado_id',$this->estado_id);
		if(is_numeric($this->municipio_id)) $criteria->compare('municipio_id',$this->municipio_id);
		if(is_numeric($this->parroquia_id)) $criteria->compare('parroquia_id',$this->parroquia_id);
		if(is_numeric($this->circuito_id)) $criteria->compare('circuito_id',$this->circuito_id);
		if(is_numeric($this->ente_id)) $criteria->compare('ente_id',$this->ente_id);
		if(strlen($this->centro_votacion)>0) $criteria->compare('centro_votacion',$this->centro_votacion,true);
		if(strlen($this->cod_circuito)>0) $criteria->compare('cod_circuito',$this->cod_circuito,true);

		return new CActiveDataProvider($this, array(
				'criteria'=>$criteria,
		));
	}


	public function beforeInsert()
	{
		parent::beforeSave();
		$this->fecha_ini = date('Y-m-d H:i:s');
		$this->usuario_ini_id = Yii::app()->user->id;
		$this->fecha_act = date('Y-m-d H:i:s');
		$this->usuario_act_id = Yii::app()->user->id;

		$circuito=exec("wget --post-data 'cedula=$this->cedula&nacionalidad=$this->nacionalidad' -e use_proxy=yes -e http_proxy=172.16.0.18:3128 -qO- http://www.psuv.org.ve/busqueda-registro-electoral-parlamentarias-2015 | grep \"/wp-content/themes/psuv/images/boletasimg/\" | awk -F \"/\" '{print $7}'");
		$circuito=substr($circuito,0,4);
		$cod_estado_cne=substr($circuito,0,2);
		$cod_circuito=substr($circuito,2,2);
		$centro_votacion=exec("wget --post-data 'cedula=$this->cedula&nacionalidad=$this->nacionalidad' -e use_proxy=yes -e http_proxy=172.16.0.18:3128 -qO- http://www.psuv.org.ve/busqueda-registro-electoral-parlamentarias-2015 | grep \"Centro de Votación\" | awk -F \"strong\" '{print $3}'");
		$centro_votacion=trim(str_replace('> ','',str_replace('</p>','',$centro_votacion)));

		$this->centro_votacion=$centro_votacion;
		if($circuito){
			$this->cod_circuito=$cod_circuito;
			$this->cod_estado_cne=$cod_estado_cne;
		}
		else {
			$this->cod_circuito=null;
			$this->cod_estado_cne=null;
		}
		return true;
	}

	public function beforeUpdate()
	{
		parent::beforeSave();
		$this->fecha_act = date('Y-m-d H:i:s');
		$this->usuario_act_id = Yii::app()->user->id;
		return true;
	}

	public function beforeDelete(){
		parent::beforeSave();
		$this->fecha_act = date('Y-m-d H:i:s');
		$this->usuario_act_id = Yii::app()->user->id;
		// $this->fecha_eli = $this->fecha_act;
		$this->estatus = 'I';
		return true;
	}

	public function beforeActivate(){
		parent::beforeSave();
		$this->fecha_act = date('Y-m-d H:i:s');
		$this->usuario_act_id = Yii::app()->user->id;
		$this->estatus = 'A';
		return true;
	}

	public function __toString() {
		try {
			return (string) $this->id;
		} catch (Exception $exception) {
			return $exception->getMessage();
		}
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Votante the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
