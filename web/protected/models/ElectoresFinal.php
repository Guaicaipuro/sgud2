<?php

/**
 * This is the model class for table "electores_final".
 *
 * The followings are the available columns in table 'electores_final':
 * @property string $origen
 * @property string $cedula
 * @property string $primer_apellido
 * @property string $segundo_apellido
 * @property string $primer_nombre
 * @property string $segundo_nombre
 * @property string $sexo
 * @property string $fecha_nacimiento
 * @property integer $cod_estado
 * @property integer $cod_municipio
 * @property integer $cod_parroquia
 * @property integer $cod_centro_votacion
 */
class ElectoresFinal extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'electores_final';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
				array('cod_estado, cod_municipio, cod_parroquia, cod_centro_votacion', 'numerical', 'integerOnly'=>true),
				array('origen, sexo', 'length', 'max'=>1),
				array('cedula', 'length', 'max'=>8),
				array('primer_apellido, segundo_apellido, primer_nombre, segundo_nombre', 'length', 'max'=>35),
				array('origen', 'in', 'range'=>array('V', 'E', 'P'), 'allowEmpty'=>false, 'strict'=>true,),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
				array('origen, cedula, primer_apellido, segundo_apellido, primer_nombre, segundo_nombre, sexo, fecha_nacimiento, cod_estado, cod_municipio, cod_parroquia, cod_centro_votacion', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
				'origen' => 'Origen',
				'cedula' => 'Cedula',
				'primer_apellido' => 'Primer Apellido',
				'segundo_apellido' => 'Segundo Apellido',
				'primer_nombre' => 'Primer Nombre',
				'segundo_nombre' => 'Segundo Nombre',
				'sexo' => 'Sexo',
				'fecha_nacimiento' => 'Fecha Nacimiento',
				'cod_estado' => 'Cod Estado',
				'cod_municipio' => 'Cod Municipio',
				'cod_parroquia' => 'Cod Parroquia',
				'cod_centro_votacion' => 'Cod Centro Votacion',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		if(strlen($this->origen)>0) $criteria->compare('origen',$this->origen,true);
		if(strlen($this->cedula)>0) $criteria->compare('cedula',$this->cedula,true);
		if(strlen($this->primer_apellido)>0) $criteria->compare('primer_apellido',$this->primer_apellido,true);
		if(strlen($this->segundo_apellido)>0) $criteria->compare('segundo_apellido',$this->segundo_apellido,true);
		if(strlen($this->primer_nombre)>0) $criteria->compare('primer_nombre',$this->primer_nombre,true);
		if(strlen($this->segundo_nombre)>0) $criteria->compare('segundo_nombre',$this->segundo_nombre,true);
		if(strlen($this->sexo)>0) $criteria->compare('sexo',$this->sexo,true);
		if(Utiles::isValidDate($this->fecha_nacimiento, 'y-m-d')) $criteria->compare('fecha_nacimiento',$this->fecha_nacimiento);
		// if(strlen($this->fecha_nacimiento)>0) $criteria->compare('fecha_nacimiento',$this->fecha_nacimiento,true);
		if(is_numeric($this->cod_estado)) $criteria->compare('cod_estado',$this->cod_estado);
		if(is_numeric($this->cod_municipio)) $criteria->compare('cod_municipio',$this->cod_municipio);
		if(is_numeric($this->cod_parroquia)) $criteria->compare('cod_parroquia',$this->cod_parroquia);
		if(is_numeric($this->cod_centro_votacion)) $criteria->compare('cod_centro_votacion',$this->cod_centro_votacion);

		return new CActiveDataProvider($this, array(
				'criteria'=>$criteria,
		));
	}

	public function  getCentroVotacion($origen,$cedula){
		$sql = "SELECT cv.nombre_cv as centro_votacion
				FROM electores_final e
				INNER JOIN centro_votacion cv ON cv.cod_nueva_cv=e.cod_centro_votacion
				WHERE e.origen=:origen AND e.cedula=:cedula";
		$buqueda = Yii::app()->dbCne->createCommand($sql);
		$buqueda->bindParam(":cedula", $cedula, PDO::PARAM_STR);
		$buqueda->bindParam(":origen", $origen, PDO::PARAM_STR);
		$resultado = $buqueda->queryScalar();
		return $resultado;
	}
	/**
	 * @return CDbConnection the database connection used for this class
	 */
	public function getDbConnection()
	{
		return Yii::app()->dbCne;
	}


	public function beforeInsert()
	{
		parent::beforeSave();
		$this->fecha_ini = date('Y-m-d H:i:s');
		$this->usuario_ini_id = Yii::app()->user->id;
		$this->fecha_act = date('Y-m-d H:i:s');
		$this->usuario_act_id = Yii::app()->user->id;
		return true;
	}

	public function beforeUpdate()
	{
		parent::beforeSave();
		$this->fecha_act = date('Y-m-d H:i:s');
		$this->usuario_act_id = Yii::app()->user->id;
		return true;
	}

	public function beforeDelete(){
		parent::beforeSave();
		$this->fecha_act = date('Y-m-d H:i:s');
		$this->usuario_act_id = Yii::app()->user->id;
		// $this->fecha_eli = $this->fecha_act;
		$this->estatus = 'I';
		return true;
	}

	public function beforeActivate(){
		parent::beforeSave();
		$this->fecha_act = date('Y-m-d H:i:s');
		$this->usuario_act_id = Yii::app()->user->id;
		$this->estatus = 'A';
		return true;
	}

	public function __toString() {
		try {
			return (string) $this->id;
		} catch (Exception $exception) {
			return $exception->getMessage();
		}
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return ElectoresFinal the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
