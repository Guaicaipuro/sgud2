<?php

/**
 * This is the model class for table "centro_votacion".
 *
 * The followings are the available columns in table 'centro_votacion':
 * @property integer $cod_vieja_cv
 * @property integer $cod_nueva_cv
 * @property integer $condicion
 * @property integer $cod_estado
 * @property integer $cod_municipio
 * @property integer $cod_parroquia
 * @property string $nombre_cv
 * @property string $direccion_cv
 */
class CentroVotacion extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'centro_votacion';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('cod_vieja_cv, cod_nueva_cv, condicion, cod_estado, cod_municipio, cod_parroquia', 'numerical', 'integerOnly'=>true),
			array('nombre_cv', 'length', 'max'=>255),
			array('direccion_cv', 'length', 'max'=>755),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('cod_vieja_cv, cod_nueva_cv, condicion, cod_estado, cod_municipio, cod_parroquia, nombre_cv, direccion_cv', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'cod_vieja_cv' => 'Cod Vieja Cv',
			'cod_nueva_cv' => 'Cod Nueva Cv',
			'condicion' => 'Condicion',
			'cod_estado' => 'Cod Estado',
			'cod_municipio' => 'Cod Municipio',
			'cod_parroquia' => 'Cod Parroquia',
			'nombre_cv' => 'Nombre Cv',
			'direccion_cv' => 'Direccion Cv',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		if(is_numeric($this->cod_vieja_cv)) $criteria->compare('cod_vieja_cv',$this->cod_vieja_cv);
		if(is_numeric($this->cod_nueva_cv)) $criteria->compare('cod_nueva_cv',$this->cod_nueva_cv);
		if(is_numeric($this->condicion)) $criteria->compare('condicion',$this->condicion);
		if(is_numeric($this->cod_estado)) $criteria->compare('cod_estado',$this->cod_estado);
		if(is_numeric($this->cod_municipio)) $criteria->compare('cod_municipio',$this->cod_municipio);
		if(is_numeric($this->cod_parroquia)) $criteria->compare('cod_parroquia',$this->cod_parroquia);
		if(strlen($this->nombre_cv)>0) $criteria->compare('nombre_cv',$this->nombre_cv,true);
		if(strlen($this->direccion_cv)>0) $criteria->compare('direccion_cv',$this->direccion_cv,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * @return CDbConnection the database connection used for this class
	 */
	public function getDbConnection()
	{
		return Yii::app()->dbCne;
	}

        
        public function beforeInsert()
	{
            parent::beforeSave();
            $this->fecha_ini = date('Y-m-d H:i:s');
            $this->usuario_ini_id = Yii::app()->user->id;
            $this->fecha_act = date('Y-m-d H:i:s');
            $this->usuario_act_id = Yii::app()->user->id;
            return true;
	}
        
        public function beforeUpdate()
	{
            parent::beforeSave();
            $this->fecha_act = date('Y-m-d H:i:s');
            $this->usuario_act_id = Yii::app()->user->id;
            return true;
	}
        
        public function beforeDelete(){
            parent::beforeSave();
            $this->fecha_act = date('Y-m-d H:i:s');
            $this->usuario_act_id = Yii::app()->user->id;
            // $this->fecha_eli = $this->fecha_act;
            $this->estatus = 'I';
            return true;
        }
        
        public function beforeActivate(){
            parent::beforeSave();
            $this->fecha_act = date('Y-m-d H:i:s');
            $this->usuario_act_id = Yii::app()->user->id;
            $this->estatus = 'A';
            return true;
        }
        
        public function __toString() {
            try {
                return (string) $this->id;
            } catch (Exception $exception) {
                return $exception->getMessage();
            }
        }
        
	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return CentroVotacion the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
