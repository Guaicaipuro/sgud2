<?php

class Utiles {

    //private $rutaTmp='/var/www/SIR-Des/archtmp/';
    //private $rutaPdf='/var/www/SIR-Des/archpdf/';

    const TRAZA = "INSERT INTO auditoria.traza (username, fecha_hora, ip_maquina, tipo_transaccion, modulo) VALUES (:username, :fecha_hora, :ip_maquina, :tipo_transaccion, :modulo);";

    private $rutaTmp = '/var/www/sirPhp/archtmp/';
    private $rutaPdf = '/var/www/sirPhp/archpdf/';


    /**
     *
     *  // $nombre_pdf = 'planteles-' . date('YmdHis') ;
     *  // $generadorPDF= new Utiles();
     *  // $reportePDF = ($this->renderPartial('application.modules.planteles.views.consultar._reporteListaPlanteles', array('model' => $dataReport, 'headers'=>$headers), true));
     *  // $horizontal = true;
     *  // $generadorPDF->crearArchivo($nombre_pdf, $reportePDF, $horizontal); // aca se genera y se guarda el archivo en un directorio
     *  //
     *  // $url= Yii::app()->baseUrl.'/../public/tmp/'.$nombre_pdf; // aca creamos un acceso o link para acceder al pdf almacenado en el direcorio
     *  //
     *  // $this->redirect($url);
     *
     * @param type $nombreArchivo
     * @param type $html
     * @param type $horizontal
     */
    public function crearArchivo($nombreArchivo, $html, $horizontal = true) {
        $nombreTemporalHTML = Yii::app()->basePath . '/../public/tmp/' . 'temporal.html';
        $nombreHTML = Yii::app()->basePath . '/../public/tmp/' . $nombreArchivo . '.html';
        $nombrePDF = Yii::app()->basePath . '/../public/tmp/' . $nombreArchivo . '.pdf';
        $archivo = fopen($nombreTemporalHTML, 'wb+');
        fwrite($archivo, $html);
        fclose($archivo);
        passthru("iconv --from-code=UTF-8 --to-code=ISO-8859-1 $nombreTemporalHTML > $nombreHTML");
        $this->crearPdf($nombreHTML, $nombrePDF, $horizontal);
    }

    public function crearPdf($archEntrada, $archSalida, $horizontal = true) {

        if ($horizontal == TRUE) {
            putenv("HTMLDOC_NOCGI=1");
            passthru("htmldoc -f $archSalida -t pdf14 --footer '/' --bodyfont Arial --fontsize 9 --jpeg  --webpage $archEntrada --landscape");
        }
        if ($horizontal == FALSE) {
            putenv("HTMLDOC_NOCGI=1");
            passthru("htmldoc -f $archSalida -t pdf14 --jpeg --footer '/' --webpage $archEntrada");
        }
        return 0;
    }

    public function mostrarPdf($archEntrada) {
        putenv("HTMLDOC_NOCGI=1");
        header("Content-Type: application/pdf");
        flush();
        passthru("htmldoc -t pdf14 --jpeg --webpage '$archEntrada'");
    }

    /**
     * Se encarga de enviar un correo desde la cuenta admin de la aplicacion SIR
     * a traves del servidor SMTP de CANTV, utiliza la extension instala de PHP-Mailer
     * Solo envia un correo a una persona, se debe mejorar para que pueda enciar CC y CCO
     * y tener varios destinatarios
     * @param string $to Direccion electronica de la persona a enviar el correo
     * @param string $subject Asunto del Correo
     * @param string $msj Mesaje del correo
     */
    static public function enviarCorreo($to, $subject = 'SIR-SWL', $msj = '') {
        $mailer = Yii::createComponent('application.extensions.mailer.EMailer');
        $mailer->Host = 'mail.me.gob.ve:25';
        $mailer->IsSMTP();
        $mailer->From = Yii::app()->params->adminEmail;
        if(is_array($to)){
            foreach($to as $sendTo){
                $mailer->AddAddress("{$sendTo}");
            }
        }else{
            $mailer->AddAddress("{$to}");
        }
        $mailer->FromName = Yii::app()->params->adminName;
        $mailer->CharSet = 'UTF-8';
        $mailer->Subject = $subject;
        $mailer->MsgHTML($msj);
        return $mailer->Send();
    }

    /**
     * Se encarga de enviar varios correos con el cintillo del ministerio y la información correspondientes.
     * @param string $correos Direccion electronica de las personas a enviar el correo.
     * @param string $from Direccion electronica del correo quien lo envia.
     * @param string $fromName Nombre de quien lo envia.
     * @param string $addBCC Para enviar copia oculta.
     * @param string $subject Asunto del correo.
     * @param string $mensaje Mesaje del correo
     * @param string $info Información de que trata el correo.
     */
    /* Ejemplo:
     * $from = 'soporte_gescolar@me.gob.ve';
     * $fromName = 'Gescolar ';
     * $emailOculto = 'gescolar.mppe@gmail.com';
     * $msjOculto = 'Sistema de Gestión Escolar';
     * $subject = 'Notificación de Validación de Entrega de Lotes de Papel Moneda a las Zonas Educativas del MPPE';
     * $info = 'de la observación de los datos incorrectos del director';
     * */
    static public function enviarCorreo2($correos, $from, $fromName, $emailOculto, $msjOculto, $subject, $mensaje, $info) {

        foreach ($correos as $key => $data) {
            $destinatario_nombre = (isset($data['nombre'])) ? $data['nombre'] : null;
            $destinatario_apellido = (isset($data['apellido'])) ? $data['apellido'] : null;
            $remitente_correo = (isset($data['correo'])) ? $data['correo'] : null;
            /*$destinatario_nombre = 'Administrador del';
            $destinatario_apellido = 'Sistema';
            $remitente_correo = 'soporte_gescolar@me.gob.ve';*/

            $mailer = Yii::createComponent('application.extensions.mailer.EMailer');
            $mailer->Host = 'mail.me.gob.ve:25';
            $mailer->IsSMTP();
            $mailer->From = $from;
            $mailer->FromName = $fromName;
            $mailer->AddAddress($remitente_correo, $destinatario_nombre . ' ' . $destinatario_apellido);
            $mailer->AddBCC($emailOculto, $msjOculto);
            $mailer->CharSet = 'UTF-8';
            $mailer->Subject = $subject;
            $mailer->IsHTML(true);
            $mailer->AddEmbeddedImage(yii::app()->basePath . '/../public/images/barra_n.png', 'barra', 'barra_n.png');
            $mailer->AddEmbeddedImage(yii::app()->basePath . '/../public/images/sintillo.png', 'sintillo', 'barra_n.png');
            $mailer->AddEmbeddedImage(yii::app()->basePath . '/../public/images/logo_sistema.png', 'logo', 'barra_n.png');

            $body = '<html>
                                <head>
                                <table>
                                <tr>
                                <td colspan="2" width="1000px"><img src="cid:barra"/></td>
                                </tr>
                                <tr>
                                <td width="700px"><img class="pull-left"  src="cid:sintillo" height="46"/></td>
                                <td width="300px"><img class="pull-right"  src="cid:logo"/></td>
                                </tr>
                                </table>
                                <br>
                                <br>
                                </head>';
            $body .= "$mensaje<br>";
            $body .='<br><title>
                                </title>
                                <style>
                                </style>
                                <body>
                                </body>
                        </html>';


            $mailer->Body = $body;
            $exito = $mailer->Send();
            $intentos = 1;
            while ((!$exito) && ($intentos < 2)) {
                sleep(5);
                $exito = $mailer->Send();
                $intentos = $intentos + 1;
            }
            if (!$exito) {
                echo "Problemas enviando correo electrónico $info";
                echo "<br>" . $mailer->ErrorInfo;
                echo " ";

                $destinatario_nombre = 'Administrador del';
                $destinatario_apellido = 'Sistema';
                $remitente_correo = 'soporte_gescolar@me.gob.ve';
                $mailer = Yii::createComponent('application.extensions.mailer.EMailer');
                $mailer->Host = 'mail.me.gob.ve:25';
                $mailer->IsSendmail();
                $mailer->From = 'soporte_gescolar@me.gob.ve'; //Es quien lo envia
                $mailer->FromName = 'Gescolar ';
                $mailer->AddAddress($remitente_correo, $destinatario_nombre . ' ' . $destinatario_apellido);
                $mailer->CharSet = 'UTF-8';
                $mailer->Subject = "Notificación de envio de correo $info";
                $mailer->Body = "Problemas enviando correo electrónico, $info";
                $mailer->Send();
            }
        }
    }


    public static function ae_detect_ie() {
        if (isset($_SERVER['HTTP_USER_AGENT']) &&
                (strpos($_SERVER['HTTP_USER_AGENT'], 'MSIE') !== false))
            return true;
        else
            return false;
    }

    public function traza($transaccion, $modulo, $fecha_hora) {

        $ipUser = $this->getRealIP();
        //$ipUser = Yii::app()->request->userHostAddress;
        $userName = Yii::app()->user->name;
        $oDbConnection = Yii::app()->db; // Getting database connection (config/main.php has to set up database
        // Here you will use your complex sql query using a string or other yii ways to create your query
        $oCommand = $oDbConnection->createCommand(self::TRAZA);
        $oCommand->bindParam(':username', $userName, PDO::PARAM_STR);
        $oCommand->bindParam(':fecha_hora', $fecha_hora, PDO::PARAM_STR);
        $oCommand->bindParam(':ip_maquina', $ipUser, PDO::PARAM_STR);
        $oCommand->bindParam(':tipo_transaccion', $transaccion, PDO::PARAM_STR);
        $oCommand->bindParam(':modulo', $modulo, PDO::PARAM_STR);
        $oCommand->queryAll(); // Run query and get all results in a CDbDataReader
    }

    public static function getRealIP() {

        $ip = "";
        if (isset($_SERVER)) {
            if (!empty($_SERVER['HTTP_CLIENT_IP'])) {
                $ip = $_SERVER['HTTP_CLIENT_IP'];
            } elseif (!empty($_SERVER['HTTP_X_FORWARDED_FOR'])) {
                $ip = $_SERVER['HTTP_X_FORWARDED_FOR'];
            } else {
                $ip = $_SERVER['REMOTE_ADDR'];
            }
        } else {
            if (getenv('HTTP_CLIENT_IP')) {
                $ip = getenv('HTTP_CLIENT_IP');
            } elseif (getenv('HTTP_X_FORWARDED_FOR')) {
                $ip = getenv('HTTP_X_FORWARDED_FOR');
            } else {
                $ip = getenv('REMOTE_ADDR');
            }
        }
        // En algunos casos muy raros la ip es devuelta repetida dos veces separada por coma
        if (strstr($ip, ',')) {
            $ip = array_shift(explode(',', $ip));
        }
        return $ip;
    }

    /**
     * Modifies a string to remove all non ASCII characters and spaces.
     */
    static public function slugify($text) {
        // replace non letter or digits by -
        $text = preg_replace('~[^\\pL\d]+~u', '-', $text);

        // trim
        $text = trim($text, '-');

        // transliterate
        if (function_exists('iconv')) {
            $text = iconv('utf-8', 'us-ascii//TRANSLIT', $text);
        }

        // lowercase
        $text = strtolower($text);

        // remove unwanted characters
        $text = preg_replace('~[^-\w]+~', '', $text);

        if (empty($text)) {
            return 'n-a';
        }

        return $text;
    }

    /**
     * Verifica si el argumento pasado es una expresión para comparar campos numéricos.
     * Por ejemplo:
     *  >=50
     *  <=50
     *  50
     *  >50
     *  <50
     * @param string $val
     */
    public static function isExpressionToNumericCompare($val) {

        $result = false;

        if (!empty($val)) {

            if (is_numeric($val)) {

                $result = true;
            } else {

                $count = 0;

                $val_lt = str_replace('<', '', $val, $count);
                $pos_lt = strpos($val, '<');

                if (!empty($val_lt) && $pos_lt == 0 && is_numeric($val_lt) && $count == 1) {
                    $result = true;
                }

                $val_gt = str_replace('>', '', $val, $count);
                $pos_gt = strpos($val, '>');

                if (!empty($val_gt) && $pos_gt == 0 && is_numeric($val_gt) && $count == 1) {
                    $result = true;
                }

                $val_lte = str_replace('<=', '', $val, $count);
                $pos_lte = strpos($val, '<=');

                if (!empty($val_lte) && $pos_lte == 0 && is_numeric($val_lte) && $count == 1) {
                    $result = true;
                }

                $val_gte = str_replace('>=', '', $val, $count);
                $pos_gte = strpos($val, '>=');

                if (!empty($val_gte) && $pos_gte == 0 && is_numeric($val_gte) && $count == 1) {
                    $result = true;
                }
            }
        } else {
            $result = true;
        }
        return $result;
    }

    /**
     *   //Ejemplo
     *   $fecha= "24/09/2009";
     *
     *   if(dateCheck($fecha)===false){
     *       echo "La fecha no es correcta";
     *   }else{
     *       echo "La fecha es correcta";
     *   }
     *   // imprime "La fecha es correcta"
     *
     *   @param String $input Fecha en formato DD/MM/YYYY ó DD.MM.YYYY ó DD-MM-YYYY
     *          si $format es igual a "dmy" (diamesanho)
     *   @param String $format formato de la fecha valores permitidos dmy, mdy y ymd
     */
    public static function dateCheck($input, $format = "dmy") {
        $separator_type = array(
            "/",
            "-",
            "."
        );
        $separator_used = '/';
        foreach ($separator_type as $separator) {
            $find = stripos($input, $separator);
            if ($find <> false) {
                $separator_used = $separator;
                break;
            }
        }
        $input_array = explode($separator_used, $input);
        if (count($input_array) == 3) {
            //echo "count(input_array)==3";
            if ($format == "mdy") {
                return checkdate($input_array[0], $input_array[1], $input_array[2]);
            } elseif ($format == "ymd") {
                return checkdate($input_array[1], $input_array[2], $input_array[0]);
            } else {
                //echo "format=dmy | {$input_array[1]},{$input_array[0]},{$input_array[2]}";
                return checkdate($input_array[1], $input_array[0], $input_array[2]);
            }
        } else {
            return false;
        }
    }

    public static function transformDate($input, $sep = '-', $from = 'dmy', $to = "ymd") {
        $separator_type = array(
            "/",
            "-",
            "."
        );
        $separator_used = '/';
        foreach ($separator_type as $separator) {
            $find = stripos($input, $separator);
            if ($find <> false) {
                $separator_used = $separator;
                break;
            }
        }
        $input_array = explode($separator_used, $input);
        if (count($input_array) == 3) {
            $isValid = false;
            //echo "count(input_array)==3";
            if ($from == "mdy") { //month-day-year
                if (checkdate($input_array[0], $input_array[1], $input_array[2])) {
                    $year = $input_array[2];
                    $month = $input_array[0];
                    $day = $input_array[1];
                    $isValid = true;
                }
            } elseif ($from == "ymd") {//year-month-day
                if (checkdate($input_array[1], $input_array[2], $input_array[0])) {
                    $year = $input_array[0];
                    $month = $input_array[1];
                    $day = $input_array[2];
                    $isValid = true;
                }
            } else {
                //echo "format=dmy | {$input_array[1]},{$input_array[0]},{$input_array[2]}";
                if (checkdate($input_array[1], $input_array[0], $input_array[2])) {
                    $year = $input_array[2];
                    $month = $input_array[1];
                    $day = $input_array[0];
                    $isValid = true;
                }
            }

            if ($isValid) {
                if ($to == 'ymd') {
                    return $year . $sep . $month . $sep . $day;
                } elseif ($to == 'dmy') {
                    return $day . $sep . $month . $sep . $year;
                } else {
                    return $month . $sep . $day . $sep . $year;
                }
            }
        }

        return $input;
    }

    public function hourCheck($input, $format = 'HH24') {
        if ($format == 'HH24')
            return preg_match("/(2[0-3]|[01][0-9]):[0-5][0-9]/", $input);
        else
            return preg_match("/(1[012]|0[0-9]):[0-5][0-9]/", $input);
    }

    public static function onlyTextString($input) {
        return preg_replace("/[^A-Za-z0-9 .,\-();]/", "", $input);
    }

    public static function onlyAlphaNumericString($input) {
        return preg_replace("/[^A-Za-z0-9]/", "", $input);
    }

    public static function onlyAlphaNumericWithSpace($input) {
        return preg_replace("/[^A-Za-z0-9 ]/", "", $input);
    }

    public static function onlyNumericString($input) {
        return preg_replace("/[^0-9]/", "", $input);
    }

    public static function getFileExtension($filename) {
        return substr(strrchr($filename, '.'), 1);
    }

    /*
     * Convertir cadenas a minúsculas en utf8
     *
     * @recibe   cadena de caracteres
     * @devuelve cadena reemplazada
     *
     * Uso: $objeto->strtolower_utf8(cadena);
     *
     */

    public static function strtolower_utf8($cadena) {
        $convertir_a = array(
            "a", "b", "c", "d", "e", "f", "g", "h", "i", "j", "k", "l", "m", "n", "o", "p", "q", "r", "s", "t", "u",
            "v", "w", "x", "y", "z", "à", "á", "â", "ã", "ä", "å", "æ", "ç", "è", "é", "ê", "ë", "ę", "ì", "í", "î", "ï",
            "ð", "ñ", "ò", "ó", "ô", "õ", "ö", "ø", "ù", "ú", "û", "ü", "ý", "а", "б", "в", "г", "д", "е", "ё", "ж",
            "з", "и", "й", "к", "л", "м", "н", "о", "п", "р", "с", "т", "у", "ф", "х", "ц", "ч", "ш", "щ", "ъ", "ы",
            "ь", "э", "ю", "я"
        );
        $convertir_de = array(
            "A", "B", "C", "D", "E", "F", "G", "H", "I", "J", "K", "L", "M", "N", "O", "P", "Q", "R", "S", "T", "U",
            "V", "W", "X", "Y", "Z", "À", "Á", "Â", "Ã", "Ä", "Å", "Æ", "Ç", "È", "É", "Ê", "Ë", "Ę", "Ì", "Í", "Î", "Ï",
            "Ð", "Ñ", "Ò", "Ó", "Ô", "Õ", "Ö", "Ø", "Ù", "Ú", "Û", "Ü", "Ý", "А", "Б", "В", "Г", "Д", "Е", "Ё", "Ж",
            "З", "И", "Й", "К", "Л", "М", "Н", "О", "П", "Р", "С", "Т", "У", "Ф", "Х", "Ц", "Ч", "Ш", "Щ", "Ъ", "Ъ",
            "Ь", "Э", "Ю", "Я"
        );
        return str_replace($convertir_de, $convertir_a, $cadena);
    }

    /*
     * Convertir cadenas a mayúsculas en utf8
     *
     * @recibe   cadena de caracters
     * @devuelve cadena reemplazada
     *
     * Uso: $objeto->strtotoupper_utf8(cadena);
     *
     */

    public static function strtoupper_utf8($cadena) {
        $convertir_de = array(
            "a", "b", "c", "d", "e", "f", "g", "h", "i", "j", "k", "l", "m", "n", "o", "p", "q", "r", "s", "t", "u",
            "v", "w", "x", "y", "z", "à", "á", "â", "ã", "ä", "å", "æ", "ç", "è", "é", "ê", "ë", "ę", "ì", "í", "î", "ï",
            "ð", "ñ", "ò", "ó", "ô", "õ", "ö", "ø", "ù", "ú", "û", "ü", "ý", "а", "б", "в", "г", "д", "е", "ё", "ж",
            "з", "и", "й", "к", "л", "м", "н", "о", "п", "р", "с", "т", "у", "ф", "х", "ц", "ч", "ш", "щ", "ъ", "ы",
            "ь", "э", "ю", "я"
        );
        $convertir_a = array(
            "A", "B", "C", "D", "E", "F", "G", "H", "I", "J", "K", "L", "M", "N", "O", "P", "Q", "R", "S", "T", "U",
            "V", "W", "X", "Y", "Z", "À", "Á", "Â", "Ã", "Ä", "Å", "Æ", "Ç", "È", "É", "Ê", "Ë", "Ę", "Ì", "Í", "Î", "Ï",
            "Ð", "Ñ", "Ò", "Ó", "Ô", "Õ", "Ö", "Ø", "Ù", "Ú", "Û", "Ü", "Ý", "А", "Б", "В", "Г", "Д", "Е", "Ё", "Ж",
            "З", "И", "Й", "К", "Л", "М", "Н", "О", "П", "Р", "С", "Т", "У", "Ф", "Х", "Ц", "Ч", "Ш", "Щ", "Ъ", "Ъ",
            "Ь", "Э", "Ю", "Я"
        );
        return str_replace($convertir_de, $convertir_a, $cadena);
    }

    /**
     * Devuelve el microtaime en formato string con o sin punto (.)
     */
    public static function microtimeString($whitPoint=false){

        list($usec, $sec) = explode(" ", microtime());

        $microtime = (string) ((float)$usec + (float)$sec);
        $resultado = $microtime;

        if(!$whitPoint){
            $resultado =  str_replace(".","",$microtime);
        }

        return $resultado;
    }

    /**
     * Devuelve el microtaime en formato float
     */
    public static function microtimeFloat(){
        list($usec, $sec) = explode(" ", microtime());
        $resultado = ((float)$usec + (float)$sec);
        return $resultado;
    }

    /**
     * Devuelve el microtaime en formato Int completo, los decimales se convierten en parte del número entero
     */
    public static function microtimeInt(){
        list($usec, $sec) = explode(" ", microtime());
        $microtime = ((float)$usec + (float)$sec);
        $resultado =  (int) str_replace(".","",$microtime);
        return $resultado;
    }

    /**
     *   //Ejemplo
     *   $fecha= "24/09/2009";
     *
     *   if(dateCheck($fecha)===false){
     *       echo "La fecha no es correcta";
     *   }else{
     *       echo "La fecha es correcta";
     *   }
     *   // imprime "La fecha es correcta"
     *
     *   @param String $input Fecha en formato DD/MM/YYYY ó DD.MM.YYYY ó DD-MM-YYYY
     *          si $format es igual a "dmy" (diamesanho)
     *   @param String $format formato de la fecha valores permitidos dmy, mdy y ymd
     */
    public static function isValidDate($input, $format = "dmy") {
        $separator_type = array(
            "/",
            "-",
            "."
        );
        $separator_used = '/';
        foreach ($separator_type as $separator) {
            $find = stripos($input, $separator);
            if ($find <> false) {
                $separator_used = $separator;
                break;
            }
        }
        $input_array = explode($separator_used, $input);
        if (count($input_array) == 3) {
            //echo "count(input_array)==3";
            if ($format == "mdy" || $format == "m-d-y") {
                return checkdate($input_array[0], $input_array[1], $input_array[2]);
            } elseif ($format == "ymd" || $format == "y-m-d") {
                return checkdate($input_array[1], $input_array[2], $input_array[0]);
            } else {
                //echo "format=dmy | {$input_array[1]},{$input_array[0]},{$input_array[2]}";
                return checkdate($input_array[1], $input_array[0], $input_array[2]);
            }
        } else {
            return false;
        }
    }

    public static function isNumericArray($array){

        if(is_array($array) and $array!=array()){

                foreach($array as $value){
                    if(!is_numeric($value)){
                        return false;
                    }
                }
                 return true;
        }else{
            return false;
        }

    }

    public static function isValidExtension($filePath, $target){

        $ext = pathinfo($filePath, PATHINFO_EXTENSION);

        $result = false;

        if(is_array($target)){
            $result = (in_array($ext, $target));
        }elseif(is_string($target)){
            $result = ($ext==$target);
        }

        return $result;
    }

    public static function toPgArray($set, $isString=false) {
        settype($set, 'ARRAY'); // can be called with a scalar or array
        $result = array();
        foreach ($set as $t) {
            if (is_array($t)) {
                $result[] = to_pg_array($t);
            } else {
                $t = str_replace('"', '\\"', $t); // escape double quote
                if (!is_numeric($t) || $isString){ // quote only non-numeric values
                    $t = "'" . $t . "'";
                }
                $result[] = $t;
            }
        }
        return 'ARRAY[' . implode(",", $result) . ']'; // format
    }

    public static function pgArrayParse($text, $output, $limit = false, $offset = 1){

        if (false === $limit){
            $limit = strlen($text) - 1;
            $output = array();
        }
        if ('{}' != $text){

            do {
                if ('{' != $text{$offset}) {
                    preg_match("/(\\{?\"([^\"\\\\]|\\\\.)*\"|[^,{}]+)+([,}]+)/", $text, $match, 0, $offset);

                    $offset += strlen($match[0]);
                    $output[] = ( '"' != $match[1]{0} ? $match[1] : stripcslashes(substr($match[1], 1, -1)) );
                    if ('},' == $match[3])
                        return $offset;
                } else {
                    $offset = $this->pg_array_parse($text, $output[], $limit, $offset + 1);
                }
            } while ($limit > $offset);

        }

        return $output;
    }

    
    public static function confirmarUsuario($password){
        $passwordDecoded = base64_decode($password);
        $respuesta = array();

        $passwordEncoded = md5($passwordDecoded);

        $modelUsuario = UserGroupsUser::model()->findByPk(Yii::app()->user->id);

        if ($modelUsuario['password'] == md5($passwordDecoded . $modelUsuario->getSalt())) {
            $respuesta = array('success' => true, 'error' => false);
        } else {
            $respuesta = array('success' => false, 'error' => true);
        }

        return json_encode($respuesta);
        
    }
    
}
