<div class="col-md-12">
    <table class="table table-striped table-bordered table-hover text-center">
        <thead>
            <tr>
                <th>Número de Cédula</th>
                <th>Resultado</th>
                <th>Detalle</th>
            </tr>
        </thead>
        <tbody>
            <?php if(count($resultado)>0): ?>
            <?php   foreach ($resultado as $data): ?>
            <tr>
                <td><?php echo $data['cedula']; ?></td>
                <td><?php echo mb_strtoupper($data['resultado'], 'UTF-8'); ?></td>
                <td class="<?php echo ($data['resultado']=='exito')?'text-success':'text-danger'; ?>"><?php echo $data['mensaje'].(($data['resultado']=='error')?' (Sección: '.$data['seccion'].')':''); ?></td>
            </tr>
            <?php   endforeach; ?>
            <?php endif; ?>
        </tbody>
    </table>
</div>
<div class="col-md-12">
    <div class="space-6"></div>
</div>
<div class="col-md-12">
    <a id="btnRegresar" href="/userGroups/cambiarPerfil" class="btn btn-danger">
        <i class="icon-arrow-left"></i>
        Volver
    </a>
</div>
