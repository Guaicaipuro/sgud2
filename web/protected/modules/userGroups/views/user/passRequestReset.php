<div class="col-sm-10 col-sm-offset-1" style="min-height: 400px;">
    <div class="login-container">
        <div class="space-6"></div>
        <div class="position-relative">
            <?php if (CHtml::errorSummary($model)): ?>
                <div class="errorDialogBox">
                    <?php echo CHtml::errorSummary($model); ?>
                </div>
            <?php elseif (isset($mensajeError) && strlen($mensajeError)>0): ?>
                <div class="errorDialogBox">
                    <p><?php echo $mensajeError; ?></p>
                </div>
            <?php endif; ?>
                <div>
                    <div class="signup-box widget-box no-border visible" id="pass-request-box">
                        <div class="widget-body">
                            <div class="widget-main">
                                <h4 class="header green lighter bigger">
                                    <i class="icon-group blue"></i>
                                    ¿Olvidaste tu Clave?
                                </h4>
                                <div class="space-6"></div>
                                <div id="mensajeValidacion" class="infoDialogBox">
                                    <p> Ingrese los datos necesarios para restablecer su clave </p>
                                </div>
                                <?php
                                $form = $this->beginWidget('CActiveForm', array(
                                    'id' => 'user-groups-reset-passwd-form',
                                    'enableAjaxValidation' => false,
                                    //'htmlOptions' => array(),
                                ));
                                ?>
                                <fieldset>

                                    <label class="block clearfix">
                                        <span class="block input-icon input-icon-right">
                                            <input type="password" autocomplete="off" class="input form-control" required="required" placeholder="Clave de Acceso" title="Ingrese su nueva Clave de Acceso" name="UserGroupsUser[password]" id="UserGroupsUser_password" value="">
                                            <i class="icon-lock"></i>
                                        </span>
                                    </label>

                                    <label class="block clearfix">
                                        <span class="block input-icon input-icon-right">
                                            <input type="password" autocomplete="off" class="input form-control" required="required" placeholder="Confirme su Clave de Acceso" title="Confirme su nueva Clave de Acceso" name="UserGroupsUser[password_confirm]" id="UserGroupsUser_password_confirm" value="">
                                            <i class="icon-lock"></i>
                                        </span>
                                    </label>
                                    <div>
                                        <div class="hide">
                                            <div></div>
                                        </div>
                                        <div class="hide">
                                            <div></div>
                                        </div>
                                        <div class="hide">
                                            <div><div></div></div>
                                            <div><input type="hidden" value="<?php echo $tokenValue; ?>" name="passRequestToken"></div>
                                            <div><div></div></div>
                                        </div>
                                        <div class="hide">
                                            <div></div>
                                        </div>
                                        <div class="hide">
                                            <div></div>
                                        </div>
                                    </div>

                                    <div class="space"></div>

                                    <div class="clearfix">
                                        <button id="btnResetClave" type="submit" data-last="Finish" class="btn btn-primary btn-sm hide hidden">
                                            Restablecer Clave
                                            <i class="icon-key icon-on-right"></i>
                                        </button>
                                        <button id="btnRecuperarClave" type="button" data-last="Finish" class="btn btn-primary btn-sm">
                                            Restablecer Clave
                                            <i class="icon-key icon-on-right"></i>
                                        </button>
                                    </div>

                                    <div class="space-4"></div>
                                </fieldset>
                                
                                <?php $this->endWidget(); ?>
                            </div>

                            <div class="toolbar center">
                                <a class="back-to-login-link" href="/login">
                                    <i class="icon-arrow-left"></i>
                                    Volver al Login
                                </a>
                            </div>
                        </div><!-- /widget-body -->
                    </div>
                </div>
        </div>
    </div>
</div>

<div id="dialog-passwd" class="hide">
    <div class="alertDialogBox"><p id="mensaje-confirm"></p></div>
</div>

<?php
    Yii::app()->clientScript->registerScriptFile(Yii::app()->request->baseUrl . '/public/js/modules/userGroups/usuario/login.min.js',CClientScript::POS_END);
?>
