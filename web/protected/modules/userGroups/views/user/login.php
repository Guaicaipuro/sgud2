<div class="col-sm-10 col-sm-offset-1">
    <div class="login-container">

        <div class="space-6"></div>

        <div class="position-relative">

            <div id="result">
                <?php //var_dump($model->getErrors()); ?>
                <?php if ($model->getErrors()): ?>
                    <?php if ($model->getError('password') != '' || $model->getError('username') != ''): ?>
                        <div class="errorDialogBox">
                            <p>
                                <?php $error= $model->getError('password'); echo $error; ?>
                                <a onclick="show_box('signup-box');
                                            return false;
                                            $('#result').fadeOut();" class="user-signup-link">¿Olvidaste Tu Contraseña?</a>
                            </p>
                        </div>
                    <?php elseif ($model->getError('verifyCode') != ''): ?>
                        <div class="errorDialogBox">
                            <p>
                                El Código De Seguridad Ingresado Es Incorrecto.
                            </p>
                        </div>
                    <?php else: ?>
                        <div class="errorDialogBox">
                            <p>
                                Puede Que Su Usuario Se Encuentre Inactivo.
                            </p>
                        </div>
                    <?php endif; ?>
                <?php endif; ?>
            </div>

            <?php if (isset(Yii::app()->request->cookies['success'])): ?>
                <div class="infoDialogBox">
                    <p><?php echo Yii::app()->request->cookies['success']->value; ?></p>
                    <?php unset(Yii::app()->request->cookies['success']); ?>
                </div>
            <?php endif; ?>
            <?php if (Yii::app()->user->hasFlash('success')): ?>
                <div class="infoDialogBox">
                    <p><?php echo Yii::app()->user->getFlash('success'); ?></p>
                </div>
            <?php endif; ?>
            <?php if (Yii::app()->user->hasFlash('mail')): ?>
                <div class="infoDialogBox">
                    <p><?php echo Yii::app()->user->getFlash('mail'); ?></p>
                </div>
            <?php endif; ?>

            <div id="login-box" class="login-box visible widget-box no-border">
                <div class="widget-body">
                    <div class="widget-main">
                        <h4 class="header blue lighter bigger">
                            <i class="icon-unlock green"></i>
                            Acceso Al Sistema.
                        </h4>

                        <div class="space-6"></div>

                        <?php
                        $form = $this->beginWidget('CActiveForm', array(
                            'id' => 'login-form',
                            'enableAjaxValidation' => false,
                            'focus' => array($model, 'username'),
                        ));
                        ?>
                        <fieldset>

                            <label class="block clearfix">
                                <span class="block input-icon input-icon-right">
                                    <input type="text" value="<?php echo $model->username; ?>" id="UserGroupsUser_username" name="UserGroupsUser[username]" placeholder="Usuario" required="required" class="input form-control" autocomplete="off" />
                                    <i class="icon-user"></i>
                                </span>
                            </label>

                            <label class="block clearfix">
                                <span class="block input-icon input-icon-right">
                                    <input type="password" value="<?php echo $model->password; ?>" id="UserGroupsUser_password" name="UserGroupsUser[password]" placeholder="Contraseña" required="required" class="input form-control" autocomplete="off" />
                                    <i class="icon-lock"></i>
                                </span>
                            </label>

                            <label class="block clearfix">
                                <span class="block input-icon input-icon-right lighter">
                                    <?php echo $form->checkBox($model, 'rememberMe', array('id' => 'rememberMe', 'checked' => 'checked')); ?>
                                    <label for="rememberMe" class="lighter">Recuérdame</label>
                                </span>
                            </label>

                            <div class="block clearfix">
                                <div class="col-xs-4" style="padding-left: 0px;">
                                    <a id="linkRefreshCaptcha" tabindex="-1" style="border-style: none;" title="Haga Click para obtener otra Imágen. El Código no es sensible a mayúsculas y minúsculas.">
                                        <img id="siimage" style="border: 1px solid #DDDDDD; margin-right: 15px" src="/login/captcha/sid/<?php echo md5(uniqid()) ?>" alt="CAPTCHA Image" align="left" height="45" />
                                    </a>
                                </div>
                                <div class="col-xs-8" style="text-align: right; padding-right: 0px;">
                                    <span class="block input-icon input-icon-right">
                                        <?php echo $form->textField($model,'verifyCode', array('required'=>'required', 'style'=>'width: 100%;', 'maxlength'=>'10', 'required'=>'required', 'placeholder'=>'Ingrese el Código de la Imagen', 'title'=>'Ingrese el Código de la Imagen. El código no es sensible a mayúsculas y minúsculas.', 'autocomplete'=>'off')); ?>
                                        <i class="icon-qrcode"></i>
                                    </span>
                                </div>
                            </div>
                            <div>
                                <div class="hide">
                                    <div></div>
                                </div>
                                <div class="hide">
                                    <div></div>
                                </div>
                                <div class="hide">
                                    <div><div></div></div>
                                    <div><input type="hidden" name="<?php echo $tokenName; ?>" value="<?php echo $tokenValue; ?>" /></div>
                                    <div><div></div></div>
                                </div>
                                <div class="hide">
                                    <div></div>
                                </div>
                                <div class="hide">
                                    <div></div>
                                </div>
                            </div>

                            <div class="space"></div>

                            <div class="clearfix">
                                <button type="submit" class="width-35 pull-right btn btn-sm btn-primary">
                                    <i class="icon-key"></i>
                                    Ingresar
                                </button>
                            </div>

                            <div class="space-4"></div>
                        </fieldset>
                    <?php $this->endWidget(); ?>

                    </div><!-- /widget-main -->

                    <div class="toolbar clearfix">
                        <div>
                            <a onclick="show_box('signup-box');
                                    return false;" class="user-signup-link">
                                <i class="icon-arrow-left"></i>
                                ¿Olvidaste Tu Contraseña?
                            </a>
                        </div>

                        <div>
                            <a onclick="show_box('regist-nuevo-box');
                                    return false;" class="user-signup-link">
                                Registrarse
                                <i class="icon-arrow-right"></i>
                            </a>
                        </div>
                    </div>
                </div><!-- /widget-body -->
            </div><!-- /login-box -->


            <div id="regist-nuevo-box" class="signup-box widget-box no-border">
                <div class="widget-body">
                    <div class="widget-main">
                        <h4 class="header green lighter bigger">
                            <i class="icon-group blue"></i>
                            Registro
                        </h4>

                        <div class="space-6"></div>
                        <p> Todos los campos son requeridos: </p>

                        <!-- Formulario de Recupación de Clave -->
                        <?php $this->renderPartial('registerNewUser', array('model' => $model)); ?>
                    </div>

                    <div class="toolbar center">
                        <a href="#" onclick="show_box('login-box');
                                return false;" class="back-to-login-link">
                            <i class="icon-arrow-left"></i>
                            Volver Al Login
                        </a>
                    </div>
                </div><!-- /widget-body -->

            </div><!-- /signup-box -->



            <div id="signup-box" class="signup-box widget-box no-border">
                <div class="widget-body">
                    <div class="widget-main">
                        <h4 class="header green lighter bigger">
                            <i class="icon-group blue"></i>
                            ¿Olvidaste Tu Contraseña?
                        </h4>

                        <div class="space-6"></div>
                        <p> Ingrese Los Datos Necesarios Para Recuperar Su Contraseña: </p>

                        <!-- Formulario de Recupación de Clave -->
                        <?php $this->renderPartial('passRequest', array('model_pr' => $model_pr)); ?>
                    </div>

                    <div class="toolbar center">
                        <a href="#" onclick="show_box('login-box');
                                return false;" class="back-to-login-link">
                            <i class="icon-arrow-left"></i>
                            Volver Al Login
                        </a>
                    </div>
                </div><!-- /widget-body -->

            </div><!-- /signup-box -->

        </div><!-- /position-relative -->

    </div>

</div><!-- /.col -->
<div><?php $this->widget('ext.loading.LoadingWidget'); ?></div>
<?php
    Yii::app()->clientScript->registerScriptFile(Yii::app()->request->baseUrl . '/public/js/jquery.maskedinput.min.js', CClientScript::POS_END);
    Yii::app()->clientScript->registerScriptFile(Yii::app()->request->baseUrl . '/public/js/main.min.js',CClientScript::POS_END);
    Yii::app()->clientScript->registerScriptFile(Yii::app()->request->baseUrl . '/public/js/modules/userGroups/usuario/login.js',CClientScript::POS_END);
?>