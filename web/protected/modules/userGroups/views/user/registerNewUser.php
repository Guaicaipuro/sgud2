<?php
/**
 * Created by PhpStorm.
 * User: mari
 * Date: 25/11/15
 * Time: 01:56 PM
 */
?>

<?php
$form = $this->beginWidget('CActiveForm', array(
    'id' => 'register-new-user-form',
   // 'enableAjaxValidation' => false,
    //'htmlOptions' => array('data-form-type'=>'registerNewUser'),
   // 'action' => $this->createUrl('/userGroups/user/registerNewUser'),
    'enableAjaxValidation' => false,
));
?>
<fieldset>

    <div id="result-new">

    </div>


    <div class="block clearfix">
        <!-- Origen -->
        <span class="block input-icon input-icon-right col-md-12">
        <select required="required" class="input form-control" name="UserGroupsUser[origen]" id="UserGroupsUser_origen">
            <option value="">Origen</option>
            <option value="V">Venezolano</option>
            <option value="E">Extranjero</option>
        </select>
        <i class="icon-globe"></i>
        </span>
    </div>

    <div class="space-6"></div>

    <!-- Cédula -->
    <div class="block clearfix">
        <span class="block input-icon input-icon-right col-md-12">
            <input type="text" autocomplete="off" class="input form-control span-12" required="required" maxlength="8" placeholder="Cédula" name="UserGroupsUser[cedula]" id="UserGroupsUser_cedula" value="" x-moz-errormessage="Debe Ingresar su Número de Cédula" />
            <i class="icon-credit-card"></i>
        </span>
    </div>


    <div class="space-6"></div>

    <div class="block clearfix">
        <!-- Nombre y Apellido -->
        <span  class="input-icon input-icon-right col-md-12">
            <input type="text" autocomplete="off"  class="input form-control span-12" required="required" placeholder="Nombres" name="UserGroupsUser[nombre]" id="UserGroupsUser_nombre" value="" maxlength="40" x-moz-errormessage="Ingrese sus Nombres" />
            <i class="icon-user"></i>
        </span>
    </div>

    <div class="space-6"></div>

    <div class="block clearfix">
        <span  class="input-icon input-icon-right col-md-12">
            <input type="text" autocomplete="off"  class="input form-control span-12" required="required" placeholder="Apellidos" name="UserGroupsUser[apellido]" id="UserGroupsUser_apellido" value="" maxlength="40" x-moz-errormessage="Ingrese sus Apellidos">
            <i class="icon-user"></i>
        </span>
    </div>

    <div class="space-6"></div>

    <!-- Teléfono y Correo Electrónico -->
    <div class="block clearfix">
        <span  class="input-icon input-icon-right col-md-12">
            <input type="text" autocomplete="off"  class="input form-control span-12" required="required" placeholder="Teléfono Celular" name="UserGroupsUser[telefono_celular]" id="UserGroupsUser_telefono_celular" value="" maxlength="14" x-moz-errormessage="Ingrese un número telefónico celular de contacto" />
               <i class="icon-phone"></i>
        </span>
    </div>

    <div class="space-6"></div>

    <div class="block clearfix">
        <span  class="input-icon input-icon-right col-md-12">
            <input type="email" autocomplete="off"  class="input form-control span-12" required="required" placeholder="Correo Electrónico" name="UserGroupsUser[email]" id="UserGroupsUser_email" value="" maxlength="120" x-moz-errormessage="Ingrese un Correo Electrónico Válido - Es necesario para la activación del Usuario en el Sistema">
            <i class="icon-envelope"></i>
        </span>
    </div>

    <div class="space-6"></div>
    <?php
//    var_dump($tokenName);
//    var_dump($tokenValue);
    ?>
    <input type="hidden" name="<?php //echo $tokenName; ?>" id="<?php //echo $tokenName; ?>" value="<?php //echo $tokenValue; ?>" />
    <hr>

    <div class="space"></div>

    <div class="clearfix">
        <button type="submit" class="width-35 pull-right btn btn-sm btn-primary register-new" id="buttonSubmitId">
            Guardar
            &nbsp;
            <i class="fa fa-save"></i>
        </button>

    </div>

    <div class="space-4"></div>

</fieldset>
<?php $this->endWidget(); ?>