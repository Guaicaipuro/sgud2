<div class="container">
    <?php echo CHtml::encode($model->nombre.' '.$model->apellido); ?> usted ha recibido este correo para confirmar el restablecimiento de su Clave de Acceso el cual se ha completado exitósamente. Sus nuevos datos de acceso son:
    <hr>
    <b style="color: #990000;">Usuario:</b> <?php echo $model->username; ?><br/>
    <b style="color: #990000;">Clave:</b> <?php echo $clave; ?><br/>
    <hr>
</div>
