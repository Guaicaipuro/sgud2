<?php

class CambiarPerfilUG extends UserGroupsGroup {

    public static $cachePerfilIndex = "PERFILES:USERGROUPS";

    public function getPerfiles($nivelGrupoUsuario) {

        $sql = "SELECT groupname, id FROM seguridad.usergroups_group WHERE groupname != 'root' AND estatus = 'A' AND level <= :nivelGrupoUsuario ORDER BY groupname";
        $consulta = Yii::app()->db->createCommand($sql);
        $consulta->bindParam(":nivelGrupoUsuario", $nivelGrupoUsuario, PDO::PARAM_INT);
        $resultado = $consulta->queryAll();

        return $resultado;

    }
    
    /**
     * 
     * @return array Array asociativo con el resultado del proceso de registro externo de usuarios
     */
    public function cambiarPerfilUsuariosByCedulas($perfilId, $cedulas, $username, $userId, $userLevel, $ipAddress) {
        
        if(count($cedulas)>0){
            
            foreach ($cedulas as $cedula){

                $cedula = Helper::onlyNumericString($cedula);

                if(is_numeric($cedula) && strlen($cedula)>0){
                
                    $data = json_encode(array("cedula"=>$cedula, "group_id"=>$perfilId, "user_act_id"=>$userId,));

                    $sql="SELECT * FROM seguridad.cambio_perfil_por_cedula("
                            . ":group_id::integer, "
                            . ":cedula::integer, "
                            . ":ip_address::character varying, "
                            . ":user_level::integer, "
                            . ":username::character varying, "
                            . ":user_act_id::bigint, "
                            . ":data::text"
                            . ") "
                            . "AS f("
                            . "codigo varchar, "
                            . "resultado varchar, "
                            . "mensaje varchar, "
                            . "seccion smallint"
                            . ");";

                    $query = Yii::app()->db->createCommand($sql);

                    $query->bindParam(':cedula', $cedula , PDO::PARAM_INT) ;
                    $query->bindParam(':group_id', $perfilId , PDO::PARAM_STR) ;
                    $query->bindParam(':ip_address', $ipAddress , PDO::PARAM_STR) ;
                    $query->bindParam(':user_level', $userLevel , PDO::PARAM_STR) ;
                    $query->bindParam(':username', $username , PDO::PARAM_STR) ;
                    $query->bindParam(':user_act_id', $userId , PDO::PARAM_STR) ;
                    $query->bindParam(':data', $data , PDO::PARAM_STR) ;

                    $operacion = $query->queryRow();
                    $operacion['cedula'] = $cedula;

                    $result[] = $operacion;
                }
                else {
                    $result[] = array('codigo'=>'APERR1003', 'resultado'=>'error', 'mensaje'=>'El número de cédula debe contener sólo caractéres numéricos y no puede estar vacío, a la Cédula "'.$cedula.'" se le han detectado caractéres no numéricos.', 'seccion'=>__LINE__, 'cedula'=>$cedula);
                }                

            }

            return $result;

        }
        return null;
    }

    public static function model($className = __CLASS__) {
        return parent::model($className);
    }
}
