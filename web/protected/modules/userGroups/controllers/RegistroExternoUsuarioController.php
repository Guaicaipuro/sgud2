<?php

class RegistroExternoUsuarioController extends Controller
{
    //public $layout='//layouts/cuerpo';   // DEFINICIÒN DEL LAYOUT

    public $defaultAction = 'nuevo';
    
    public $layout = '//layouts/login';
    
    public $pathTimeOut = '/login/inactividad';
    
    // PARTE I
    static $_permissionControl = array(
        'read' => 'Ver el formulario de Registro Externo Usuarios',
        'write' => 'Registro Externo de Usuarios',
        'admin' => 'Administración de Registro Externo de Usuarios',
        'label' => 'Registro Externo de Usuarios del Sistema'
    );

    /**
     * @over
     * @return array action filters
     */
    public function filters() {
        return array(
            'userGroupsAccessControl', // perform access control for CRUD operations
            'postOnly + delete', // we only allow deletion via POST request
        );
    }
    
    public function actionNuevo() {

        $fechaHora = date('Y-m-d H:i:s');
        $securimage = new Securimage();
        $this->csrfTokenName = 'registroUsuarioToken';

        $modelPr = new UserGroupsRegistro('passRequest');
        $model = new UserGroupsRegistro('registroExterno');
        $grupos = array(); //CGrupoExterno::getData();
        $estados = array(); //CEstado::getData();
        
        $codigoDeSeguridad = '';
        $codigoDeSeguridadDesencriptado = '';
        
        if(Yii::app()->request->isPostRequest){

            if(Yii::app()->request->isAjaxRequest){

                if($this->hasPost('UserGroupsRegistro')){
                    
                    $response = array();
                    $codigoResultado = 'EA0000'; // E0001|S0000|W0004
                    $resultado = 'error'; // error|exito|alerta
                    $mensaje = 'Ha Ocurrido un Error en el Proceso. Comuniquese con el Administrador de la Aplicación.';
                    $tokenValue = $this->getCsrfToken('Registro de Usuarios Titulares. Chavez Vive!', $this->csrfTokenName);
                    $resultMail = null;

                    if ($this->validateCsrfToken()) {
                        
                        $model->attributes = $this->getPost('UserGroupsRegistro');
                        $model->setDataBeforeRegisterNewExternalUser();
                        $clave = $model->password;
                        
                        # Definir las reglas de validación... Sin consultar a base de datos.
                        if($model->validate()) {

                            $result = $model->registroExternoUsuario();

                            $codigoResultado = $result['codigo'];
                            $resultado = $result['resultado'];
                            $mensaje =  $result['mensaje'];
                            $talentoHumanoId = $result['talento_humano_id'];
                            $usuarioId = $result['user_id'];
                            $seccion = $result['seccion'];

                            if(strtolower($resultado) == 'exito') {

                                try{

                                    //Se elimina la Cache de los datos de Talento Humano si ya fueron cargados Previamente antes de crear su usuario.
                                    //Si no se elimina, cuando se carge de nuevo de la cache no se va a obtener el id del usuario correspondiente al talento humano de forma correcta.
                                    TalentoHumano::model()->deleteCacheDatosTalentoHumano($talentoHumanoId);

                                    
                                    
                                    $this->layout= '//layouts/correo';

                                    $codigoDeSeguridad = $this->getCodigoDeSeguridadEncriptado($model->username, $model->activation_code);
                                    $codigoDeSeguridadDesencriptado = $this->getCodigoDeSeguridadDesencriptado($codigoDeSeguridad);

                                    $contenido = $this->render('correoInicioRegistroExterno', array('model'=>$model, 'clave' => $clave, 'codigoDeSeguridad'=>$codigoDeSeguridad), true);
                                    $nombreDestinatario = $model->nombre." ".$model->apellido;
                                    $correDestinatario = strtolower($model->email);
                                    $fromName = Yii::app()->getParams()->appName;
                                    $from = yii::app()->getParams()->adminEmail;
                                    
                                    $resultMail = $this->sendEmail($correDestinatario, $nombreDestinatario, 'Correo de Activación de Usuario', $contenido, $from, $fromName);

                                } catch (Exception $ex) {
                                    
                                    $model->marcarCorreoNoEnviado($model->id);
                                    $codigoResultado = 'EA0003';
                                    $resultado = 'alerta';
                                    $mensaje = $this->renderPartial("//serverError", array('mensaje'=> 'El registro del usuario se ha efectuado exitósamente. Pero, ha ocurrido un error al enviar el correo. El mismo ha caido en la lista de correos para ser enviado más tarde.', 'error'=>$ex->getMessage()), true);
                                }

                            }
                        }
                        else{
                            $codigoResultado = 'EA0002';
                            $resultado = 'error';
                            $mensaje = $this->renderPartial('//errorForm', array('model'=>$model), true);
                        }
                    }
                    else{
                        $codigoResultado = 'EA0001';
                        $resultado = 'error';
                        $mensaje = 'El Código de Autenticación del Formulario no ha podido ser validado, ingrese los datos requeridos e inténtelo de nuevo.';
                    }

                    // $response = array('tokenCsrf'=>$tokenValue, 'codigoResultado'=>$codigoResultado, 'resultado'=>$resultado, 'mensaje'=>$mensaje, 'resultMail'=>$resultMail, 'codigoDeActivacionEncriptado'=>$codigoDeSeguridad, 'username'=>$model->username, 'codigoDeActivacion'=>$model->activation_code, 'CodigoDeActivacionDesencriptado'=>$codigoDeSeguridadDesencriptado);
                    $response = array('tokenCsrf'=>$tokenValue, 'codigoResultado'=>$codigoResultado, 'resultado'=>$resultado, 'mensaje'=>$mensaje, 'resultMail'=>$resultMail, 'username'=>$model->username);
                    $this->jsonResponse($response);
                    Yii::app()->end();

                }
                else{
                    throw new CHttpException(402, 'No se han recibido los datos necesarios para efectuar esta acción.');
                }

            }
            else{
                throw new CHttpException(403, 'No está permitido el envío de información mediante esta vía. Permiso denegado.');
            }

        }
        
        if(!Yii::app()->request->isAjaxRequest){
            $tokenValue = $this->getCsrfToken('Registro de Usuarios Titulares. Chavez Vive!', $this->csrfTokenName);
            $this->pageTitle = 'Registro de Nuevo Usuario';
            $this->render('registro', array('model'=>$model, 'modelPr'=>$modelPr,'gruposExternos'=>$grupos, 'estados'=>$estados, 'tokenName' => $this->csrfTokenName, 'tokenValue' => $tokenValue));
        }
        else{
            throw new CHttpException(403, 'No está permitido la petición de esta acción mediante esta vía. Permiso denegado.');
        }
    }
    
    public function actionActivarCuenta($cod){
        
        $codigoDeSeguridadEncriptadoCifrado = str_replace(' ', '+', str_replace('**', '/', $cod));
        
        if(strlen($codigoDeSeguridadEncriptadoCifrado)>0){

            $codigo = $this->getCodigoDeSeguridadDesencriptado($codigoDeSeguridadEncriptadoCifrado);
            $arrCodigo = explode('|', $codigo);
            
            if(count($arrCodigo)==2){
                
                $username = $arrCodigo[0];
                $codigoDeActivacion = $arrCodigo[1];
                $fechaActual = date('Y-m-d H:i:s');
                $modulo = 'usergroups.registroExternoUsuario.activarCuenta';
                $ipAddress = Helper::getRealIP();
                
                $resultado = UserGroupsRegistro::model()->activarRegistroExternoUsuario($codigoDeActivacion, $username, $fechaActual, $modulo, $ipAddress);

                $this->render('resultadoActivacionUsuario', array('resultado'=>$resultado));

            }
            else{
               throw new CHttpException(400, 'ERROR A10002: No se han proporcionado los datos necesarios para efectuar esta operación.'); 
            }

        }
        else{
            throw new CHttpException(400, 'ERROR A10001: No se han proporcionado los datos necesarios para efectuar esta operación.');
        }
        
    }
    
    /**
     * 
     * @param string $username
     * @param string $activationCode md5
     * @return string base64
     */
    private function getCodigoDeSeguridadEncriptado($username, $activationCode){
        $iv_size = mcrypt_get_iv_size(MCRYPT_RIJNDAEL_128, MCRYPT_MODE_CBC);
        $iv = mcrypt_create_iv($iv_size, MCRYPT_RAND);
        $codigoDeSeguridad = $username.'|'.$activationCode;
        $codigoDeSeguridadEncriptado = mcrypt_encrypt(MCRYPT_RIJNDAEL_128, Yii::app()->getParams()->keySecret, $codigoDeSeguridad, MCRYPT_MODE_CBC, $iv);
        $codigoDeSeguridadEncriptado = $iv . $codigoDeSeguridadEncriptado;
        $codigoDeSeguridadEncriptadoCifrado = base64_encode($codigoDeSeguridadEncriptado);
        return $codigoDeSeguridadEncriptadoCifrado;
    }

    /**
     * 
     * @param string $codigoDeSeguridadEncriptadoCifrado base64 encriptado
     * @return string
     */
    private function getCodigoDeSeguridadDesencriptado($codigoDeSeguridadEncriptadoCifrado){
        $iv_size = mcrypt_get_iv_size(MCRYPT_RIJNDAEL_128, MCRYPT_MODE_CBC);
        $codigoDeSeguridadEncriptadoDecoded = base64_decode($codigoDeSeguridadEncriptadoCifrado);
        $iv = substr($codigoDeSeguridadEncriptadoDecoded, 0, $iv_size);
        $codigoDeSeguridadEncriptado = substr($codigoDeSeguridadEncriptadoDecoded, $iv_size);
        $codigoDeSeguridad = mcrypt_decrypt(MCRYPT_RIJNDAEL_128, Yii::app()->getParams()->keySecret, $codigoDeSeguridadEncriptado, MCRYPT_MODE_CBC, $iv);
        return trim($codigoDeSeguridad);
    }
    
    public function actionResetTokenCsrf($tokenName){
        if(strlen($tokenName)>0){
            $this->csrfTokenName = $tokenName;
        }
        $this->jsonResponse(array('tokenCsrf'=>$this->getCsrfToken('Registro de Usuarios Titulares. Chavez Vive!', $this->csrfTokenName)));
    }
    
}
