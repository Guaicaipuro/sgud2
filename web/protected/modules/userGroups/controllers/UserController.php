<?php

class UserController extends Controller {

    public $layout = '//layouts/main';

    /**
     * @var mixed tooltip for the permission menagement $fecha_ nombre passRequest
     */
    /* public static $_permissionControl = array(
      'write'=>'can invite other users.',
      'admin'=>'can edit other users, ban them and approve their registrations.',
      ); */
    //Para que no aparezca en el admin (root) de userGroups
    const TRAZA = "INSERT INTO auditoria.traza (username, fecha_hora, ip_maquina, tipo_transaccion, modulo) VALUES (:username, :fecha_hora, :ip_maquina, :tipo_transaccion, :modulo);";

    public static $_permissionControl = false;

    /**
     * @return array action filters
     */
    public function filters() {
        return array(
            'userGroupsAccessControl', // perform access control for CRUD operations
        );
    }

    /**
     * Specifies the access control rules.
     * This method is used by the 'accessControl' filter.
     * @return array access control rules
     */
    public function accessRules() {
        return array(
            array('allow', // just guest can perform 'activate', 'login' and 'passRequest' actions
                'actions' => array('activate', 'login', 'passRequest'),
                'ajax' => false,
                'users' => array('?'),
            ),
            array('allow', // just guest can perform 'activate', 'login' and 'passRequest' actions
                'actions' => array('obtenerDatosPersona','registerNewUser'),
                'ajax' => true,
                'users' => array('?'),
            ),
            array('allow', // captchas can be loaded just by guests
                'actions' => array('captcha'),
                'expression' => 'UserGroupsConfiguration::findRule("registration")',
                'users' => array('?'),
            ),
            array('allow', // just guest can perform registration actions and just if it is enabled
                'actions' => array('register'),
                'ajax' => false,
                'expression' => 'UserGroupsConfiguration::findRule("registration")',
                'users' => array('?'),
            ),
            array('allow', // actions users can access while in recovery mode
                'actions' => array('recovery', 'logout'),
                'users' => array('#'),
            ),
            array('allow', // allow authenticated user to perform 'logout' actions
                'actions' => array('logout'),
                'users' => array('@'),
            ),
            array('allow', // allow logged user to access the userlist page if the have admin rights on users or if the list is public
                'actions' => array('index'),
                'expression' => 'UserGroupsConfiguration::findRule("public_user_list") || Yii::app()->user->pbac("userGroups.user.admin")',
                'users' => array('@'),
            ),
            array('allow', // allow guest to view users profiles according to the configuration
                'actions' => array('view'),
                'expression' => 'UserGroupsConfiguration::findRule("public_profiles")',
                'pbac' => array('write'),
            ),
            array('allow', // allow logged user to view other users profiles according to the configuration and always their own
                'actions' => array('view'),
                'expression' => 'strtolower(Yii::app()->user->name) === (isset($_GET["u"]) ? strtolower($_GET["u"]) : strtolower(Yii::app()->user->name))',
                'pbac' => array('write'),
            ),
            array('allow', // allow user with user admin permission to view every profile, approve, ban and invite users
                'actions' => array('invite'),
                'pbac' => array('write'),
            ),
            array('allow', // allow user with user admin permission to view every profile, approve, ban and invite users
                'actions' => array('view', 'approve', 'ban', 'invite'),
                'pbac' => array('admin', 'admin.admin'),
            ),
            array('allow', // allow a user tu open an update view just on their own accounts
                'actions' => array('update'),
                'expression' => '$_GET["id"] == Yii::app()->user->id',
                'ajax' => true,
            ),
            array('allow', // allow user with admin permission to perform any action
                'pbac' => array('admin', 'admin.admin'),
                'ajax' => true,
            ),
            array('deny', // deny all users
                'users' => array('*'),
            ),
        );
    }

    /**
     * override of the actions method to implement captcha
     */
    public function actions() {
        return array(
            // captcha action renders the CAPTCHA image displayed on the contact page
            'captcha' => array(
                'class' => 'CCaptchaAction',
                'backColor' => 0xFFFFFF,
            ),
        );
    }

    /**
     * Lists all users.
     */
    public function actionIndex() {
        $model = new UserGroupsUser('search');
        $model->unsetAttributes();  // clear any default values
        if (isset($_GET['UserGroupsUser']))
            $model->attributes = $_GET['UserGroupsUser'];

        if (Yii::app()->request->isAjaxRequest)
            $this->renderPartial('index', array('model' => $model,), false, true);
        else
            $this->render('index', array('model' => $model,), false, true);
    }

    /**
     * render a user profile
     */
    public function actionView() {
        // load the user profile according to the request
        if (isset($_GET['u'])) {
            // look for the right user criteria to use according to the viewer permissions
            if (Yii::app()->user->pbac(array('user.admin', 'admin.admin')))
                $criteria = array('username' => $_GET['u']);
            else
                $criteria = array('username' => $_GET['u'], 'status' => UserGroupsUser::ACTIVE);
            // load the profile
            $model = UserGroupsUser::model()->findByAttributes($criteria);
            if ($model === null || ($model->relUserGroupsGroup->level > Yii::app()->user->level && !UserGroupsConfiguration::findRule('public_profiles')))
                throw new CHttpException(404, Yii::t('userGroupsModule.general', 'The requested page does not exist.'));
        } else
            $model = $this->loadModel(Yii::app()->user->id);

        // load the profile extensions
        $profiles = array();
        $profile_list = Yii::app()->controller->module->profile;


        foreach ($profile_list as $p) {
            // check if the profile data exist on the current user, otherwise
            // create an instance of the profile extension
            $relation = "rel$p";

            if (!$model->$relation instanceof CActiveRecord)
                $p_instance = new $p;
            else
                $p_instance = $model->$relation;

            // check if the profile extension is supporting profile views
            $views = $p_instance->profileViews();
            if (isset($views[UserGroupsUser::VIEW])) {
                $profiles[] = array('view' => $views[UserGroupsUser::VIEW], 'model' => $p_instance);
            }
        }


        if (Yii::app()->request->isAjaxRequest || isset($_GET['_isAjax']))
            $this->renderPartial('view', array('model' => $model, 'profiles' => $profiles), false, true);
        else
            $this->render('view', array('model' => $model, 'profiles' => $profiles));
    }

    /**
     * user registration
     */
    public function actionRegister() {
        $model = new UserGroupsUser('registration');

        // set the profile extension array
        $profiles = array();
        $profile_list = Yii::app()->controller->module->profile;

        foreach ($profile_list as $p) {
            // create an instance of the profile extension
            $p_instance = new $p('registration');
            // check if the profile extension is supporting registration
            $views = $p_instance->profileViews();
            if (isset($views[UserGroupsUser::REGISTRATION])) {
                $profiles[] = array('view' => $views[UserGroupsUser::REGISTRATION], 'model' => $p_instance);
            }
        }

        if (isset($_POST['UserGroupsUser'])) {
            $model->attributes = $_POST['UserGroupsUser'];
            // set validation for additional fields
            foreach ($profiles as &$p) {

                if (isset($_POST[get_class($p['model'])]))
                    $p['model']->attributes = $_POST[get_class($p['model'])];

                if (!$p['model']->validate())
                    $error = true;
            }
            if ($model->validate() && !isset($error)) {
                if ($model->save()) {
                    // save the related profile extensions
                    foreach ($profiles as $p) {
                        $p['model']->ug_id = $model->id;
                        $p['model']->save();
                    }
                    $this->redirect(Yii::app()->baseUrl . '/userGroups');
                }
            }
        }

        $this->render('register', array(
            'model' => $model,
            'profiles' => $profiles,
        ));
    }

    /**
     * user invite form
     */
    public function actionInvite() {
        $model = new UserGroupsUser('invitation');

        $this->performAjaxValidation($model);

        if (isset($_POST['UserGroupsUser'])) {
            $model->attributes = $_POST['UserGroupsUser'];
            if ($model->validate()) {
                if ($model->save()) {
                    $mail = new UGMail($model, UGMail::INVITATION);
                    $mail->send();
                } else
                    Yii::app()->user->setFlash('user', Yii::t('userGroupsModule.general', 'An Error Occurred. Please try later.'));
                $this->redirect(Yii::app()->baseUrl . '/userGroups');
            }
        }

        if (Yii::app()->request->isAjaxRequest)
            $this->renderPartial('invite', array('model' => $model,), false, true);
        else
            $this->render('invite', array('model' => $model,));
    }

    /**
     * Updates a user data
     * if the update is succefull the user profile will be reloaded
     * You can change password or mail indipendently
     * @param integer $id the ID of the model to be updated
     */
    public function actionUpdate($id) {
        $miscModel = $this->loadModel($id, 'changeMisc');
        $passModel = clone $miscModel;
        $passModel->setScenario('changePassword');
        $passModel->password = NULL;

        // pass the models inside the array for ajax validation
        $ajax_validation = array($miscModel, $passModel);

        // load additional profile models
        $profile_models = array();
        $profiles = $this->module->profile;
        foreach ($profiles as $p) {
            $external_profile = new $p;
            // check if the loaded profile has an update view
            $external_profile_views = $external_profile->profileViews();
            if (array_key_exists(UserGroupsUser::EDIT, $external_profile_views)) {
                // load the model data
                $loaded_data = $external_profile->findByAttributes(array('ug_id' => $id));
                $external_profile = $loaded_data ? $loaded_data : $external_profile;
                // set the scenario
                $external_profile->setScenario('updateProfile');
                // load the models inside both the ajax validation array and the profile models
                // array to pass it to the view
                $profile_models[$p] = $external_profile;
                $ajax_validation[] = $external_profile;
            }
        }

        // perform ajax validation
        $this->performAjaxValidation($ajax_validation);

        // check if an additional profile model form was sent
        if ($form = array_intersect_key($_POST, array_flip($profiles))) {
            $model_name = key($form);
            $form_values = reset($form);
            // load the form values into the model
            $profile_models[$model_name]->attributes = $form_values;
            $profile_models[$model_name]->ug_id = $id;

            // save the model
            if ($profile_models[$model_name]->save()) {
                Yii::app()->user->setFlash('user', Yii::t('userGroupsModule.general', 'Data Updated Succefully'));
                $this->redirect(Yii::app()->baseUrl . '/userGroups?_isAjax=1&u=' . $passModel->username);
            } else
                Yii::app()->user->setFlash('user', Yii::t('userGroupsModule.general', 'An Error Occurred. Please try later.'));
        }

        if (isset($_POST['UserGroupsUser']) && isset($_POST['formID'])) {
            // pass the right model according to the sended form and load the permitted values
            if ($_POST['formID'] === 'user-groups-password-form') {
                $model = $passModel;
                $model->old_password = $_POST['UserGroupsUser']['old_password'];
                $model->password = $_POST['UserGroupsUser']['password'];
                $model->password_confirm = $_POST['UserGroupsUser']['password_confirm'];
                //$model->question = $_POST['UserGroupsUser']['question'];
                //$model->answer = $_POST['UserGroupsUser']['answer'];
            } else if ($_POST['formID'] === 'user-groups-misc-form') {
                $model = $miscModel;
                $model->email = $_POST['UserGroupsUser']['email'];
                $model->home = $_POST['UserGroupsUser']['home'];
            }

            if ($model->validate()) {
                if ($model->save()) {
                    Yii::app()->user->setFlash('user', Yii::t('userGroupsModule.general', 'Data Updated Succefully'));
                    $this->redirect(Yii::app()->baseUrl . '/userGroups?_isAjax=1&u=' . $model->username);
                } else
                    Yii::app()->user->setFlash('user', Yii::t('userGroupsModule.general', 'An Error Occurred. Please try later.'));
            }
        }

        $this->renderPartial('update', array('miscModel' => $miscModel, 'passModel' => $passModel, 'profiles' => $profile_models), false, true);
    }

    /**
     * user activation view
     */
    public function actionActivate() {
        $activeModel = new UserGroupsUser('activate');
        $requestModel = new UserGroupsUser('mailRequest');

        if (isset($_POST['UserGroupsUser']) || isset($_GET['UserGroupsUser'])) {
            if (isset($_GET['UserGroupsUser']) || $_POST['id'] === 'user-groups-activate-form')
                $model = $activeModel;
            else if (($_POST['id'] === 'user-groups-request-form'))
                $model = $requestModel;

            if (isset($_POST['UserGroupsUser']))
                $model->attributes = $_POST['UserGroupsUser'];
            else
                $model->attributes = $_GET['UserGroupsUser'];


            if ($model->validate()) {
                if (isset($_GET['UserGroupsUser']) || $_POST['id'] === 'user-groups-activate-form') {
                    $model->login('recovery');
                    $this->redirect(Yii::app()->baseUrl . '/userGroups/user/recovery');
                } else {
                    $userModel = UserGroupsUser::model()->findByAttributes(array('email' => $model->email));
                    $mail = new UGMail($userModel, UGMail::ACTIVATION);
                    $mail->send();
                    $this->redirect(Yii::app()->baseUrl . '/userGroups/user/activate');
                }
            }
        }



        $this->render('activate', array(
            'activeModel' => $activeModel,
            'requestModel' => $requestModel
        ));
    }

    /**
     * approve the user account
     */
    public function actionApprove() {
        if (isset($_POST['UserGroupsApprove'])) {
            $model = $this->loadModel((int) $_POST['UserGroupsApprove']['id']);
            $model->status = UserGroupsUser::ACTIVE;
            if ($model->save())
                Yii::app()->user->setFlash('user', Yii::t('userGroupsModule.admin', '{username}\'s account is now active.', array('{username}' => $model->username)));
            else
                Yii::app()->user->setFlash('user', Yii::t('userGroupsModule.general', 'An Error Occurred. Please try later.'));
            $this->redirect(Yii::app()->baseUrl . '/userGroups?u=' . $model->username);
        }
    }

    /**
     * form for new pass request
     */
    public function actionPassRequest() {
        $this->layout = '//layouts/login';

        $model_pr = new UserGroupsUser('passRequest');

        if (isset($_POST['UserGroupsUser'])) {
            $model_pr->attributes = $_POST['UserGroupsUser'];
            if ($model_pr->validate()) {
                $model_pr = UserGroupsUser::model()->findByAttributes(array('username' => $_POST['UserGroupsUser']['username']));
                $model_pr->scenario = 'passRequest';
                if ($model_pr->save()) {

                    $mail = new UGMail($model_pr, UGMail::PASS_RESET);
                    $mail->send();
                } else {
                    Yii::app()->user->setFlash('success', Yii::t('userGroupsModule.general', 'An Error Occurred. Please try later.'));
                }
                $this->redirect(Yii::app()->baseUrl . '/');
            }
        }

        $this->render('passRequestResult', array('model_pr' => $model_pr));
    }

    /**
     * ban user from the system
     */
    public function actionBan() {
        // load the user data
        $model = $this->loadModel((int) $_POST['UserGroupsBan']['id'], 'ban');
        // check if you are trying to ban a user with an higher level
        if ($model->relUserGroupsGroup->level > Yii::app()->user->level) {
            Yii::app()->user->setFlash('user', Yii::t('userGroupsModule.admin', 'You cannot ban a user with a level higher then yours.'));
        } else {
            $model->ban = date('Y-m-d H:i:s', time() + ($_POST['UserGroupsBan']['period'] * 86400));
            $model->ban_reason = $_POST['UserGroupsBan']['reason'];
            $model->status = UserGroupsUser::BANNED;
            if ($model->save()) {
                Yii::app()->user->setFlash('user', Yii::t('userGroupsModule.admin', '{username}\'s account is banned untill {day}.', array('{username}' => $model->username, '{day}' => $model->ban)));
            } else {
                Yii::app()->user->setFlash('user', Yii::t('userGroupsModule.general', 'An Error Occurred. Please try later.'));
            }
        }
        $this->redirect(Yii::app()->baseUrl . '/userGroups?u=' . $model->username);
    }

    public function actionLogin() {

        //var_dump($_SESSION);

        $fecha_hora = date('Y-m-d H:i:s');

        $securimage = new Securimage();

        $this->csrfTokenName = 'loginToken';

        if (isset($_SESSION['_items_menu'])) {
            unset($_SESSION['_items_menu']);
        }

        $this->layout = '//layouts/login'; //Para el layout con cabecera CANTV

        $model = new UserGroupsUser('login');

        $model_pr = new UserGroupsUser('passRequest');

        // if it is ajax validation request
        if (isset($_POST['ajax']) && $_POST['ajax'] === 'login-form') {
            echo CActiveForm::validate($model);
            Yii::app()->end();
        }

        if ($this->validateCsrfToken()) {
            // collect user input data
            if (isset($_POST['UserGroupsUser']) && array_key_exists('verifyCode', $_POST['UserGroupsUser']) && (trim($_POST['UserGroupsUser']['verifyCode'])) !== null) {
                $model->attributes = $_POST['UserGroupsUser'];
                // validate user input and redirect to the previous page if valid
                if ($model->validate() && $model->login()) {
                    $Utiles = new Utiles();
                    $transaccion = "Inicio de Sesion";
                    $modulo = "UserGroups.UserController.Login";
                    $Utiles->traza($transaccion, $modulo, $fecha_hora);
                    $this->redirect(Yii::app()->user->returnUrl);
                }
            }
        } else {
            Yii::app()->session->remove($this->csrfTokenName);
        }

        $token = $this->getCsrfToken('Chávez Vive! MPPE y MPPCTI');
        // display the login form
        if (Yii::app()->request->isAjaxRequest || isset($_GET['_isAjax'])) {
            $this->renderPartial('/user/login', array('model' => $model, 'model_pr' => $model_pr, 'tokenName' => $this->csrfTokenName, 'tokenValue' => $token,));
        } else {
            $this->render('/user/login', array('model' => $model, 'model_pr' => $model_pr, 'tokenName' => $this->csrfTokenName, 'tokenValue' => $token,));
        }
    }

    public function actionRegisterNewUser(){

//        var_dump($_REQUEST);
//        die();

        if($this->hasPost('UserGroupsUser')){

            $model = new UserGroupsUser('create');
            $model->attributes = $this->getRequest('UserGroupsUser');
           // var_dump($model);
           // $usuarios = Ruta::model()->getJefeMP();
            $fecha = date('Y-m-d H:i:s');
            $mensaje = '';

                $usuario = 1;
                $group_id = 55;//PATRULLERO
                $status = 4;
                $home = '/site';
                $cambio_clave = 0;
                $nombre = strtoupper(trim(isset($_REQUEST['UserGroupsUser']['nombre'])))?$_REQUEST['UserGroupsUser']['nombre']:'';
                $apellido = strtoupper(trim(isset($_REQUEST['UserGroupsUser']['apellido'])))?$_REQUEST['UserGroupsUser']['apellido']:'';
                $cedula = (trim(isset($_REQUEST['UserGroupsUser']['cedula'])))?$_REQUEST['UserGroupsUser']['cedula']:null;
                $email = strtoupper(trim(isset($_REQUEST['UserGroupsUser']['email'])))?$_REQUEST['UserGroupsUser']['email']:'';
                $origen = strtoupper(trim(isset($_REQUEST['UserGroupsUser']['origen'])))?$_REQUEST['UserGroupsUser']['origen']:'';
                $username = substr($apellido,0,1).$cedula;
                $password = UserGroupsUser::model()->getRandomPassword();
                $tlf_movil = Utiles::onlyNumericString((trim(isset($_REQUEST['UserGroupsUser']['telefono_celular'])))?$_REQUEST['UserGroupsUser']['telefono_celular']:'');

                $validarExisteUser = UserGroupsUser::model()->validarUser($origen, $cedula);
                if($validarExisteUser == false) {
//var_dump($group_id,$username,$password,$email,$home,$status,$fecha,$nombre,$apellido,$cedula,$usuario,$origen,$tlf_movil,$cambio_clave);die();
                    $transaction = Yii::app()->db->beginTransaction();
                    try {

                        $model->group_id = $group_id;
                        $model->username = $username;
                        $model->password = $password;
                        $model->email = $email;
                        $model->home = $home;
                        $model->status = $status;
                        $model->creation_date = $fecha;
                        $model->nombre = $nombre;
                        $model->apellido = $apellido;
                        $model->cedula = $cedula;
                        $model->user_ini_id = $usuario;
                        $model->origen = $origen;
                        $model->telefono_celular = $tlf_movil;
                        $model->cambio_clave = $cambio_clave;

                        $model->setScenario('register-new');

                        if ($model->validate()) {
                            if ($model->save()) {
                                $transaction->commit();
                                $this->registerLog('ESCRITURA', 'userGroups.user.actionRegisterNewUser', 'EXITOSO', 'El registro del usuario se ha efectuado exitósamente. Data-> '.json_encode($model->attributes));
                                $mensaje = 'Estimado usuario su proceso se ejecuto con exito, verifique su correo para visualizar su usuario y clave.';
                                $title = 'Notificación de Exito';
                                echo json_encode(array('statusCode' => 'SUCCESS', 'mensaje' => $mensaje, 'title' => $title));

                                $from = 'soporte_gescolar@me.gob.ve';
                                $fromName = 'Gescolar ';
                                $emailOculto = 'gescolar.mppe@gmail.com';
                                $msjOculto = 'Sistema SGUD';
                                $subject = "Notificación exitosa del registro del usuario $apellido $nombre";
                                $info = 'del registro del usuario $apellido $nombre';
                                $mensaje = "Se notifica que se realizó satisfactoriamente el registro del usuario, perteneciente de la cédula de identidad $origen - $cedula correspondiente a $apellido $nombre.
                                                <br>
                                                <br>
                                                Datos de acceso:<br>
                                                Usuario: <b>$username</b>,<br>
                                                Clave: <b>$password</b>,<br>
                                                Correo: <b>$email</b>,<br>
                                                Página: http://elecciones.dev/login
                                            ";
                                $correos = array(
                                    0=>array(
                                        'correo' => $email,
                                        'nombre' => $nombre,
                                        'apellido' => $apellido)
                                );
                                Utiles::enviarCorreo2($correos, $from, $fromName, $emailOculto, $msjOculto, $subject, $mensaje, $info);
                               // $this->redirect(array('edicion','id'=>base64_encode($model->id),'plantel'=>base64_encode($plantel_id)));
                                Yii::app()->end();
                            } else {
                                $this->registerLog('ESCRITURA', 'userGroups.user.actionRegisterNewUser', 'NO EXITOSO', 'El registro del usuario no se ha efectuado exitósamente. Data-> '.json_encode($model->attributes));
                                $mensaje = 'Estimado usuario no se pudo registrar el usuario, recargue la página e intente nuevamente.';
                                $title = 'Notificación de Error';
                                echo json_encode(array('statusCode' => 'ERROR', 'mensaje' => $mensaje, 'title' => $title));
                                Yii::app()->end();
                            }
                        } else {
                            $mensaje = CHtml::errorSummary($model);
                            $title = 'Notificación de Error';
                            echo json_encode(array('statusCode' => 'ERROR', 'mensaje' => $mensaje, 'title' => $title));
                            Yii::app()->end();
                        }
                    } catch (Exception $e) {
//                        var_dump($e);
//                        die();
                        $transaction->rollback();
                        $this->registerLog('ESCRITURA', 'userGroups.user.actionRegisterNewUser', 'NO EXITOSO', "Ocurrio un error,el registro del usuario no se ha efectuado exitósamente. Error: {$e->getMessage()} Data-> ".json_encode($model->attributes));
                        $mensaje = 'Ocurrio un error, Estimado usuario no se pudo registrar el usuario, recargue la página e intente nuevamente.';
                        $title = 'Notificación de Error';
                        echo json_encode(array('statusCode' => 'ERROR', 'mensaje' => $mensaje, 'title' => $title));
                        Yii::app()->end();
                    }
                }else{
                    $ori = $validarExisteUser['origen'];
                    $ced = $validarExisteUser['cedula'];
                    $nom = $validarExisteUser['nombre'];
                    $ape = $validarExisteUser['apellido'];
                    $username = $validarExisteUser['username'];
                    $title = 'Notificación de Error';
                    $mensaje = "Ocurrio un error, la persona $ape $nom, perteneciente a la cédula de identidad $ori - $ced, esta intentando registrarse pero ya se encuentra registrado con el usuario <b>$username</b>";
                    echo json_encode(array('statusCode' => 'ERROR', 'mensaje' => $mensaje, 'title' => $title));
                    Yii::app()->end();
                }

        }

    }


    /* VALIDAR DATOS PERSONA CON EL SAIME */
    public function actionObtenerDatosPersona() {

        if (Yii::app()->request->isAjaxRequest) {

            $origen = $this->getPost('origen');
            $cedula = $this->getPost('cedula');

            $datosPersona = '';
            $nombresPersona = '';
            $apellidosPersona = '';
            if (in_array($origen, array('V', 'E'))) {
                if (in_array($origen, array('V', 'E'))) {
                    if (strlen((string) $cedula) > 9) {
                        $mensaje = 'Estimado usuario el campo cédula no puede superar los diez (8) caracteres.';
                        $title = 'Notificación de Error';
                        echo json_encode(array('statusCode' => 'ERROR', 'mensaje' => $mensaje, 'title' => $title));
                        Yii::app()->end();
                    } else {
                        if (!is_numeric($cedula)) {
                            $mensaje = 'Estimado usuario el campo cédula debe poseer solo caracteres numéricos.';
                            $title = 'Notificación de Error';
                            echo json_encode(array('statusCode' => 'ERROR', 'mensaje' => $mensaje, 'title' => $title));
                            Yii::app()->end();
                        }
                    }
                }

                $datosPersona = UserGroupsUser::model()->busquedaSaimeMixta($origen, $cedula);
                if ($datosPersona) {
                    (isset($datosPersona['nombre'])) ? $nombresPersona = $datosPersona['nombre'] : $nombresPersona = '';
                    (isset($datosPersona['apellido'])) ? $apellidosPersona = $datosPersona['apellido'] : $apellidosPersona = '';
                    (isset($datosPersona['sexo'])) ? $sexoPersona = $datosPersona['sexo'] : $sexoPersona = '';
                    (isset($datosPersona['fecha_nacimiento'])) ? $fecha_nacimiento = date("d-m-Y", strtotime($datosPersona['fecha_nacimiento'])) : $fecha_nacimiento = '';
                    (isset($datosPersona['edad'])) ? $edadPersona = $datosPersona['edad'] : $edadPersona = '';

                    if($edadPersona >= 18){
                        echo json_encode(array('statusCode' => 'SUCCESS', 'nombres' => $nombresPersona, 'apellidos' => $apellidosPersona));
                    }else{
                        $mensaje = 'La cédula ' . $origen . '-' . $cedula . ' pertenece a una persona menor de edad, para votar debe tener al menos 18 años.';
                        $title = 'Notificación de Error';
                        echo json_encode(array('statusCode' => 'ERROR', 'mensaje' => $mensaje, 'title' => $title));
                    }

                    Yii::app()->end();
                } else {
                    $mensaje = 'La cédula ' . $origen . '-' . $cedula . ' no se encuentra registrado en el saime, por favor contacte al personal de soporte.';
                    $title = 'Notificación de Error';
                    echo json_encode(array('statusCode' => 'ERROR', 'mensaje' => $mensaje, 'title' => $title));
                    Yii::app()->end();
                }
            } else {
                $mensaje = 'Estimado usuario, se han enviado valores no permitidos en el campo origen.';
                $title = 'Notificación de Error';
                echo json_encode(array('statusCode' => 'ERROR', 'mensaje' => $mensaje, 'title' => $title));
                Yii::app()->end();
            }
        } else {
            throw new CHttpException(403, 'No está permitido efectuar la petición de este recurso por esta vía.');
        }
    }

    /* FIN */

    /**
     * login in recovery mode
     */
    public function actionRecovery() {
        $model = $this->loadModel(Yii::app()->user->id, 'recovery');

        // if user and password are already setted and so question and answer no form will be prompted
        if (strpos($model->username, '_user') !== 0 && $model->password && $model->salt && $model->question && $model->answer) {
            $model->scenario = 'swift_recovery';
            if (!$model->save())
                Yii::app()->user->setFlash('success', Yii::t('userGroupsModule.general', 'An Error Occurred. Please try later.'));
            $this->redirect(Yii::app()->baseUrl . '/logout');
        }

        // empty the password field
        $model->password = NULL;

        $this->performAjaxValidation($model);

        if (isset($_POST['UserGroupsUser'])) {
            $model->attributes = $_POST['UserGroupsUser'];
            if ($model->validate()) {
                if (!$model->save())
                    Yii::app()->user->setFlash('success', Yii::t('userGroupsModule.general', 'An Error Occurred. Please try later.'));
                $this->redirect(Yii::app()->baseUrl . '/logout');
            }
        }

        $this->render('recovery', array(
            'model' => $model,
        ));
    }

    /**
     * Logs out the current user and redirect to homepage.
     */
    public function actionLogout() {
        $fecha_hora = date('Y-m-d H:i:s');
        // keep the flash messages flowing
        if (Yii::app()->user->hasFlash('success')) {
            $message = Yii::app()->user->getFlash('success');
            Yii::app()->request->cookies['success'] = new CHttpCookie('success', $message);
        }
        $Utiles = new Utiles();
        $transaccion = "Cerrar Sesion";
        $modulo = "UserController.Logout";
        $Utiles->traza($transaccion, $modulo, $fecha_hora);
        Yii::app()->user->logout();
        $this->redirect(Yii::app()->baseUrl . '/login');
    }

    /**
     * Returns the data model based on the primary key given in the GET variable.
     * Optionally sets a scenario
     * If the data model is not found, an HTTP exception will be raised.
     * @param integer the ID of the model to be loaded
     * @param string the scenario to apply to the model
     */
    public function loadModel($id, $scenario = false) {
        $model = UserGroupsUser::model()->findByPk((int) $id);
        if ($model === null || ($model->relUserGroupsGroup->level > Yii::app()->user->level && !UserGroupsConfiguration::findRule('public_profiles')))
            throw new CHttpException(404, Yii::t('userGroupsModule.general', 'The requested page does not exist.'));
        if ($scenario)
            $model->setScenario($scenario);
        return $model;
    }

    /**
     * Performs the AJAX validation.
     * @param CModel the model to be validated
     */
    protected function performAjaxValidation($model) {
        if (isset($_POST['ajax'])) {
            echo CActiveForm::validate($model);
            Yii::app()->end();
        }
    }

}
