<?php
/* @var $this VotanteController */
/* @var $model Votante */
/* @var $form CActiveForm */
?>
<div class="col-xs-12">
    <div class="row-fluid">

        <div class="tabbable">

            <ul class="nav nav-tabs">
                <li class="active"><a href="#datosGenerales" data-toggle="tab">Datos Generales</a></li>
                <!--<li class="active"><a href="#otrosDatos" data-toggle="tab">Otros Datos Relacionados</a></li>-->
            </ul>

            <div class="tab-content">
                <div class="tab-pane active" id="datosGenerales">
                    <div class="form">

                        <?php $form=$this->beginWidget('CActiveForm', array(
                                'id'=>'votante-form',
                                'htmlOptions' => array('data-form-type'=>$formType,), // for inset effect
                                // Please note: When you enable ajax validation, make sure the corresponding
                                // controller action is handling ajax validation correctly.
                                // There is a call to performAjaxValidation() commented in generated controller code.
                                // See class documentation of CActiveForm for details on this.
                                'enableAjaxValidation'=>false,
                        )); ?>

                        <div id="div-result">
                        <?php
           if($model->hasErrors()):
               $this->renderPartial('//errorSumMsg', array('model' => $model));
           elseif(Yii::app()->user->hasFlash('success')):               $this->renderPartial('//msgBox', array('class' => 'successDialogBox', 'message' => Yii::app()->user->getFlash('success')));
           else:
                ?>
                        <div class="infoDialogBox"><p class="note">Todos los campos con <span class="required">*</span> son requeridos.</p></div>
                <?php
                   endif;
               ?>
                        </div>

                        <div id="div-datos-generales">

                            <div class="widget-box">

                                <div class="widget-header">
                                    <h5>Datos Generales</h5>

                                    <div class="widget-toolbar">
                                        <a data-action="collapse" href="#">
                                            <i class="icon-chevron-up"></i>
                                        </a>
                                    </div>
                                </div>

                                <div class="widget-body">
                                    <div class="widget-body-inner">
                                        <div class="widget-main">
                                            <div class="widget-main form">
                                                <div class="row">
                                                    <div class="col-md-12">

                                                        <div class="col-md-4">
                                                            <?php echo $form->labelEx($model,'cedula'); ?>
                                                            <?php echo $form->textField($model,'cedula', array('class' => 'span-12',"required"=>"required",)); ?>
                                                        </div>


                                                        <div class="col-md-4">
                                                            <?php echo $form->labelEx($model,'direccion_residencia'); ?>
                                                            <?php echo $form->textField($model,'direccion_residencia',array('size'=>60, 'maxlength'=>100, 'class' => 'span-12', )); ?>
                                                        </div>


                                                        <div class="col-md-4">
                                                            <?php echo $form->labelEx($model,'doble_nacionalidad'); ?>
                                                            <?php echo $form->textField($model,'doble_nacionalidad',array('size'=>1, 'maxlength'=>1, 'class' => 'span-12', "required"=>"required",)); ?>
                                                        </div>

                                                  </div>

                                                    <div class="space-6"></div>

                                                    <div class="col-md-12">

                                                        <div class="col-md-4">
                                                            <?php echo $form->labelEx($model,'email'); ?>
                                                            <?php echo $form->emailField($model,'email',array('size'=>60, 'maxlength'=>60, 'class' => 'span-12', )); ?>
                                                        </div>


                                                        <div class="col-md-4">
                                                            <?php echo $form->labelEx($model,'estado_civil'); ?>
                                                            <?php echo $form->textField($model,'estado_civil',array('size'=>1, 'maxlength'=>1, 'class' => 'span-12', "required"=>"required",)); ?>
                                                        </div>


                                                        <div class="col-md-4">
                                                            <?php echo $form->labelEx($model,'fecha_nacimiento'); ?>
                                                            <?php echo $form->textField($model,'fecha_nacimiento', array('class' => 'span-12',"required"=>"required",)); ?>
                                                        </div>

                                                  </div>

                                                    <div class="space-6"></div>

                                                    <div class="col-md-12">

                                                        <div class="col-md-4">
                                                            <?php echo $form->labelEx($model,'nacionalidad'); ?>
                                                            <?php echo $form->textField($model,'nacionalidad',array('size'=>1, 'maxlength'=>1, 'class' => 'span-12', "required"=>"required",)); ?>
                                                        </div>


                                                        <div class="col-md-4">
                                                            <?php echo $form->labelEx($model,'primer_apellido'); ?>
                                                            <?php echo $form->textField($model,'primer_apellido',array('size'=>25, 'maxlength'=>25, 'class' => 'span-12', "required"=>"required",)); ?>
                                                        </div>


                                                        <div class="col-md-4">
                                                            <?php echo $form->labelEx($model,'primer_nombre'); ?>
                                                            <?php echo $form->textField($model,'primer_nombre',array('size'=>25, 'maxlength'=>25, 'class' => 'span-12', "required"=>"required",)); ?>
                                                        </div>

                                                  </div>

                                                    <div class="space-6"></div>

                                                    <div class="col-md-12">

                                                        <div class="col-md-4">
                                                            <?php echo $form->labelEx($model,'segundo_apellido'); ?>
                                                            <?php echo $form->textField($model,'segundo_apellido',array('size'=>25, 'maxlength'=>25, 'class' => 'span-12', )); ?>
                                                        </div>


                                                        <div class="col-md-4">
                                                            <?php echo $form->labelEx($model,'segundo_nombre'); ?>
                                                            <?php echo $form->textField($model,'segundo_nombre',array('size'=>25, 'maxlength'=>25, 'class' => 'span-12', )); ?>
                                                        </div>


                                                        <div class="col-md-4">
                                                            <?php echo $form->labelEx($model,'sexo'); ?>
                                                            <?php echo $form->dropDownList($model, 'sexo', array('F'=>'Femenino', 'M'=>'Masculino',), array('prompt'=>'- - -', 'class' => 'span-12', "required"=>"required",)); ?>
                                                        </div>

                                                  </div>

                                                    <div class="space-6"></div>

                                                    <div class="col-md-12">

                                                        <div class="col-md-4">
                                                            <?php echo $form->labelEx($model,'telefono_celular'); ?>
                                                            <?php echo $form->textField($model,'telefono_celular',array('size'=>15, 'maxlength'=>15, 'class' => 'span-12', )); ?>
                                                        </div>


                                                        <div class="col-md-4">
                                                            <?php echo $form->labelEx($model,'telefono_residencia'); ?>
                                                            <?php echo $form->textField($model,'telefono_residencia',array('size'=>15, 'maxlength'=>15, 'class' => 'span-12', )); ?>
                                                        </div>


                                                        <div class="col-md-4">
                                                            <?php echo $form->labelEx($model,'zona_postal_residencia'); ?>
                                                            <?php echo $form->textField($model,'zona_postal_residencia',array('size'=>6, 'maxlength'=>6, 'class' => 'span-12', )); ?>
                                                        </div>

                                                  </div>

                                                    <div class="space-6"></div>

                                                    <div class="col-md-12">

                                                        <div class="col-md-4">
                                                            <?php echo $form->labelEx($model,'registro'); ?>
                                                            <?php echo $form->textField($model,'registro',array('size'=>1, 'maxlength'=>1, 'class' => 'span-12', )); ?>
                                                        </div>


                                                        <div class="col-md-4">
                                                            <?php echo $form->labelEx($model,'estatus'); ?>
                                                            <?php echo $form->dropDownList($model, 'estatus', array('A'=>'Activo', 'I'=>'Inactivo', 'E'=>'Eliminado'), array('prompt'=>'- - -', 'class' => 'span-12', )); ?>
                                                        </div>


                                                        <div class="col-md-4">
                                                            <?php echo $form->labelEx($model,'estado_id'); ?>
                                                            <?php echo $form->dropDownList($model, 'estado_id', CHtml::listData(Estado::model()->findAll(array('limit'=>50)), 'id', 'nombre'), array('prompt'=>'- - -', 'class' => 'span-12', )); ?>
                                                        </div>

                                                  </div>

                                                    <div class="space-6"></div>

                                                    <div class="col-md-12">

                                                        <div class="col-md-4">
                                                            <?php echo $form->labelEx($model,'municipio_id'); ?>
                                                            <?php echo $form->dropDownList($model, 'municipio_id', CHtml::listData(Municipio::model()->findAll(array('limit'=>50)), 'id', 'nombre'), array('prompt'=>'- - -', 'class' => 'span-12', )); ?>
                                                        </div>


                                                        <div class="col-md-4">
                                                            <?php echo $form->labelEx($model,'parroquia_id'); ?>
                                                            <?php echo $form->dropDownList($model, 'parroquia_id', CHtml::listData(Parroquia::model()->findAll(array('limit'=>50)), 'id', 'nombre'), array('prompt'=>'- - -', 'class' => 'span-12', )); ?>
                                                        </div>


                                                        <div class="col-md-4">
                                                            <?php echo $form->labelEx($model,'circuito_id'); ?>
                                                            <?php echo $form->textField($model,'circuito_id', array('class' => 'span-12',)); ?>
                                                        </div>

                                                  </div>

                                                    <div class="space-6"></div>

                                                    <div class="col-md-12">

                                                        <div class="col-md-4">
                                                            <?php echo $form->labelEx($model,'ente_id'); ?>
                                                            <?php echo $form->textField($model,'ente_id', array('class' => 'span-12',)); ?>
                                                        </div>


                                                        <div class="col-md-4">
                                                            <?php echo $form->labelEx($model,'centro_votacion'); ?>
                                                            <?php echo $form->textArea($model,'centro_votacion',array('rows'=>6, 'cols'=>12, 'class' => 'span-12', )); ?>
                                                        </div>


                                                        <div class="col-md-4">
                                                            <?php echo $form->labelEx($model,'cod_circuito'); ?>
                                                            <?php echo $form->textField($model,'cod_circuito',array('size'=>4, 'maxlength'=>4, 'class' => 'span-12', )); ?>
                                                        </div>

                                                  </div>

                                                    <div class="space-6"></div>

                                                    <div class="col-md-12">
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <hr>

                                <div class="row">

                                    <div class="col-md-6">
                                        <a class="btn btn-danger" href="<?php echo $this->createUrl("/personas/votante"); ?>" id="btnRegresar">
                                            <i class="icon-arrow-left"></i>
                                            Volver
                                        </a>
                                    </div>

                                    <div class="col-md-6 wizard-actions">
                                        <button class="btn btn-primary btn-next" data-last="Finish" type="submit">
                                            Guardar
                                            <i class="icon-save icon-on-right"></i>
                                        </button>
                                    </div>

                                </div>
                            </div>
                        </div>
                        <?php $this->endWidget(); ?>
                    </div><!-- form -->
                </div>

                <div class="tab-pane" id="otrosDatos">
                    <div class="alertDialogBox">
                        <p>
                            Próximamente: Esta área se encuentra en Desarrollo.
                        </p>
                    </div>
                </div>

            </div>
        </div>

        <div id="resultDialog" class="hide"></div>

        <?php
            /**
             * Yii::app()->clientScript->registerScriptFile(
             *   Yii::app()->request->baseUrl . '/public/js/modules/miModulo/VotanteController/votante/form.js',CClientScript::POS_END
             *);
             */
        ?>
    </div>
</div>
