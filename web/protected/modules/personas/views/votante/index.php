<?php

/* @var $this VotanteController */
/* @var $model Votante */

$this->breadcrumbs=array(
	'Votantes'=>array('lista'),
	'Administración',
);
$this->pageTitle = 'Administración de Votantes';

?>
<div class="widget-box">
    <div class="widget-header">
        <h5>Lista de Votantes</h5>

        <div class="widget-toolbar">
            <a href="#" data-action="collapse">
                <i class="icon-chevron-up"></i>
            </a>
        </div>
    </div>

    <div class="widget-body">
        <div style="display:block;" class="widget-body-inner">
            <div class="widget-main">

                <div class="row space-6"></div>
                <div>
                    <div id="resultadoOperacion">
                        <div class="infoDialogBox">
                            <p>
                                En este módulo podrá registrar y/o actualizar los datos de Votantes.
                            </p>
                        </div>
                    </div>

                    <div class="pull-right" style="padding-left:10px;">
                        <a href="<?php echo $this->createUrl("/personas/votante/registro"); ?>" type="submit" id='newRegister' data-last="Finish" class="btn btn-success btn-next btn-sm">
                            <i class="fa fa-plus icon-on-right"></i>
                            Registrar Nuevo Votantes                        </a>
                    </div>


                    <div class="row space-20"></div>

                </div>

<?php $this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'votante-grid',
	'dataProvider'=>$dataProvider,
        'filter'=>$model,
        'itemsCssClass' => 'table table-striped table-bordered table-hover',
        'summaryText' => 'Mostrando {start}-{end} de {count}',
        'pager' => array(
            'header' => '',
            'htmlOptions' => array('class' => 'pagination'),
            'firstPageLabel' => '<span title="Primera página">&#9668;&#9668;</span>',
            'prevPageLabel' => '<span title="Página Anterior">&#9668;</span>',
            'nextPageLabel' => '<span title="Página Siguiente">&#9658;</span>',
            'lastPageLabel' => '<span title="Última página">&#9658;&#9658;</span>',
        ),
        'afterAjaxUpdate' => "
                function(){

                }",
	'columns'=>array(
        array(
            'header' => '<center>id</center>',
            'name' => 'id',
            'htmlOptions' => array(),
            //'filter' => CHtml::textField('Votante[id]', $model->id, array('title' => '',)),
        ),
        array(
            'header' => '<center>cedula</center>',
            'name' => 'cedula',
            'htmlOptions' => array(),
            //'filter' => CHtml::textField('Votante[cedula]', $model->cedula, array('title' => '',)),
        ),
        array(
            'header' => '<center>direccion_residencia</center>',
            'name' => 'direccion_residencia',
            'htmlOptions' => array(),
            //'filter' => CHtml::textField('Votante[direccion_residencia]', $model->direccion_residencia, array('title' => '',)),
        ),
        array(
            'header' => '<center>doble_nacionalidad</center>',
            'name' => 'doble_nacionalidad',
            'htmlOptions' => array(),
            //'filter' => CHtml::textField('Votante[doble_nacionalidad]', $model->doble_nacionalidad, array('title' => '',)),
        ),
        array(
            'header' => '<center>email</center>',
            'name' => 'email',
            'htmlOptions' => array(),
            //'filter' => CHtml::textField('Votante[email]', $model->email, array('title' => '',)),
        ),
        array(
            'header' => '<center>estado_civil</center>',
            'name' => 'estado_civil',
            'htmlOptions' => array(),
            //'filter' => CHtml::textField('Votante[estado_civil]', $model->estado_civil, array('title' => '',)),
        ),
		/*
        array(
            'header' => '<center>fecha_nacimiento</center>',
            'name' => 'fecha_nacimiento',
            'htmlOptions' => array(),
            //'filter' => CHtml::textField('Votante[fecha_nacimiento]', $model->fecha_nacimiento, array('title' => '',)),
        ),
        array(
            'header' => '<center>nacionalidad</center>',
            'name' => 'nacionalidad',
            'htmlOptions' => array(),
            //'filter' => CHtml::textField('Votante[nacionalidad]', $model->nacionalidad, array('title' => '',)),
        ),
        array(
            'header' => '<center>primer_apellido</center>',
            'name' => 'primer_apellido',
            'htmlOptions' => array(),
            //'filter' => CHtml::textField('Votante[primer_apellido]', $model->primer_apellido, array('title' => '',)),
        ),
        array(
            'header' => '<center>primer_nombre</center>',
            'name' => 'primer_nombre',
            'htmlOptions' => array(),
            //'filter' => CHtml::textField('Votante[primer_nombre]', $model->primer_nombre, array('title' => '',)),
        ),
        array(
            'header' => '<center>segundo_apellido</center>',
            'name' => 'segundo_apellido',
            'htmlOptions' => array(),
            //'filter' => CHtml::textField('Votante[segundo_apellido]', $model->segundo_apellido, array('title' => '',)),
        ),
        array(
            'header' => '<center>segundo_nombre</center>',
            'name' => 'segundo_nombre',
            'htmlOptions' => array(),
            //'filter' => CHtml::textField('Votante[segundo_nombre]', $model->segundo_nombre, array('title' => '',)),
        ),
        array(
            'header' => '<center>sexo</center>',
            'name' => 'sexo',
            'htmlOptions' => array(),
            //'filter' => CHtml::textField('Votante[sexo]', $model->sexo, array('title' => '',)),
        ),
        array(
            'header' => '<center>telefono_celular</center>',
            'name' => 'telefono_celular',
            'htmlOptions' => array(),
            //'filter' => CHtml::textField('Votante[telefono_celular]', $model->telefono_celular, array('title' => '',)),
        ),
        array(
            'header' => '<center>telefono_residencia</center>',
            'name' => 'telefono_residencia',
            'htmlOptions' => array(),
            //'filter' => CHtml::textField('Votante[telefono_residencia]', $model->telefono_residencia, array('title' => '',)),
        ),
        array(
            'header' => '<center>zona_postal_residencia</center>',
            'name' => 'zona_postal_residencia',
            'htmlOptions' => array(),
            //'filter' => CHtml::textField('Votante[zona_postal_residencia]', $model->zona_postal_residencia, array('title' => '',)),
        ),
        array(
            'header' => '<center>usuario_id</center>',
            'name' => 'usuario_id',
            'htmlOptions' => array(),
            //'filter' => CHtml::textField('Votante[usuario_id]', $model->usuario_id, array('title' => '',)),
        ),
        array(
            'header' => '<center>registro</center>',
            'name' => 'registro',
            'htmlOptions' => array(),
            //'filter' => CHtml::textField('Votante[registro]', $model->registro, array('title' => '',)),
        ),
        array(
            'header' => '<center>estatus</center>',
            'name' => 'estatus',
            'htmlOptions' => array(),
            //'filter' => CHtml::textField('Votante[estatus]', $model->estatus, array('title' => '',)),
        ),
        array(
            'header' => '<center>usuario_ini_id</center>',
            'name' => 'usuario_ini_id',
            'htmlOptions' => array(),
            //'filter' => CHtml::textField('Votante[usuario_ini_id]', $model->usuario_ini_id, array('title' => '',)),
        ),
        array(
            'header' => '<center>fecha_ini</center>',
            'name' => 'fecha_ini',
            'htmlOptions' => array(),
            //'filter' => CHtml::textField('Votante[fecha_ini]', $model->fecha_ini, array('title' => '',)),
        ),
        array(
            'header' => '<center>fecha_act</center>',
            'name' => 'fecha_act',
            'htmlOptions' => array(),
            //'filter' => CHtml::textField('Votante[fecha_act]', $model->fecha_act, array('title' => '',)),
        ),
        array(
            'header' => '<center>usuario_act_id</center>',
            'name' => 'usuario_act_id',
            'htmlOptions' => array(),
            //'filter' => CHtml::textField('Votante[usuario_act_id]', $model->usuario_act_id, array('title' => '',)),
        ),
        array(
            'header' => '<center>estado_id</center>',
            'name' => 'estado_id',
            'htmlOptions' => array(),
            //'filter' => CHtml::textField('Votante[estado_id]', $model->estado_id, array('title' => '',)),
        ),
        array(
            'header' => '<center>municipio_id</center>',
            'name' => 'municipio_id',
            'htmlOptions' => array(),
            //'filter' => CHtml::textField('Votante[municipio_id]', $model->municipio_id, array('title' => '',)),
        ),
        array(
            'header' => '<center>parroquia_id</center>',
            'name' => 'parroquia_id',
            'htmlOptions' => array(),
            //'filter' => CHtml::textField('Votante[parroquia_id]', $model->parroquia_id, array('title' => '',)),
        ),
        array(
            'header' => '<center>circuito_id</center>',
            'name' => 'circuito_id',
            'htmlOptions' => array(),
            //'filter' => CHtml::textField('Votante[circuito_id]', $model->circuito_id, array('title' => '',)),
        ),
        array(
            'header' => '<center>ente_id</center>',
            'name' => 'ente_id',
            'htmlOptions' => array(),
            //'filter' => CHtml::textField('Votante[ente_id]', $model->ente_id, array('title' => '',)),
        ),
        array(
            'header' => '<center>centro_votacion</center>',
            'name' => 'centro_votacion',
            'htmlOptions' => array(),
            //'filter' => CHtml::textField('Votante[centro_votacion]', $model->centro_votacion, array('title' => '',)),
        ),
        array(
            'header' => '<center>cod_circuito</center>',
            'name' => 'cod_circuito',
            'htmlOptions' => array(),
            //'filter' => CHtml::textField('Votante[cod_circuito]', $model->cod_circuito, array('title' => '',)),
        ),
		*/
		array(
                    'type' => 'raw',
                    'header' => '<center>Acción</center>',
                    'value' => array($this, 'getActionButtons'),
                    'htmlOptions' => array('nowrap'=>'nowrap'),
                ),
	),
)); ?>
            </div>
        </div>
    </div>
</div>