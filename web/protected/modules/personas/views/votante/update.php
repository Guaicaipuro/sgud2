<?php
/* @var $this VotanteController */
/* @var $model Votante */

$this->pageTitle = 'Actualización de Datos de Votantes';
      $this->breadcrumbs=array(
        'Personas' => array('/personas'),
	'Votantes'=>array('lista'),
	'Actualización',
);
?>

<?php $this->renderPartial('_form', array('model'=>$model, 'formType'=>'edicion')); ?>