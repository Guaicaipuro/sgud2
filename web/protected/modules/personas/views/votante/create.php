<?php
/* @var $this VotanteController */
/* @var $model Votante */

$this->pageTitle = 'Registro de Votantes';
      $this->breadcrumbs=array(
        'Personas' => array('/personas'),
	'Votantes'=>array('lista'),
	'Registro',
);
?>

<?php $this->renderPartial('_form', array('model'=>$model, 'formType'=>'registro')); ?>