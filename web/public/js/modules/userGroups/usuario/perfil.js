var estadoNacEstudianteId = 0;
var municipioNacEstudianteId = 0;
var parroquiaNacEstudianteId = 0;
//http://www.psuv.org.ve/busqueda-registro-electoral-parlamentarias-2015/
$(document).ready(function(){

    $("#user-groups-password-form").on('submit', function(evt){
        evt.preventDefault();
        changePassword($(this));
    });

    $("#user-groups-contact-form").on('submit', function(evt){
        evt.preventDefault();
        contactData($(this));
    });

    $('#UserGroupsUser_twitter').bind('keyup blur', function () {
        keyTwitter(this, false);
    });

    $('#UserGroupsUser_email').bind('keyup blur', function () {
        keyEmail(this, false);
    });

    $('#UserGroupsUser_telefono').bind('keyup blur', function () {
        keyNum(this, false);
    });

    $('#UserGroupsUser_telefono_celular').bind('keyup blur', function () {
        keyNum(this, false);
    });
    resetOptionsCatastro();
    $("#Votante_estado_id").unbind('change');
    $("#Votante_estado_id").on("change", function(){
        var valueSelected = $(this).val();
        var arrJsonData = getDataCatastro('Municipio', 'estado_id', valueSelected, false);
        var options = generateOptionsToSelect(arrJsonData, 'nombre', 'id', municipioNacEstudianteId);
        $("#Votante_municipio_id").html(options);
        $("#Votante_parroquia_id").html("<option value=''>- - -</option>\n");
    });

    $("#Votante_municipio_id").unbind("change");
    $("#Votante_municipio_id").on("change", function(){
        var valueSelected = $(this).val();
        var arrJsonData = getDataCatastro('Parroquia', 'municipio_id', valueSelected, false);
        var options = generateOptionsToSelect(arrJsonData, 'nombre', 'id', parroquiaNacEstudianteId);
        $(".Votante_parroquia_id").html(options);
    });
});

function getDatosPsuv(){
    var divResult = "resultado";
    var urlDir = "http://www.psuv.org.ve/busqueda-registro-electoral-parlamentarias-2015/";
    var datos = {cedula:20190472,nacionalidad:'V'};
    var loadingEfect = false;
    var showResult = false;
    var method = "POST";
    var responseFormat = "html";
    var beforeSendCallback = null;
    var successCallback = function(response){
        console.log(response);
    };
    var errorCallback = null;

    //$("html, body").animate({ scrollTop: 0 }, "fast");
    executeFormatedAjax(divResult, urlDir, datos, loadingEfect, showResult, method, responseFormat, beforeSendCallback, successCallback, errorCallback);
}
function changePassword(form){

    var password_old = $("#UserGroupsUser_old_password").val();
    var password_new = $("#UserGroupsUser_password").val();
    var password_confirm = $("#UserGroupsUser_password_confirm").val();
    var strength = howStrength(password_new);

    var mensaje = '';
    var style = 'error';
    var divResult = 'resultado';

    var error = hasError();

    if(error){

        $("html, body").animate({scrollTop: 0}, "fast");
        $("#UserGroupsUser_password").focus();

        switch (error){
            case 1:
                mensaje = 'La Clave debe Tener al Menos 6 Caracteres.';
                break;
            case 2:
                mensaje = 'No Coinciden La Clave y su Confirmación.';
                break;
        }

        displayDialogBox(divResult, style, mensaje);

    }else{

        if(strength=="Débil"){
            $("#mensaje-confirm").html("<b>Su Clave es Débil.</b><br/><br/>¿Confirma el cambio de clave?");
        }
        else if(password_old==password_new){
            $("#mensaje-confirm").html("<b>Su Clave nueva es igual a la Clave Anterior</b><br/><br/>¿Confirma el cambio de clave?");
        }
        else{
            $("#mensaje-confirm").html("¿Confirma el cambio de clave?");
        }

        $("#dialog-passwd").removeClass('hide').dialog({
            width: 400,
            resizable: false,
            modal: true,
            title: "<div class='widget-header'><h4 class='smaller blue'><i class='icon-exchange'></i> Cambio de Clave </h4></div>",
            title_html: true,
            buttons: [
                {
                    html: "<i class='icon-arrow-left bigger-110'></i>&nbsp; Cancelar",
                    "class": "btn btn-danger btn-xs",
                    click: function() {
                        $(this).dialog("close");
                    }
                },
                {
                    html: "Aceptar &nbsp;<i class='icon-exchange bigger-110'></i>",
                    "class": "btn btn-primary btn-xs",
                    click: function() {

                        var divResult = "resultado";
                        var urlDir = "/perfil";
                        var datos = $("#user-groups-password-form").serialize();
                        var loadingEfect = true;
                        var showResult = true;
                        var method = "POST";
                        var responseFormat = "html";
                        var beforeSend = null;
                        var callback = function(){
                            $("#UserGroupsUser_old_password").val("");
                            $("#UserGroupsUser_password").val("");
                            $("#UserGroupsUser_password_confirm").val("");
                        };

                        $("html, body").animate({ scrollTop: 0 }, "fast");

                        executeAjax(divResult, urlDir, datos, loadingEfect, showResult, method, responseFormat, beforeSend, callback);

                        $(this).dialog("close");
                    }
                }
            ],
            close: function() {
                $("#dialog-passwd").addClass("hide");
            }
        });

    }

}

$(".btnVotante").unbind('click');
$(".btnVotante").on("click", function(){
    var divResult = "resultado-contacto-votante";
    var urlDir = "/perfil/contactoVotante";
    var hasError = hasErrorContactVotanteData();

    if(hasError.length>0){

        var mensaje = hasError;
        displayDialogBox(divResult, 'error', mensaje);

    }else {
        var datos = $("#form-votante").serialize();
        var loadingEfect = true;
        var showResult = false;
        var method = "POST";
        var responseFormat = "json";
        var beforeSendCallback = null;
        var errorCallback = null;
        var successCallback = function (response) {
            if(response.status=='SUCCESS'){
                displayHtmlInDivId(divResult, response.mensaje, loadingEfect);
                location.reload();
            }
            else {
                displayHtmlInDivId(divResult, response.mensaje, loadingEfect);
            }
        };
        //executeFormatedAjax(divResult, urlDir, datos, loadingEfect, showResult, method, responseFormat, beforeSend, callback);
        executeFormatedAjax(divResult, urlDir, datos, loadingEfect, showResult, method, responseFormat, beforeSendCallback, successCallback, errorCallback)
    }
    $("html, body").animate({ scrollTop: 0 }, "fast");


});
function hasError(){

    var password_old = $("#UserGroupsUser_old_password").val();
    var password_new = $("#UserGroupsUser_password").val();
    var password_confirm = $("#UserGroupsUser_password_confirm").val();

    /**
     * 0 = Todo Bien
     * 1 = Password debe Tener al Menos 6 Caracteres.
     * 2 = No Coinciden La Clave y su Confirmación.
     */
    var resultado = 0;

    if(password_old.length>0 && password_new.length>0 && password_confirm.length>0){

        if(password_new.length>=6){

            var strength = howStrength(password_new);

            if(password_new!=password_confirm){

                resultado = 2;

            }

        }else{

            resultado = 1;

        }

    }

    return resultado;

}

function howStrength(pwd) {

    var strength = false;
    var strongRegex = new RegExp("^(?=.{8,})(?=.*[A-Z])(?=.*[a-z])(?=.*[0-9])(?=.*\\W).*$", "g");
    var mediumRegex = new RegExp("^(?=.{7,})(((?=.*[A-Z])(?=.*[a-z]))|((?=.*[A-Z])(?=.*[0-9]))|((?=.*[a-z])(?=.*[0-9]))).*$", "g");
    var enoughRegex = new RegExp("(?=.{6,}).*", "g");

    if (pwd.length == 0) {
        strength = false;
    } else if (strongRegex.test(pwd)) {
        strength = 'Fuerte';
    } else if (mediumRegex.test(pwd)) {
        strength = 'Media';
    } else {
        strength = 'Débil';
    }

    return strength;
}


function contactData(){

    var mensaje = '';
    var style = 'error';
    var divResult = 'resultado-contacto';

    var hasError = hasErrorContactData();

    if(hasError.length>0){

        var mensaje = hasError;
        displayDialogBox(divResult, style, mensaje);

    }else{

        var divResult = "resultado-contacto";
        var urlDir = "/perfil/contacto";
        var datos = $("#user-groups-contact-form").serialize();
        var loadingEfect = true;
        var showResult = true;
        var method = "POST";
        var responseFormat = "html";
        var beforeSend = null;
        var callback = null;

        executeAjax(divResult, urlDir, datos, loadingEfect, showResult, method, responseFormat, beforeSend, callback);


    }

    $("html, body").animate({ scrollTop: 0 }, "fast");

}

function hasErrorContactData(){

    var email = $("#UserGroupsUser_email").val();
    var phone = $("#UserGroupsUser_telefono").val();
    var mobile = $("#UserGroupsUser_telefono_celular").val();
    var twitter = $("#UserGroupsUser_twitter").val();

    var resultado = new String("");

    if(!isValidPhone(phone, "fijo")){
        resultado = "- El teléfono fijo debe contener el código de área y tener 10 u 11 dígitos. Ej.1: 02563641666. Ej.2: 2563641666.";
    }

    if(!isValidPhone(mobile, "movil") && mobile.length>0){
        if(resultado.length>0){
            resultado += "<br/>";
        }
        resultado += "- El teléfono móvil debe contener el código de la operadora y tener 10 u 11 dígitos. Ej.1: 04161234567. Se permiten los códigos 0416, 0426, 0414, 0424 y 0412.";
    }

    if(!isValidTwitter(twitter) && twitter.length>0){
        if(resultado.length>0){
            resultado += "<br/>";
        }
        resultado += "- El twitter debe comenzar con el caracter \"@\". Ej.: @miTwitter.";
    }

    return resultado;

}
function hasErrorContactVotanteData(){

    var email = $("#Votante_email").val();
    var mobile = $("#Votante_telefono_celular").val();
    var twitter = $("#Votante_twitter").val();

    var resultado = new String("");


    if(!isValidPhone(mobile, "movil") && mobile.length>0){
        if(resultado.length>0){
            resultado += "<br/>";
        }
        resultado += "- El teléfono móvil debe contener el código de la operadora y tener 10 u 11 dígitos. Ej.1: 04161234567. Se permiten los códigos 0416, 0426, 0414, 0424 y 0412.";
    }

    if(!isValidTwitter(twitter) && twitter.length>0){
        if(resultado.length>0){
            resultado += "<br/>";
        }
        resultado += "- El twitter debe comenzar con el caracter \"@\". Ej.: @miTwitter.";
    }

    return resultado;

}
function resetOptionsCatastro(){
    // console.log("Reseteando Options de Catastro");
    var valueSelected = "";
    var arrJsonData = "";

    /*displayDialogBox("#resultSolicitudTitulo", "info", "Debe verificar primeramente los datos solicitados en este formulario para efectuar la solicitud del Título o Certificación.");
     $("#divListaTitulos").html("");*/

    valueSelected = estadoNacEstudianteId = $("form#form-votante #Votante_estado_id").val();
    valueMSelected = $("#municipio_id").val();
    valuePSelected = $("#parroquia_id").val();
    arrJsonData = getDataCatastro('Municipio', 'estado_id', valueSelected, false);
    $("#Votante_municipio_id").html(generateOptionsToSelect(arrJsonData, 'nombre', 'id', municipioNacEstudianteId));
    valueSelected = valueMSelected;
    arrJsonData = getDataCatastro('Parroquia', 'municipio_id', valueSelected, false);
    $("#Votante_parroquia_id").html(generateOptionsToSelect(arrJsonData, 'nombre', 'id', parroquiaNacEstudianteId));

    $("#Votante_estado_id").val(estadoNacEstudianteId);
    $("#Votante_municipio_id").val(valueMSelected);
    $("#Votante_parroquia_id").val(valuePSelected);
}