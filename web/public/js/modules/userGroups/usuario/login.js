$(document).ready(function(){
   $("#linkRefreshCaptcha").on('click', function (){
       reloadCaptcha();
   });
   
   $("#UserGroupsUser_username").val("");
   $("#UserGroupsUser_password").val("");
   $("#UserGroupsUser_verifyCode").val("");

    $('#UserGroupsUser_nombre, #UserGroupsUser_apellido').bind('keyup blur', function() {
        keyAlpha(this, true);
        makeUpper(this);
    });

    $('#UserGroupsUser_cedula').bind('keyup blur', function() {
        keyNum(this, true, false);
        clearField(this);
    });

    $('#UserGroupsUser_email').bind('keyup blur', function() {
        keyEmail(this, true);//Acepta solo caracteres validos para un correo(email)
        makeUpper(this);//Transforma los caracteres a mayusculas
        clearField(this);
    });

    $.mask.definitions['L'] = '[1-2]';
    $.mask.definitions['X'] = '[2|4|6]';
    $.mask.definitions['~'] = '[+-]';
    $('#UserGroupsUser_telefono_celular').mask('(04LX) 999-9999');

    $('#regist-nuevo-box').unbind('click');
    $('#regist-nuevo-box').click(function(){
        $('.close').parent().fadeOut('slow');
    });

    /* VALIDANDO EL DOCUMENTO IDENTIDAD EN SAIME*/
    $('#UserGroupsUser_origen, #UserGroupsUser_cedula').unbind('blur');
    $('#UserGroupsUser_origen, #UserGroupsUser_cedula').bind(' blur', function() {

        var origen = $('#UserGroupsUser_origen').val();
        var cedula = $('#UserGroupsUser_cedula').val();
        var mensaje;
        var style;
        var divResult = 'result-new';
        var urlDir = '/userGroups/user/obtenerDatosPersona';
        var datos;
        var loadingEfect = false;
        var showResult = false;
        var method = 'POST';
        var responseFormat = 'json';
        var beforeSendCallback = function() {
        };
        var successCallback;
        var errorCallback = function() {
        };
        if (cedula != null && cedula != '') {
            if (origen != null && origen != '') {
                datos = {
                    cedula: cedula,
                    origen: origen
                }
                successCallback = function(response) {
                    if (response.statusCode == 'SUCCESS') {
                        $('#UserGroupsUser_nombre').val(response.nombres);
                        $('#UserGroupsUser_apellido').val(response.apellidos);
                        Loading.hide();
                    }
                    if (response.statusCode == 'ERROR') {
                        mensaje = response.title+ '<br>' +response.mensaje;
                        style = 'error';
                        displayDialogBox(divResult, style, mensaje);
                        $('#UserGroupsUser_origen').val('');
                        $('#UserGroupsUser_cedula').val('');
                        $('#UserGroupsUser_nombre').val('');
                        $('#UserGroupsUser_apellido').val('');
                        Loading.hide();
                    }
                };
                Loading.show();
                executeFormatedAjax(divResult, urlDir, datos, loadingEfect, showResult, method, responseFormat, beforeSendCallback, successCallback, errorCallback);
                // Loading.hide();
            }
            else {
                mensaje = 'Notificación de Error<br> Estimado usuario debe seleccionar un Origen Valido.';
                style = 'error';
                displayDialogBox(divResult, style, mensaje);
                $('#UserGroupsUser_origen').val('');
                $('#UserGroupsUser_cedula').val('');
                $('#UserGroupsUser_nombre').val('');
                $('#UserGroupsUser_apellido').val('');
            }

        }
    });
    /* FIN */

    $('.register-new').unbind('click');
    $('.register-new').click(function(evt){
        evt.preventDefault();

        var mensaje;
        var style;
        var divResult = 'result-new';
        var urlDir = '/userGroups/user/registerNewUser';
        var datos;
        var loadingEfect = false;
        var showResult = false;
        var method = 'POST';
        var responseFormat = 'json';
        var beforeSendCallback = function() {
        };
        var successCallback;
        var errorCallback = function() {
        };

                datos = $('#register-new-user-form').serialize();
                successCallback = function(response) {
                    if (response.statusCode == 'SUCCESS') {
                        mensaje = response.title+ '<br>' +response.mensaje;
                        style = 'success';
                        displayDialogBox(divResult, style, mensaje);
                        location.reload();
                        Loading.hide();
                    }
                    if (response.statusCode == 'ERROR') {
                        mensaje = response.title+ '<br>' +response.mensaje;
                        style = 'error';
                        displayDialogBox(divResult, style, mensaje);
                        Loading.hide();
                    }
                };
                Loading.show();
                executeFormatedAjax(divResult, urlDir, datos, loadingEfect, showResult, method, responseFormat, beforeSendCallback, successCallback, errorCallback);
                // Loading.hide();

    });

});

function reloadCaptcha(){
    jQuery('#siimage').attr('src', '/login/captcha/sid/' + Math.random());
    $("#UserGroupsUser_verifyCode").val("");
}



