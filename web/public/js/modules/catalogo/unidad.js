$(document).ready(function() {
    $('#unidad_nombre_form').bind('keyup blur', function() {
        keyLettersAndSpaces(this, true);// Valida que contenga números, letras y espacios
        makeUpper(this);//Convierte las letras en mayusculas
    });

    $('#nombre_unidad_grid').bind('keyup blur', function() {
        keyLettersAndSpaces(this, true);// Valida que contenga números, letras y espacios
        makeUpper(this);//Convierte las letras en mayusculas
    });
});


$("#btnRegistrarNuevaUnidad").unbind('click');
$("#btnRegistrarNuevaUnidad").click(function() {

    /// Loading.show();
    $.ajax({
        url: "mostrarFormDeUnidad",
        data: $("#unidad_form").serialize(),
        dataType: 'html',
        type: 'post',
        success: function(resp) {

            var dialogRegistrar = $("#dialog_registrarNuevaUnidad").removeClass('hide').dialog({
                modal: true,
                width: '850px',
                draggable: false,
                resizable: false,
                title: "<div class='widget-header widget-header-small'><h4 class='smaller'>Agregar Nueva Unidad</h4></div>",
                title_html: true,
                buttons: [
                    {
                        html: "<i class='icon-arrow-left bigger-110'></i>&nbsp; Volver",
                        "class": "btn btn-danger btn-xs",
                        click: function() {
                            $(this).dialog("close");
                        }
                    },
                    {
                        html: "Guardar &nbsp; <i class='icon-save icon-on-right bigger-110'></i>",
                        "class": "btn btn-primary btn-xs",
                        click: function() {
                            //    Loading.show();
                            $.ajax({
                                url: "guardarNuevaUnidad",
                                data: $("#unidad_form").serialize(),
                                dataType: 'html',
                                type: 'post',
                                success: function(resp, resp2, resp3) {

                                    try {
                                        var json = jQuery.parseJSON(resp3.responseText);
                                        if (json.statusCode == "success") {

                                            $("#msj_error").addClass('hide');
                                            $("#msj_error p").html('');
                                            $("#msj_exitoso").removeClass('hide');
                                            $("#msj_exitoso p").html(json.mensaje);
                                            //$("#unidad_nombre_form").val('');
                                            refrescarGrid();
                                            dialogRegistrar.dialog('close');
                                            $("html, body").animate({scrollTop: 0}, "fast");
                                        }
                                        if (json.statusCode == "error") {
                                            //   alert('entre');
                                            $("#msj_exitoso").addClass('hide');
                                            $("#msj_exitoso p").html('');
                                            $("#msj_error").removeClass('hide');
                                            $("#msj_error p").html(json.mensaje);
                                            $("html, body").animate({scrollTop: 0}, "fast");
                                        }
                                    }
                                    catch (e) {

                                    }
                                    //      Loading.hide();
                                }
                            })

                        }
                    }
                ]
            });
            $("#dialog_registrarNuevaUnidad").html(resp);
            //   Loading.hide();
        }
    });
});






$(".add-modificar").unbind('click');
$(".add-modificar").on('click', function(e) {
    e.preventDefault();

    var registro_id;
    registro_id = $(this).attr("data");

    modificar(registro_id);
});

function modificar(registro_id) {
    var data = {
        id: registro_id
    };

    $.ajax({
        url: "mostrarDatosUnidad",
        data: data,
        dataType: 'html',
        type: 'get',
        success: function(resp, resp2, resp3) {

            //  $("#dialog_registrarSeccion span").html($("#SeccionPlantel_seccion_id option:selected").text());
            var dialogRegistrar = $("#dialog_registrarNuevaUnidad").removeClass('hide').dialog({
                modal: true,
                width: '850px',
                draggable: false,
                resizable: false,
                title: "<div class='widget-header widget-header-small'><h4 class='smaller'>Modificar Unidad</h4></div>",
                title_html: true,
                buttons: [
                    {
                        html: "<i class='icon-arrow-left bigger-110'></i>&nbsp; Volver",
                        "class": "btn btn-danger btn-xs",
                        click: function() {
                            $(this).dialog("close");
                        }
                    },
                    {
                        html: "Guardar &nbsp; <i class='icon-save icon-on-right bigger-110'></i>",
                        "class": "btn btn-primary btn-xs",
                        click: function() {


                            var data = {
                                id: registro_id,
                                nombre: $("#unidad_nombre_form").val(),
                            }

                            $.ajax({
                                url: "modificarUnidad",
                                data: data,
                                dataType: 'html',
                                type: 'post',
                                success: function(resp, resp2, resp3) {

                                    try {
                                        var json = jQuery.parseJSON(resp3.responseText);

                                        if (json.statusCode == "success") {

                                            refrescarGrid();
                                            dialogRegistrar.dialog('close');
                                            $("#unidad_nombre_form").val('');
                                            $("#msj_error").addClass('hide');
                                            $("#msj_error p").html('');
                                            $("#msj_exitoso").removeClass('hide');
                                            $("#msj_exitoso p").html(json.mensaje);
                                            $("html, body").animate({scrollTop: 0}, "fast");
                                        }

                                        if (json.statusCode == "error") {

                                            $("#unidad_nombre_form").val('');
                                            $("#msj_exitoso").addClass('hide');
                                            $("#msj_exitoso p").html('');
                                            $("#msj_error").removeClass('hide');
                                            $("#msj_error p").html(json.mensaje);
                                            $("html, body").animate({scrollTop: 0}, "fast");
                                        }
                                    }
                                    catch (e) {
                                        //  $("#dialog_registrarNuevaUnidad").html(resp);
                                    }
                                }
                            })
                        }
                    }
                ],
            });


            try {
                var json = jQuery.parseJSON(resp3.responseText);

                if (json.statusCode == "error") {

                    $("#unidad_nombre_form").val('');
                    $("#msj_exitoso").addClass('hide');
                    $("#msj_exitoso p").html('');
                    $("#msj_error").removeClass('hide');
                    $("#msj_error p").html(json.mensaje);
                    $("html, body").animate({scrollTop: 0}, "fast");
                }
            }
            catch (e) {
                $("#msj_exitoso").addClass('hide');
                $("#msj_exitoso p").html('');
                $("#msj_error").addClass('hide');
                $("#msj_error p").html('');
                $("#dialog_registrarNuevaUnidad").html(resp);
                $("html, body").animate({scrollTop: 0}, "fast");
            }

        }
    })
}





$(".add-consultar").unbind('click');
$(".add-consultar").on('click', function(e) {
    e.preventDefault();

    var registro_id;
    registro_id = $(this).attr("data");

    consultar(registro_id);
});


function consultar(registro_id) {
   // alert(registro_id);
    var data = {
        id: registro_id
    };

    $.ajax({
        url: "consultarUnidad",
        data: data,
        dataType: 'html',
        type: 'get',
        success: function(resp)
        {
 
            var dialog_consultar = $("#dialog_consultar").removeClass('hide').dialog({
                modal: true,
                width: '800px',
                draggable: false,
                resizable: false,
                //position: 'top',
                title: "<div class='widget-header widget-header-small'><h4 class='smaller'><i class='icon-list bigger-110'></i>&nbsp; Detalles del Registro de Unidad</h4></div>",
                title_html: true,
                buttons: [
                    {
                        html: "<i class='icon-arrow-left bigger-110'></i>&nbsp; Volver",
                        "class": "btn btn-danger btn-xs",
                        click: function() {
                            dialog_consultar.dialog("close");
                        }
                    }
                ]
            });
            try {
                var json = jQuery.parseJSON(resp3.responseText);

                if (json.statusCode == "error") {
                  //  alert('error');
                    $("#msj_error_consulta").removeClass('hide');
                    $("#msj_error_consulta p").html(json.mensaje);
                    $("html, body").animate({scrollTop: 0}, "fast");
                }
            }
            catch (e) {
               // alert('catch');
                $("#msj_error_consulta").addClass('hide');
                $("#msj_error_consulta p").html('');
                $("#dialog_consultar").html(resp);
                $("html, body").animate({scrollTop: 0}, "fast");
            }
        }
    });
}






$(".add-inactivar").unbind('click');
$(".add-inactivar").on('click', function(e) {
    e.preventDefault();

    var registro_id;
    registro_id = $(this).attr("data");

    inactivar(registro_id);
});

function inactivar(registro_id) {
    $("#pregunta_inactivar").removeClass('hide');
    $("#msj_error_inactivar").addClass('hide');
    $("#msj_error_inactivar p").html('');

    var dialogInactivar = $("#dialog_inactivacion").removeClass('hide').dialog({
        modal: true,
        width: '600px',
        draggable: false,
        resizable: false,
        title: "<div class='widget-header widget-header-small'><h4 class='smaller'><i class='fa fa-exclamation-triangle'></i> Inactivar Registro de Unidad</h4></div>",
        title_html: true,
        buttons: [
            {
                html: "<i class='icon-remove bigger-110'></i>&nbsp; Cancelar",
                "class": "btn btn-xs",
                click: function() {
                    $(this).dialog("close");
                }
            },
            {
                html: "<i class='icon-trash bigger-110'></i>&nbsp; Inactivar",
                "class": "btn btn-danger btn-xs",
                click: function() {
                    var data = {
                        id: registro_id
                    };
                    $.ajax({
                        url: "inactivarUnidad",
                        data: data,
                        dataType: 'html',
                        type: 'post',
                        success: function(resp, resp2, resp3) {

                            try {
                                var json = jQuery.parseJSON(resp3.responseText);

                                if (json.statusCode == "success") {

                                    refrescarGrid();
                                    dialogInactivar.dialog('close');
                                    $("#msj_error_inactivar").addClass('hide');
                                    $("#msj_error_inactivar p").html('');
                                    $("#msj_exitoso").removeClass('hide');
                                    $("#msj_exitoso p").html(json.mensaje);
                                    $("html, body").animate({scrollTop: 0}, "fast");
                                }

                                if (json.statusCode == "error") {

                                    $("#pregunta_inactivar").addClass('hide');
                                    $("#msj_exitoso").addClass('hide');
                                    $("#msj_exitoso p").html('');
                                    $("#msj_error_inactivar").removeClass('hide');
                                    $("#msj_error_inactivar p").html(json.mensaje);
                                    $("html, body").animate({scrollTop: 0}, "fast");
                                }
                            }
                            catch (e) {

                            }
                        }
                    })
                }
            }
        ]
    });
    $("#dialog_inactivacion").show();
}




$(".add-activar").unbind('click');
$(".add-activar").on('click', function(e) {

    e.preventDefault();

    var registro_id;
    registro_id = $(this).attr("data");

    activar(registro_id);

});


function activar(registro_id) {

    $("#pregunta_activar").removeClass('hide');
    $("#msj_error_activar").addClass('hide');
    $("#msj_error_activar p").html('');

    var dialogActivar = $("#dialog_activacion").removeClass('hide').dialog({
        modal: true,
        width: '600px',
        draggable: false,
        resizable: false,
        title: "<div class='widget-header widget-header-small'><h4 class='smaller'><i class='fa fa-exclamation-triangle'></i> Activar Registro de Unidad</h4></div>",
        title_html: true,
        buttons: [
            {
                html: "<i class='icon-remove bigger-110'></i>&nbsp; Cancelar",
                "class": "btn btn-xs",
                click: function() {
                    $(this).dialog("close");
                }
            },
            {
                html: "<i class='fa icon-ok bigger-110'></i>&nbsp; Activar",
                "class": "btn btn-success btn-xs",
                click: function() {
                    var data = {
                        id: registro_id
                    }

                    $.ajax({
                        url: "activarUnidad",
                        data: data,
                        dataType: 'html',
                        type: 'get',
                        success: function(resp, resp2, resp3) {

                            try {
                                var json = jQuery.parseJSON(resp3.responseText);

                                if (json.statusCode == "success") {

                                    refrescarGrid();
                                    dialogActivar.dialog('close');
                                    $("#msj_error_activar").addClass('hide');
                                    $("#msj_error_activar p").html('');
                                    $("#msj_exitoso").removeClass('hide');
                                    $("#msj_exitoso p").html(json.mensaje);
                                    $("html, body").animate({scrollTop: 0}, "fast");
                                }

                                if (json.statusCode == "error") {

                                    $("#pregunta_activar").addClass('hide');
                                    $("#msj_exitoso").addClass('hide');
                                    $("#msj_exitoso p").html('');
                                    $("#msj_error_activar").removeClass('hide');
                                    $("#msj_error_activar p").html(json.mensaje);
                                    $("html, body").animate({scrollTop: 0}, "fast");
                                }
                            }
                            catch (e) {

                            }
                        }
                    })
                }
            }
        ]
    });
    $("#dialog_activacion").show();
}





function refrescarGrid() {

    $('#unidad-grid').yiiGridView('update', {
        data: $(this).serialize()
    });
}


