$(document).ready(function()
{


    function verDialogo(message, title, style, clickCallback, reload, buttonsExtra) {
        if (!style)
            style = 'info';

        var buttonsDialog = [
            {
                html: "<i class='icon-remove bigger-110'></i>&nbsp; Cerrar",
                "class": "btn btn-xs",
                click: function() {
                    $(this).dialog("close");
                    if (clickCallback) {
                        clickCallback();
                    }
                    if (reload) {
                        window.location.reload();
                    }
                }
            }
        ];

        if (buttonsExtra)
            buttonsDialog.push(buttonsExtra);

        displayDialogBox('dialog_error', style, message);

        var dialog = $("#dialog_error").removeClass('hide').dialog({
            modal: true,
            width: '450px',
            draggable: false,
            resizable: false,
            title: "<div class='widget-header widget-header-small'><h4 class='smaller'><i class='fa fa-exclamation-triangle'></i> " + title + " </h4></div>",
            title_html: true,
            buttons: buttonsDialog
        });
    }

    $('#SubSeccionPrueba_tipo_pregunta').change(function()
    {
        var variable = $('#SubSeccionPrueba_tipo_pregunta').val();
        if (variable == 'D') {
            $('#SubSeccionPrueba_cantidad_respuesta').prop("readOnly", true);
            $('#SubSeccionPrueba_cantidad_respuesta').val(2);
        }
        else {
            $('#SubSeccionPrueba_cantidad_respuesta').prop("readOnly", false);
            $('#SubSeccionPrueba_cantidad_respuesta').val('');
        }
    });



    $('#SubSeccionPrueba_seccion_prueba_id').change(function() {



        if ($('#seccion_prueba_id').val() != '') //Esta Validacion Aplica en el Caso que esta Editando Un Registro
        {
            //if($('#SubSeccionPrueba_seccion_prueba_id').val()==$('#seccion_prueba_id').val())
            //  alert('No aplicar la Validacion');

            if ($('#SubSeccionPrueba_seccion_prueba_id').val() != $('#seccion_prueba_id').val())
            {


                var seccion_prueba_id = {id: $('#SubSeccionPrueba_seccion_prueba_id').val()};

                //alert(id); 
                $.ajax({
                    url: "/prueba/subSeccion/obtenerDatos",
                    data: seccion_prueba_id,
                    dataType: 'html',
                    type: 'post',
                    success: function(json) {


                        var json = jQuery.parseJSON(json);

                        if (json.statusCode == "success")
                        {
                            var duracion_seccion = json.duracion_seccion;
                            //alert(duracion_seccion);
                            $("#alerta").removeClass('hide');
                            $('#alerta p').html(json.mensaje);
                            var subSecciones = json.subSecciones;
                            var totalSubSecciones = json.totalSubSecciones;
                            if (subSecciones == totalSubSecciones)
                            {
                                var mensaje = '<b>Estimado usuario; la Sección Seleccionada tiene el Limite de SubSecciones, por favor intente nuevamente.</b>';
                                var title = 'Total de SubSecciones';
                                $("#SubSeccionPrueba_seccion_prueba_id").val('');
                                $("#alerta").addClass('hide');
                                $('#alerta p').html('');
                                verDialogo(mensaje, title);
                            }
                        }
                        if (json.statusCode == 'error')
                        {
                            $("#alerta").addClass('hide');
                            $('#alerta p').html('');
                        }
                    },
                });
            }
        }



        if ($('#seccion_prueba_id').val() == '') //Esta Validacion Aplica en el Caso de que este Registrando una Nueva SubSeccion
        {

            var seccion_prueba_id = {id: $('#SubSeccionPrueba_seccion_prueba_id').val()};


            $.ajax({
                url: "/prueba/subSeccion/obtenerDatos",
                data: seccion_prueba_id,
                dataType: 'html',
                type: 'post',
                success: function(json) {


                    var json = jQuery.parseJSON(json);

                    if (json.statusCode == "success")
                    {
                        var duracion_seccion = json.duracion_seccion;
                        //alert(duracion_seccion);
                        $("#alerta").removeClass('hide');
                        $('#alerta p').html(json.mensaje);
                        var subSecciones = json.subSecciones;
                        var totalSubSecciones = json.totalSubSecciones;
                        if (subSecciones == totalSubSecciones)
                        {
                            var mensaje = '<b>Estimado usuario; la Sección Seleccionada tiene el Limite de SubSecciones, por favor intente nuevamente.</b>';
                            var title = 'Total de SubSecciones';
                            $("#SubSeccionPrueba_seccion_prueba_id").val('');
                            $("#alerta").addClass('hide');
                            $('#alerta p').html('');
                            verDialogo(mensaje, title);
                        }
                    }
                    if (json.statusCode == 'error')
                    {
                        $("#alerta").addClass('hide');
                        $('#alerta p').html('');
                    }
                },
            });

        }


    });







    // VALIDACIONES DEL CGRIDVIEW
    $('#nombre').bind('keyup blur', function() {
        keyAlphaNum(this, true, true);
        makeUpper(this); //Convierte las palabras en mayusculas.
    });
    $.mask.definitions['H'] = '[0-1-2]';
    $.mask.definitions['Y'] = '[0|1|2|3]';
    $.mask.definitions['M'] = '[0|1|2|3|4|5]';
    $.mask.definitions['n'] = '[0123]'
    $.mask.definitions['~'] = '[+-]';
    $("#duracion_sub_seccion").mask("Hn:M9:M9"); // Filtrar el campo "duracion_seccion" del CGGridView

    // VALIDACIONES DEL FORMULARIO SeccionPrueba.
    $('#SubSeccionPrueba_nombre').bind('keyup blur', function() {
        keyAlphaNum(this, true, true); // Validar que solo sea Letras, Numeros y Espacios.
        makeUpper(this); //Convierte las palabras en mayusculas.
    });

    $('#SubSeccionPrueba_instruccion_sub_seccion').bind('keyup blur', function() {
        keyText(this, true, true); // validar Solo texto
        makeUpper(this);
    });

    //$("#SubSeccionPrueba_duracion_seccion").mask("H9:M9:M9"); // Valida el Campo "duracion_seccion" del formulario.

    $('#SubSeccionPrueba_cantidad_respuesta').bind('keyup blur', function() {
        keyNum(this, true, true); // validar Solo Numero
    });


    $("#sub-seccion-prueba-form").submit(function() {

        var seccion_prueba_id = $('#SubSeccionPrueba_seccion_prueba_id').val();
        // var duracion_seccion = $('#SubSeccionPrueba_duracion_seccion').val();
        var nombre_sec = $('#SubSeccionPrueba_nombre').val();
        var instrucciones = $("#instruccion_seccion1").html();
        //  instrucciones.trim();
        var instrucciones2 = $("#instruccion_seccion2").html();
        // instrucciones2.trim();
        var tipoPrueba = $("#SubSeccionPrueba_tipo_pregunta").val();
        var cantidadSeccion = $('#SubSeccionPrueba_cantidad_respuesta').val();


        //alert(instrucciones);
        //alert(tipoPrueba);
        var mensaje = '<b>Estimado usuario Asegurese de Llenar los Campos Correspondientes, por favor intente nuevamente.</b>';
        var title = 'Campos Vacios';

        if (nombre_sec == '')
        {
            //alert('pude entrar aqui');
            $('#SubSeccionPrueba_nombre').addClass('error');
            verDialogo(mensaje, title);
            return false;

        } else {
            $('#SubSeccionPrueba_nombre').removeClass('error');
        }
        if (seccion_prueba_id == '')
        {
            $('#SubSeccionPrueba_seccion_prueba_id').addClass('error');
            verDialogo(mensaje, title);
            return false;

        } else {
            $('#SubSeccionPrueba_seccion_prueba_id').removeClass('error');
        }

        // alert(cantidadSeccion);
        if (cantidadSeccion == '')
        {
            $('#SubSeccionPrueba_cantidad_respuesta').addClass('error');
            verDialogo(mensaje, title);
            return false;

        } else {
            $('#SubSeccionPrueba_cantidad_respuesta').removeClass('error');
        }
        console.log(tipoPrueba);
        if (tipoPrueba == '')
        {
            $('#SubSeccionPrueba_tipo_pregunta').addClass('error');
            verDialogo(mensaje, title);
            return false;
        } else {
            $('#SubSeccionPrueba_tipo_pregunta').removeClass('error');
        }

        if (instrucciones == '' && instrucciones2 == '')
        {
            // $('#instruccion_seccion1').class('border');
            document.getElementById('instruccion_seccion1').style.border = '1px solid ##ff3333';
            verDialogo(mensaje, title);
            return false;
        } else {
            document.getElementById('instruccion_seccion1').style.border = '1px solid #ccc';
        }

    });







    $('input[name="tieneSubSeccion"]').on('change', function() {
        if ($(this).val() == 'si') {
            $('#tipo_pregunta,#instruccion_seccion').hide();
            $('#cantidad_sub_seccion').show();
            $('#instruccion_seccion1').html('');
            $('#instruccion_seccion2').html('');
            $('#SeccionPrueba_cantidad_sub_seccion').val('');
            $('#SeccionPrueba_tipo_pregunta').val('');

        } else {
            $('#cantidad_sub_seccion').hide();
            $('#tipo_pregunta,#instruccion_seccion').show();
            $('#instruccion_seccion1').html('');
            $('#instruccion_seccion2').html('');
            $('#SeccionPrueba_cantidad_sub_seccion').val('');
            $('#SeccionPrueba_tipo_pregunta').val('');
        }
    });

    $("#SubSeccionPrueba_cantidad_respuesta").blur(function() {
        var cantidad_respuesta = $('#SubSeccionPrueba_cantidad_respuesta').val();
        if (cantidad_respuesta < 2) {
            var mensaje = '<b>Estimado usuario la cantidad de Repuestas debe ser mayor a 1, por favor intente nuevamente.</b>';
            var title = 'Cantidad de Repuestas';
            $("#SubSeccionPrueba_cantidad_respuesta").val('');
            verDialogo(mensaje, title);
        }
    });






    $("#SubSeccionPrueba_duracion_seccion").blur(function() {

        var duracion_sub_seccion = $('#SubSeccionPrueba_duracion_seccion').val();
        if (duracion_sub_seccion == '00:00:00' || duracion_sub_seccion == '' || duracion_sub_seccion == '__:__:__')
        {
            var mensaje = '<b>Estimado usuario el formato de duración ingresado es inválido, por favor intente nuevamente.</b>';
            var title = 'Formato de hora';
            $("#SubSeccionPrueba_duracion_seccion").val('');
            verDialogo(mensaje, title);
        }
        else
        {
            //if($('#SubSeccionPrueba_id').val()!='') alert('esta actualizando');   
            if ($('#SubSeccionPrueba_id').val() == '') {
                var seccion_prueba_id = {id: $('#SubSeccionPrueba_seccion_prueba_id').val()};
                $.ajax({
                    url: "/prueba/subSeccion/obtenerDatos",
                    data: seccion_prueba_id,
                    dataType: 'html',
                    type: 'post',
                    success: function(json) {
                        var json = jQuery.parseJSON(json);
                        if (json.statusCode == "success")
                        {
                            //var duracion_seccion= json.duracion_seccion; 
                            var totalSubSecciones = json.totalSubSecciones; // TOTAL de SUB SECCIONES REGISTRADAS A LA SECCION SELECCIONADA
                            var subSecciones = json.subSecciones; // Total de Sub Secciones que tiene la Seccion Seleccionada (No indica cuantas hay registradas)
                            var SumaTiemposSubSecciones = json.SumaTiemposSubSecciones; // Obtengo la Suma de las Sub-Secciones Registradas a la Seccion Seleccionada

                            if (totalSubSecciones == 0)
                            {
                                $('#algo').val(duracion_sub_seccion);
                            }
                            else
                            {
                                // Divido la Sumatoria en Horas, Minutos y Segundos.
                                var SumaTiemposSubSecciones_dividido = SumaTiemposSubSecciones.split(':');
                                var horas_SumaTiemposSubSecciones = parseInt(SumaTiemposSubSecciones_dividido[0]);
                                var minutos_SumaTiemposSubSecciones = parseInt(SumaTiemposSubSecciones_dividido [1]);
                                var segundos_SumaTiemposSubSecciones = parseInt(SumaTiemposSubSecciones_dividido [2]);

                                // Divido la Duracion de la Sub Seccion Ingresada en Horas, Minutos y Segundos
                                var duracion_sub_seccion_dividido = duracion_sub_seccion.split(':');
                                var horas_duracion_sub_seccion = parseInt(duracion_sub_seccion_dividido[0]);
                                var minutos_duracion_sub_seccion = parseInt(duracion_sub_seccion_dividido[1]);
                                var segundos_duracion_sub_seccion = parseInt(duracion_sub_seccion_dividido [2]);

                                var horas_nuevas = horas_SumaTiemposSubSecciones + horas_duracion_sub_seccion;
                                var minutos_nuevos = minutos_SumaTiemposSubSecciones + minutos_duracion_sub_seccion;
                                var segundos_nuevos = segundos_SumaTiemposSubSecciones + segundos_duracion_sub_seccion;


                                if (segundos_nuevos >= 60)
                                {
                                    segundos_nuevos = segundos_nuevos - 60;
                                    minutos_nuevos = minutos_nuevos + 1;
                                }
                                if (minutos_nuevos > 59)
                                {
                                    minutos_nuevos = minutos_nuevos - 60;
                                    horas_nuevas = horas_nuevas + 1;
                                }
                                if (horas_nuevas > 23)
                                {
                                    var mensaje = '<b>Estimado usuario, Ingrese otro rango de duración de sub-sección.</b>';
                                    var title = 'Duración de Sub-Sección';
                                    $("#SubSeccionPrueba_duracion_seccion").val('');
                                    verDialogo(mensaje, title);
                                }

                                //Pasando las Variables Numericas a Variables Tipo String.
                                horas_nuevas = horas_nuevas.toString();
                                minutos_nuevos = minutos_nuevos.toString();
                                segundos_nuevos = segundos_nuevos.toString();

                                //Si los Digitos de las horas, minutos y segundos es igual a 1, entonces agrego 0 a la izquierda, ejemplo: 3 -> 03.
                                if (horas_nuevas.length == 1)
                                    horas_nuevas = "0" + horas_nuevas;
                                if (minutos_nuevos.length == 1)
                                    minutos_nuevos = "0" + minutos_nuevos;
                                if (segundos_nuevos.length == 1)
                                    segundos_nuevos = "0" + segundos_nuevos;

                                var nuevo_tiempo_duracion = horas_nuevas + ':' + minutos_nuevos + ':' + segundos_nuevos;
                                $('#algo').val(nuevo_tiempo_duracion);

                            }



                        }
                    },
                });

            }


            if ($('#SubSeccionPrueba_id').val() != '')
            {
                //alert('ESTA EDITANDOOOO');
                var duracion_sub_seccion = $('#SubSeccionPrueba_duracion_seccion').val();
                var data = {id: $('#SubSeccionPrueba_seccion_prueba_id').val(), subSeccion_id: $('#SubSeccionPrueba_id').val()};
                $.ajax({
                    url: "/prueba/subSeccion/obtenerDatosDos",
                    data: data,
                    dataType: 'html',
                    type: 'post',
                    success: function(json) {
                        var json = jQuery.parseJSON(json);
                        if (json.statusCode == "success")
                        {
                            if (json.SumaTiemposSubSecciones == null)
                            {
                                $('#algo').val(duracion_sub_seccion);
                            }
                            if (json.SumaTiemposSubSecciones != null)
                            {
                                var SumaTiemposSubSecciones = json.SumaTiemposSubSecciones; // Obtengo la Suma de las Sub-Secciones Registradas a la Seccion Seleccionada

                                // Divido la Sumatoria en Horas, Minutos y Segundos.
                                var SumaTiemposSubSecciones_dividido = SumaTiemposSubSecciones.split(':');
                                var horas_SumaTiemposSubSecciones = parseInt(SumaTiemposSubSecciones_dividido[0]);
                                var minutos_SumaTiemposSubSecciones = parseInt(SumaTiemposSubSecciones_dividido [1]);
                                var segundos_SumaTiemposSubSecciones = parseInt(SumaTiemposSubSecciones_dividido [2]);

                                // Divido la Duracion de la Sub Seccion Ingresada en Horas, Minutos y Segundos
                                var duracion_sub_seccion_dividido = duracion_sub_seccion.split(':');
                                var horas_duracion_sub_seccion = parseInt(duracion_sub_seccion_dividido[0]);
                                var minutos_duracion_sub_seccion = parseInt(duracion_sub_seccion_dividido[1]);
                                var segundos_duracion_sub_seccion = parseInt(duracion_sub_seccion_dividido [2]);

                                var horas_nuevas = horas_SumaTiemposSubSecciones + horas_duracion_sub_seccion;
                                var minutos_nuevos = minutos_SumaTiemposSubSecciones + minutos_duracion_sub_seccion;
                                var segundos_nuevos = segundos_SumaTiemposSubSecciones + segundos_duracion_sub_seccion;


                                if (segundos_nuevos >= 60)
                                {
                                    segundos_nuevos = segundos_nuevos - 60;
                                    minutos_nuevos = minutos_nuevos + 1;
                                }
                                if (minutos_nuevos > 59)
                                {
                                    minutos_nuevos = minutos_nuevos - 60;
                                    horas_nuevas = horas_nuevas + 1;
                                }
                                if (horas_nuevas > 23)
                                {
                                    var mensaje = '<b>Estimado usuario, Ingrese otro rango de duración de sub-sección.</b>';
                                    var title = 'Duración de Sub-Sección';
                                    $("#SubSeccionPrueba_duracion_seccion").val('');
                                    verDialogo(mensaje, title);
                                }

                                //Pasando las Variables Numericas a Variables Tipo String.
                                horas_nuevas = horas_nuevas.toString();
                                minutos_nuevos = minutos_nuevos.toString();
                                segundos_nuevos = segundos_nuevos.toString();

                                //Si los Digitos de las horas, minutos y segundos es igual a 1, entonces agrego 0 a la izquierda, ejemplo: 3 -> 03.
                                if (horas_nuevas.length == 1)
                                    horas_nuevas = "0" + horas_nuevas;
                                if (minutos_nuevos.length == 1)
                                    minutos_nuevos = "0" + minutos_nuevos;
                                if (segundos_nuevos.length == 1)
                                    segundos_nuevos = "0" + segundos_nuevos;

                                var nuevo_tiempo_duracion = horas_nuevas + ':' + minutos_nuevos + ':' + segundos_nuevos;
                                $('#algo').val(nuevo_tiempo_duracion);

                            }
                        }
                    },
                });
            }




        }

    });



    function refrescarGrid() {

        $('#sub-seccion-prueba-grid').yiiGridView('update', {
            data: $(this).serialize()
        });
    }

    function inactivar(registro_id) {
        $("#pregunta_inactivar").removeClass('hide');
        $("#msj_error_inactivar").addClass('hide');
        $("#msj_error_inactivar p").html('');

        var dialogInactivar = $("#dialog_inactivacion").removeClass('hide').dialog({
            modal: true,
            width: '600px',
            draggable: false,
            resizable: false,
            title: "<div class='widget-header widget-header-small'><h4 class='smaller'><i class='fa fa-exclamation-triangle'></i> Inactivar Registro de Sub-sección de Prueba</h4></div>",
            title_html: true,
            buttons: [
                {
                    html: "<i class='icon-remove bigger-110'></i>&nbsp; Cancelar",
                    "class": "btn btn-xs",
                    click: function() {
                        $(this).dialog("close");
                    }
                },
                {
                    html: "<i class='icon-trash bigger-110'></i>&nbsp; Inactivar",
                    "class": "btn btn-danger btn-xs",
                    click: function() {
                        var data = {
                            id: registro_id
                        };
                        $.ajax({
                            url: "/prueba/subSeccion/inactivarSubSeccionPrueba",
                            data: data,
                            dataType: 'html',
                            type: 'post',
                            success: function(resp, resp2, resp3) {

                                try {
                                    var json = jQuery.parseJSON(resp3.responseText);

                                    if (json.statusCode == "success") {

                                        refrescarGrid();
                                        dialogInactivar.dialog('close');
                                        $("#msj_error_inactivar").addClass('hide');
                                        $("#msj_error_inactivar p").html('');
                                        $("#msj_exitoso").removeClass('hide');
                                        $("#msj_exitoso p").html(json.mensaje);
                                        $("html, body").animate({scrollTop: 0}, "fast");
                                    }

                                    if (json.statusCode == "error") {

                                        $("#pregunta_inactivar").addClass('hide');
                                        $("#msj_exitoso").addClass('hide');
                                        $("#msj_exitoso p").html('');
                                        $("#msj_error_inactivar").removeClass('hide');
                                        $("#msj_error_inactivar p").html(json.mensaje);
                                        $("html, body").animate({scrollTop: 0}, "fast");
                                    }
                                }
                                catch (e) {

                                }
                            }
                        })
                    }
                }
            ]
        });
        $("#dialog_inactivacion").show();
    }





    $(".add-inactivar").unbind('click');
    $(".add-inactivar").on('click', function(e) {
        e.preventDefault();

        var registro_id;
        registro_id = $(this).attr("data");

        inactivar(registro_id);
    });




    function activar(registro_id) {

        $("#pregunta_activar").removeClass('hide');
        $("#msj_error_activar").addClass('hide');
        $("#msj_error_activar p").html('');

        var dialogActivar = $("#dialog_activacion").removeClass('hide').dialog({
            modal: true,
            width: '600px',
            draggable: false,
            resizable: false,
            title: "<div class='widget-header widget-header-small'><h4 class='smaller'><i class='fa fa-exclamation-triangle'></i> Activar Registro de la Sección de Prueba</h4></div>",
            title_html: true,
            buttons: [
                {
                    html: "<i class='icon-remove bigger-110'></i>&nbsp; Cancelar",
                    "class": "btn btn-xs",
                    click: function() {
                        $(this).dialog("close");
                    }
                },
                {
                    html: "<i class='fa icon-ok bigger-110'></i>&nbsp; Activar",
                    "class": "btn btn-success btn-xs",
                    click: function() {
                        var data = {
                            id: registro_id
                        }

                        $.ajax({
                            url: "/prueba/subSeccion/activarSubSeccionPrueba",
                            data: data,
                            dataType: 'html',
                            type: 'get',
                            success: function(resp, resp2, resp3) {

                                try {
                                    var json = jQuery.parseJSON(resp3.responseText);

                                    if (json.statusCode == "success") {

                                        refrescarGrid();
                                        dialogActivar.dialog('close');
                                        $("#msj_error_activar").addClass('hide');
                                        $("#msj_error_activar p").html('');
                                        $("#msj_exitoso").removeClass('hide');
                                        $("#msj_exitoso p").html(json.mensaje);
                                        $("html, body").animate({scrollTop: 0}, "fast");
                                    }

                                    if (json.statusCode == "error") {

                                        $("#pregunta_activar").addClass('hide');
                                        $("#msj_exitoso").addClass('hide');
                                        $("#msj_exitoso p").html('');
                                        $("#msj_error_activar").removeClass('hide');
                                        $("#msj_error_activar p").html(json.mensaje);
                                        $("html, body").animate({scrollTop: 0}, "fast");
                                    }
                                }
                                catch (e) {

                                }
                            }
                        })
                    }
                }
            ]
        });
        $("#dialog_activacion").show();
    }



    $('.add-activar').unbind('click');
    $('.add-activar').on('click', function(e) {

        e.preventDefault();
        var registro_id = $(this).attr('data');
        var seccion_id = $(this).attr('seccion_id');
        var data = {seccion_id: seccion_id};
        $.ajax({
            url: "/prueba/subSeccion/verificarEstatusSeccion",
            data: data,
            type: 'post',
            success: function(json) {
                var json = jQuery.parseJSON(json);
                //console.log(json.estatusSeccion);
                if (json.estatusSeccion != 'I') {
                    activar(registro_id);
                }
                else {
                    var title = "Error de Activación";
                    var mensaje = "Estimado usuario, Para Habilitar Esta SubSección Habilite la Sección a la Que esta Asociada e Intente Nuevamente."
                    verDialogo(mensaje, title);
                }
            }
        });
        //activar(registro_id); 
    });


    $('#SubSeccionPrueba_seccion_prueba_id').change(function() {
        $('#SubSeccionPrueba_duracion_seccion').val('');
    });


    $("#SubSeccionPrueba_duracion_seccion").mask("Hn:M9:M9");
    $('#SubSeccionPrueba_duracion_seccion').focus(function()
    {
        if ($('#SubSeccionPrueba_seccion_prueba_id').val() == '')
        {
            var title = "Error";
            var mensaje = "Estimado usuario, para ingresar la Duraccion de esta Sub-Seccion debe Escoger previamente la Seccion Correspondiente."
            $('#SubSeccionPrueba_duracion_seccion').blur();
            verDialogo(mensaje, title);
        }
    });




    $(".editar-datos").unbind('click');
    $(".editar-datos").on('click', function(e) {
        e.preventDefault();

        var registro_id;
        registro_id = $(this).attr("data");
        var data = {id: registro_id};


        //return false;
        $.ajax({
            url: "/prueba/subSeccion/verificarEstatus",
            data: data,
            dataType: 'html',
            type: 'post',
            success: function(json) {
                try {
                    var json = jQuery.parseJSON(json);

                    if (json.statusCode == "success")
                    {
                        var mensaje = '<b>Estimado Usuario: La Seccion Seleccionada ya ha sido utilizada para las pruebas Correspondientes, No puede editar este Registro</b>';
                        var title = 'Edicion de Seccion';
                        //return false;
                        verDialogo(mensaje, title);
                        //alert('no puede editar este registro');
                    }
                    if (json.statusCode == "error") {
                        //$(".editar-datos").unbind('click');
                        //$(".editar-datos").click();
                        var ruta = json.mensaje;
                        //alert('puede pasar tranquilamente');
                        location.href = ruta + "/prueba/subSeccion/edicion/id/" + registro_id;

                    }
                }
                catch (e) {

                }
            }
        });

        //return false;    
    });


});

