/**
 * Created by nelson on 27/03/15.
 */

$(document).ready(function() {
    $('#nivel-form').on('submit', function(evt) {
        evt.preventDefault();
        registrarArticulo();
    });

    $('#Articulo_nombre_form').bind('keyup blur', function() {
    makeUpper(this);
    keyAlphaNum(this, true, true);
    });

    $('#Articulo_precio_regulado_form').bind('keyup blur', function() {
    keyNum(this, true);
    });

    $('#Articulo_precio_baremo_form').bind('keyup blur', function() {
    keyNum(this, true);
    });

    $('#Articulo_serial_form').bind('keyup blur', function() {
    makeUpper(this);
    keyAlphaNum(this, true, true);
    });

    $('#Articulo_presentacion_form').bind('keyup blur', function() {
    makeUpper(this);
    keyAlphaNum(this, true, true);
    });
    $('#Articulo_meses_vida_util_form').bind('keyup blur', function() {
    keyNum(this, true);
    });

    $('#Articulo_marca_id_form').bind('keyup blur', function() {
    keyNum(this, true);
    });

    $('#Articulo_tipo_articulo_id').bind('change', function() {
    var id = $("#Articulo_tipo_articulo_id").val();
    if(id == 1) {
    $("#franja").removeClass('hidden');
    }
    else {
    $("#franja").addClass('hidden');
    }
    });
    var id = $("#Articulo_tipo_articulo_id").val();
    if(id == 1) {
    $("#franja").removeClass('hidden');
    }
    else {
    $("#franja").addClass('hidden');
    }

});
