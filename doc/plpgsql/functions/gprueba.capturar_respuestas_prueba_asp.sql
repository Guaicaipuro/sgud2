

--DROP FUNCTION gprueba.puede_presentar_examen(bigint, bigint, character varying, character varying, character varying, pregunta_id_vi BIGINT[], respuesta_id_vi BIGINT[]);

CREATE OR REPLACE FUNCTION gprueba.capturar_respuestas_prueba_asp(
    aplicacion_prueba_id_vi INT,
    pregunta_id_vi BIGINT[],
    respuesta_id_vi BIGINT[],
    data_pregunta_respuesta_vi TEXT,
    aspirante_id_vi BIGINT,
    apertura_periodo_id_vi INT,
    seccion_prueba_id_vi INT,
    calificacion_final_vi NUMERIC,
    cantidad_respuestas_correctas_vi SMALLINT, 
    cantidad_respuestas_incorrectas_vi SMALLINT, 
    finalizada_por_vi VARCHAR, --crear campo aplicar prueba: USUARIO, TIEMPO
    hora_finalizada_real_vi TIME,
    postulacion_id_vi BIGINT,
    usuario_ini_id_vi BIGINT,
    fecha_act_vi TIMESTAMP
)
RETURNS RECORD AS
$BODY$
--Declarando las variables  
DECLARE

--  Variables Constantes.
    const_postulacion_beneficiario SMALLINT := 6;
    const_postulacion_no_beneficiario SMALLINT := 7;
    const_prueba_culminada_v SMALLINT := 2;
    const_prueba_iniciada_v SMALLINT := 1;

--  variable
    aprobado_prueba_v BOOLEAN := true;

--  Variables de Bucle
    indice_v INT := 1;
    length_pregunta_id_v INT := 0;
    length_respuesta_id_v INT := 0;
    lista_id_preguntas_v BIGINT[];

--  Variables RETURNING
    insert_respuesta_prueba_asp_id BIGINT := null;
    update_aplicacion_prueba_id_v BIGINT := null;
    update_postulacion_id_v BIGINT := null;

--  Variables Captura
    captura_estatus_prueba_aplicada_v INT := null; -- Indica 1: Si se esta iniciando la prueba o 2: Si ya se culmino 
    captura_hora_final_prueba_vi TIME WITHOUT TIME ZONE := now(); -- Indica el tiempo final de la prueba
    captura_estado_final_postulacion_v SMALLINT := null;
    captura_puntuacion_minima_aprobacion_v NUMERIC := null;

--  Variables Existencia 
    existencia_apertura_periodo_id_v SMALLINT := 0;
    existencia_aplicacion_prueba_v SMALLINT := 0;
    existencia_postulacion_id_v SMALLINT := 0;
    existencia_seccion_prueba_id_v SMALLINT := 0;
    existencia_talento_humano_id_v SMALLINT := 0;
    existencia_usuario_ini_id_v SMALLINT := 0;
    existencia_configuracion_puntuacion_v SMALLINT := 0;

--  Variables de Resultado
    estatus_v VARCHAR := 'error';
    resultado RECORD;

--  Variables de exception
--      Seccion que permite localizar hasta que punto corrio el codigo hasta que se genero el error.
    mensaje_usuario_v VARCHAR := 'No se pudo Registrar las Respuestas de la Prueba. Contacte con el Administrador (000).';
    mensaje_sistema_v VARCHAR := 'Ha Ocurrido un Error en el PLPGSQL.'; --SQLERRM
    seccion_v SMALLINT := 0;

--  Variables de auditoria
    data_v TEXT := data_pregunta_respuesta_vi;
    fecha_hora_v DATE := NOW();
    ipaddress_v VARCHAR := ''; -- Si es necesario capturar la ip del servidor donde se aloja la base de datos se usa: 'inet_client_addr();'
    mensaje_auditoria_v VARCHAR := null;
    modulo_v VARCHAR := 'prueba.Aplicacion.accion'; --Modulo.Controlador.Accion 
    tipo_transaccion_v VARCHAR := 'ESCRITURA'; --Si es ESCRITURA, ELIMINACION, EDICION o LECTURA
    transaccion_v VARCHAR := null;
    username_v VARCHAR := '';

BEGIN

    seccion_v := 1;


    SELECT count(1) INTO existencia_configuracion_puntuacion_v FROM sistema.configuracion WHERE nombre = 'CALIFICACION MINIMA DE APROBACION';
    IF existencia_configuracion_puntuacion_v > 0 THEN
        SELECT valor_num INTO captura_puntuacion_minima_aprobacion_v FROM sistema.configuracion WHERE nombre = 'CALIFICACION MINIMA DE APROBACION';
    END IF;

    IF calificacion_final_vi < captura_puntuacion_minima_aprobacion_v  THEN
        captura_estado_final_postulacion_v := const_postulacion_no_beneficiario;
    ELSEIF calificacion_final_vi >= captura_puntuacion_minima_aprobacion_v THEN
        captura_estado_final_postulacion_v := const_postulacion_beneficiario;
    END IF;

--  Validar si la aplicación de la prueba ya fue realizada anteriormente.
    SELECT count(1) INTO existencia_aplicacion_prueba_v FROM gprueba.aplicacion_prueba WHERE id = aplicacion_prueba_id_vi AND estatus_prueba_aplicada = const_prueba_culminada_v;
    IF existencia_aplicacion_prueba_v > 0 THEN
        mensaje_usuario_v := 'La prueba ya fue realizada.';
        RAISE EXCEPTION 'El Valor del Campo estatus_prueba_aplicada del Registro con id (%) de la Tabla gprueba.aplicacion_prueba ya posee el Estado "Prueba Culminada" (%).', aplicacion_prueba_id_vi, const_prueba_culminada_v;
    END IF;

    seccion_v := 2;
    SELECT array_length(pregunta_id_vi, 1) INTO length_pregunta_id_v;
    seccion_v := 3;
    SELECT array_length(respuesta_id_vi, 1) INTO length_respuesta_id_v;

    IF length_pregunta_id_v != length_respuesta_id_v THEN
        mensaje_usuario_v := 'El Número de Preguntas no coincide con el Número de Respuestas.';
        RAISE EXCEPTION 'La Longitud del array de Preguntas no coincide con la Longitud del array de Respuestas. (length_pregunta_id_v:% != length_respuesta_id_v:%)',length_pregunta_id_v ,length_respuesta_id_v;
    END IF;

    seccion_v := 4;
    FOR indice_v IN 1..length_pregunta_id_v LOOP
        IF (pregunta_id_vi)[indice_v] NOT IN (SELECT id FROM gprueba.pregunta) THEN
            mensaje_usuario_v := 'El Número de Preguntas no coinciden con el Número de Respuestas.';
            RAISE EXCEPTION 'La pregunta_id: % no se encuentra en la Tabla gprueba.pregunta.', (pregunta_id_vi)[indice_v];
        END IF;
        IF (respuesta_id_vi)[indice_v] NOT IN (SELECT id FROM gprueba.respuesta) THEN
            mensaje_usuario_v := 'El Número de Preguntas no coinciden con el Número de Respuestas.';
            RAISE EXCEPTION 'El Valor pregunta_id (%) no se encuentra en la Tabla gprueba.pregunta de la Base de Datos.', (pregunta_id_vi)[indice_v];
        END IF;
    END LOOP;

    seccion_v := 5;
    SELECT COUNT(1) INTO existencia_talento_humano_id_v FROM gestion_humana.talento_humano WHERE id = aspirante_id_vi;
    IF existencia_talento_humano_id_v = 0 THEN
        mensaje_usuario_v := 'El Talento Humano no se encuentra Registrado en el Sistema.';
        RAISE EXCEPTION 'El Talento Humano con id: % no se encuentra en la Base de Datos.', aspirante_id_vi;
    END IF;

    seccion_v := 6;
    SELECT COUNT(1) INTO existencia_apertura_periodo_id_v FROM gestion_humana.apertura_periodo WHERE id = apertura_periodo_id_vi;
    IF existencia_apertura_periodo_id_v = 0 THEN
        mensaje_usuario_v := 'El Periodo Aperturado no se encuentra disponible. Puede intentar Borrar la Cache e Ingresar al Sistema Nuevamente.';
        RAISE EXCEPTION 'El Valor del Campo apertura_id (%) no se encuentra en la Tabla gestion_humana.apertura_periodo de la Base de Datos.', apertura_periodo_id_vi;
    END IF;

    seccion_v := 7;
    SELECT COUNT(1) INTO existencia_seccion_prueba_id_v FROM gprueba.seccion_prueba WHERE id = seccion_prueba_id_vi;
    IF existencia_seccion_prueba_id_v = 0 THEN
        mensaje_usuario_v := 'La Seccion de la Prueba no se encuentra Disponible.';
        RAISE EXCEPTION 'El Valor (%) del Campo seccion_prueba_id no se encuentra en la Tabla gprueba.seccion_prueba de la base de datos.', seccion_prueba_id_vi;
    END IF;

    seccion_v := 8;
    SELECT COUNT(1) INTO existencia_usuario_ini_id_v FROM seguridad.usergroups_user WHERE id = usuario_ini_id_vi;
    IF existencia_usuario_ini_id_v = 0 THEN
        mensaje_usuario_v := 'El Usuario no se encuentra Registrado. Puede intentar borrar Cache y vuelva a Ingresar al Sistema con su Usuario.';
        RAISE EXCEPTION 'El Valor (%) no se encuentra en la Tabla seguridad.usergroups_user de la base de datos.', usuario_ini_id_vi;
    END IF;

    IF cantidad_respuestas_correctas_vi > length_respuesta_id_v THEN
        mensaje_usuario_v := 'La Cantidad de Respuestas Correctas supera la Cantidad Base de Respuestas Encontradas, La Data esta Corrupta.';
        RAISE EXCEPTION 'La Cantidad de Respuestas Correctas (%) Supera a la Cantidad de Respuestas Existentes (%)', cantidad_respuestas_correctas_vi, length_respuesta_id_v;
    END IF;

    IF cantidad_respuestas_incorrectas_vi > length_respuesta_id_v THEN
        mensaje_usuario_v := 'La Cantidad de Respuestas Incorrectas supera la Cantidad Base de Respuestas Encontradas, La Data esta Corrupta.';
        RAISE EXCEPTION 'La Cantidad de Respuestas incorrectas (%) Supera a la Cantidad de Respuestas Existentes (%)', cantidad_respuestas_incorrectas_vi, length_respuesta_id_v;
    END IF;

    seccion_v := 9;
    SELECT COUNT(1) INTO existencia_postulacion_id_v FROM gestion_humana.postulacion WHERE id = postulacion_id_vi;
    IF existencia_postulacion_id_v = 0 THEN
        mensaje_usuario_v := 'La Postulacion no se encuentra Disponible, intente Borrara Cache e Ingresas al Sistema nuevamente.';
        RAISE EXCEPTION 'La Postulacion id: % no se Encuentra Registrada en la Base de Datos.', postulacion_id_vi;
    END IF;


    indice_v := 1;
    seccion_v := 10;
    FOR indice_v IN 1..length_pregunta_id_v LOOP
        INSERT INTO 
            gprueba.respuesta_prueba_asp (
                aplicacion_prueba_id,
                pregunta_id,
                respuesta_id,
                usuario_ini_id,
                usuario_act_id,
                aspirante_id,
                apertura_periodo_id,
                seccion_prueba_id
            ) 
            VALUES (
                aplicacion_prueba_id_vi,
                (pregunta_id_vi)[indice_v],
                (respuesta_id_vi)[indice_v],
                usuario_ini_id_vi,
                usuario_ini_id_vi,
                aspirante_id_vi,
                apertura_periodo_id_vi,
                seccion_prueba_id_vi
            ) RETURNING id INTO insert_respuesta_prueba_asp_id;

        IF insert_respuesta_prueba_asp_id is null THEN
            mensaje_usuario_v := 'No se pudo Registrar las Respuestas de la Prueba. Contacte con el Administrador (001).';
            RAISE EXCEPTION 'Ocurrio un Error al Insertar los Datos en la Tabla gprueba.respuesta_prueba_asp.';
        END IF;
    END LOOP;

    seccion_v := 11;
    UPDATE 
        gprueba.aplicacion_prueba 
    SET
        estatus_prueba_aplicada = const_prueba_culminada_v,
        hora_final_real = hora_finalizada_real_vi,
        hora_final_prueba = now(),
        puntuacion_prueba = calificacion_final_vi,
        cantidad_respuestas_correctas = cantidad_respuestas_correctas_vi,
        cantidad_respuestas_incorrectas = cantidad_respuestas_incorrectas_vi,
        finalizada_por = finalizada_por_vi,
        usuario_act_id = usuario_ini_id_vi,
        fecha_act = fecha_act_vi,
        data_pregunta_respuesta = data_pregunta_respuesta_vi
    WHERE 
        id = aplicacion_prueba_id_vi 
    RETURNING 
        id INTO update_aplicacion_prueba_id_v;

    IF update_aplicacion_prueba_id_v is null THEN
        mensaje_usuario_v := 'No se pudo Registrar las Respuestas de la Prueba. Contacte con el Administrador (002).';
        RAISE EXCEPTION 'Ocurrio un Error en la Actualizacion de la Tabla gprueba.aplicacion_prueba.';
    END IF;

    seccion_v := 12;
    UPDATE gestion_humana.postulacion SET 
        puntuacion_prueba = calificacion_final_vi, 
        estatus_aprobado = captura_estado_final_postulacion_v,
        usuario_act_id = usuario_ini_id_vi,
        fecha_act = fecha_act_vi
    WHERE id = postulacion_id_vi RETURNING id INTO update_postulacion_id_v;

    IF update_postulacion_id_v is null THEN
        mensaje_usuario_v := 'No se pudo Registrar las Respuestas de la Prueba. Contacte con el Administrador (003).';
        RAISE EXCEPTION 'Ocurrio un Error en la Actualizacion de la Tabla gestion_humana.postulacion';
    ELSE
        mensaje_sistema_v := 'La Funcion gprueba.capturar_respuestas_prueba_asp se ha Completado Exitosamente.';
        mensaje_usuario_v := 'Los Datos de la Prueba de Conocimientos se han Registrado Satisfactoriamente. Ha Obtenido una Puntuación de '||calificacion_final_vi||' con '||cantidad_respuestas_correctas_vi||' Respuesta(s) Correcta(s) y '||cantidad_respuestas_incorrectas_vi||' Respuesta(s) Incorrecta(s).';
        estatus_v := 'success';
    END IF;

    seccion_v := 13;
    SELECT
        mensaje_sistema_v,
        mensaje_usuario_v,
        estatus_v,
        seccion_v,
        captura_estado_final_postulacion_v
    INTO 
        resultado;

--  AUDITORIA

    INSERT INTO 
        auditoria.traza(
            fecha_hora, 
            ip_maquina, 
            tipo_transaccion, 
            modulo, 
            resultado_transaccion, 
            descripcion, 
            user_id, 
            username, 
            data
        ) VALUES (
            fecha_hora_v, 
            ipaddress_v, 
            tipo_transaccion_v, 
            modulo_v, 
            transaccion_v, 
            mensaje_auditoria_v,
            usuario_ini_id_vi, 
            username_v, 
            data_v
        );
    
    RETURN resultado;

EXCEPTION

    WHEN OTHERS THEN
    transaccion_v := SQLSTATE;
    mensaje_sistema_v := SQLERRM;
    estatus_v := 'error';
    mensaje_auditoria_v := 'ALMACENAR LAS RESPUESTAS DE LA PRUEBA: '||SQLERRM||' (ERROR NRO: '||SQLSTATE||') (Seccion: '||seccion_v||') (Resultado: '||transaccion_v||')';
--  AUDITORIA
    
    INSERT INTO 
        auditoria.traza(
            fecha_hora, 
            ip_maquina, 
            tipo_transaccion, 
            modulo, 
            resultado_transaccion, 
            descripcion, 
            user_id, 
            username, 
            data
        ) VALUES (
            fecha_hora_v, 
            ipaddress_v, 
            tipo_transaccion_v, 
            modulo_v, 
            transaccion_v, 
            mensaje_auditoria_v,
            usuario_ini_id_vi, 
            username_v, 
            data_v
        );

    SELECT
        mensaje_sistema_v,
        mensaje_usuario_v,
        estatus_v,
        seccion_v,
        captura_estado_final_postulacion_v
    INTO 
        resultado;

    RETURN resultado;

END;$BODY$
LANGUAGE plpgsql VOLATILE
COST 100;

/*
select * FROM gprueba.capturar_respuestas_prueba_asp(
     36::INT, --aplicacion_prueba_id_vi INT
     array[15,14,13,9,11,12,8,10,17,16]::BIGINT[], --pregunta_id_vi BIGINT[]
     array[56,53,47,29,39,42,21,32,66,null]::BIGINT[], --respuesta_id_vi BIGINT[]
     17957::BIGINT, --aspirante_id_vi BIGINT
     1::INT, --apertura_periodo_id_vi INT
     6::INT, --seccion_prueba_id_vi INT
     70::NUMERIC, --calificacion_final_vi NUMERIC
     7::SMALLINT , --cantidad_respuestas_correctas_vi SMALLINT 
     3::SMALLINT , --cantidad_respuestas_incorrectas_vi SMALLINT 
     'USUARIO'::VARCHAR , --finalizada_por_vi VARCHAR 
     '4:39'::TIME, --hora_finalizada_real_vi TIME
     8::BIGINT, --postulacion_id_vi BIGINT
     65::BIGINT, --usuario_ini_id_vi BIGINT
     '2015-09-18'::TIMESTAMP --fecha_act_vi TIMESTAMP
) as f (
    mensaje_sistema_v varchar, 
    mensaje_usuario_v varchar, 
    estatus_v varchar,
    seccion_v INT
    );
*/