CREATE OR REPLACE FUNCTION etls.captura_log_estado(
	descripcion_v TEXT,
	nombre_ktr_v VARCHAR,
	nombre_kjb_v VARCHAR,
	observaciones_v TEXT,
	seccion_v TEXT
) RETURNS VARCHAR AS
$BODY$
DECLARE

--	VARIABLES
	base_datos_origen_v VARCHAR := 'dbseguro';
	base_datos_destino_v VARCHAR := 'app_concurso_merito';
	servidor_ip_origen_v VARCHAR := '172.16.3.220';
	servidor_ip_destino_v VARCHAR := '172.16.3.115';
	fecha_ejecucion_v DATE := NOW();
	estado_ejecucion_v VARCHAR;

--	Variables de existencia 
	existencia_estado_a_v INT := 0;
	existencia_etl_v INT := 0;

--	Variables de captura
	captura_log_estado_id_v BIGINT := null;

BEGIN


	IF (seccion_v = 'EXCEPTION') THEN
		estado_ejecucion_v := 'E';
	ELSEIF (seccion_v = 'EXITO') THEN
		estado_ejecucion_v := 'S';
	END IF;

	SELECT COUNT(1) INTO existencia_estado_a_v FROM etls.proceso_etl_trigger WHERE fecha_ejecucion < NOW()::DATE AND nombre_kjb = nombre_kjb_v AND nombre_ktr = nombre_ktr_v AND estatus = 'A';
	IF (existencia_estado_a_v > 0) THEN
		UPDATE etls.proceso_etl_trigger SET estatus = 'I' WHERE fecha_ejecucion < NOW()::DATE AND nombre_kjb = nombre_kjb_v AND nombre_ktr = nombre_ktr_v AND estatus = 'A';
	END IF;

	SELECT COUNT(1) INTO existencia_etl_v FROM etls.proceso_etl_trigger WHERE fecha_ejecucion = NOW()::DATE AND nombre_kjb = nombre_kjb_v AND nombre_ktr = nombre_ktr_v AND estatus = 'A';
	IF (existencia_etl_v = 0) THEN
		INSERT INTO etls.proceso_etl_trigger (
			descripcion,
			base_datos_origen,
			base_datos_destino,
			servidor_ip_origen,
			servidor_ip_destino,
			nombre_kjb,
			nombre_ktr,
			fecha_ejecucion,
			estado_ejecucion
		)VALUES(
			descripcion_v,
			base_datos_origen_v,
			base_datos_destino_v,
			servidor_ip_origen_v,
			servidor_ip_destino_v,
			nombre_kjb_v,
			nombre_ktr_v,
			fecha_ejecucion_v,
			estado_ejecucion_v
		);
	ELSEIF (estado_ejecucion_v = 'E' AND existencia_etl_v = 1) THEN
		UPDATE etls.proceso_etl_trigger SET estado_ejecucion = estado_ejecucion_v WHERE fecha_ejecucion = NOW()::DATE AND nombre_kjb = nombre_kjb_v AND nombre_ktr = nombre_ktr_v AND estatus = 'A';
		SELECT id INTO captura_log_estado_id_v FROM etls.proceso_etl_trigger WHERE fecha_ejecucion = NOW()::DATE AND nombre_kjb = nombre_kjb_v AND nombre_ktr = nombre_ktr_v AND estatus = 'A';
		INSERT INTO etls.captura_log_errores (captura_log_estado_id, descripcion, nombre_ktr) VALUES (captura_log_estado_id_v, observaciones_v, nombre_ktr_v);
	END IF;

	RETURN null;
END;
$BODY$
LANGUAGE plpgsql VOLATILE
COST 100;
