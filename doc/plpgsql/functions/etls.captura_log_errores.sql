-- Function: etls.particionamiento_log_errores()

-- DROP FUNCTION etls.particionamiento_log_errores();

CREATE OR REPLACE FUNCTION etls.particionamiento_log_errores()
  RETURNS trigger AS
$BODY$
DECLARE   

--  Variables de Existencia
existencia_id_v INT := 0;

BEGIN 

SELECT COUNT(1) INTO existencia_id_v FROM etls.captura_log_errores WHERE id = NEW.id;
IF (existencia_id_v > 0) THEN
  RAISE EXCEPTION 'Ya existe un valor id = "%" registrado e la tabla etls.captura_log_errores, el campo id es unico.', NEW.id;
END IF;

/*
**  Lista de Transformaciones.
**    1: TRANS_CONTRATADOS_TCARGO
**    2: TRANS_CONTRATADOS_TDEPENDENCIA
**    3: TRANS_CONTRATADOS_TPERSONA
**    4: TRANS_FIJOS_TCARGO
**    5: TRANS_FIJOS_TDEPENDENCIA
**    6: TRANS_FIJOS_TPERSONA
**    7: TRANS_JUBILADOS_TPERSONA
*/

-- Segun la lista de las transormaciones, inserta en su respectiva tabla donde cada Trigger posee su tabla de log de errores.
IF (NEW.nombre_ktr = 'TRANS_CONTRATADOS_TCARGO') THEN
  INSERT INTO etls.captura_log_errores_trans_mirror_contratados_tcargo  VALUES (NEW.*);
ELSEIF (NEW.nombre_ktr = 'TRANS_CONTRATADOS_TDEPENDENCIA') THEN
  INSERT INTO etls.captura_log_errores_trans_mirror_contratados_tdependencia  VALUES (NEW.*);
ELSEIF (NEW.nombre_ktr = 'TRANS_CONTRATADOS_TPERSONA') THEN
  INSERT INTO etls.captura_log_errores_trans_mirror_contratados_tpersona  VALUES (NEW.*);
ELSEIF (NEW.nombre_ktr = 'TRANS_FIJOS_TCARGO') THEN
  INSERT INTO etls.captura_log_errores_trans_mirror_fijos_tcargo  VALUES (NEW.*);
ELSEIF (NEW.nombre_ktr = 'TRANS_FIJOS_TDEPENDENCIA') THEN
  INSERT INTO etls.captura_log_errores_trans_mirror_fijos_tdependencia  VALUES (NEW.*);
ELSEIF (NEW.nombre_ktr = 'TRANS_FIJOS_TPERSONA') THEN
  INSERT INTO etls.captura_log_errores_trans_mirror_fijos_tpersona  VALUES (NEW.*); 
ELSEIF (NEW.nombre_ktr = 'TRANS_JUBILADOS_TPERSONA') THEN
  INSERT INTO etls.captura_log_errores_trans_mirror_jubilados_tpersona  VALUES (NEW.*);
ELSE
  RAISE EXCEPTION 'No se pudo encontrar el nombre de la transformacion en la lista de la particion, Verificar el nombre de los ETL con los listados en el codigo del trigger.';
END IF;

  RETURN NULL;
END;
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;

  -- Trigger: etls_particionamiento_log_errores on etls.captura_log_errores

-- DROP TRIGGER etls_particionamiento_log_errores ON etls.captura_log_errores;

CREATE TRIGGER etls_particionamiento_log_errores
  BEFORE INSERT OR UPDATE
  ON etls.captura_log_errores
  FOR EACH ROW
  EXECUTE PROCEDURE etls.particionamiento_log_errores();
