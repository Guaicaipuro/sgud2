CREATE OR REPLACE FUNCTION gestion_humana.cancelar_postulacion(
  tipo_periodo_id_vi SMALLINT,
  postulacion_id_vi BIGINT,
  talento_humano_id_vi BIGINT,
  usuario_ini_id_vi BIGINT,
  motivo_eliminacion_id_vi SMALLINT,
  observacion_cancelar_postulacion_vi VARCHAR,
  ipaddress_vi VARCHAR
)
  RETURNS RECORD AS
  $BODY$
  --Declarando las variables
  DECLARE

    --  Variables de Resultado
    resultado record;

    --  Variables de exception
    --      Seccion que permite localizar hasta que punto corrio el codigo hasta que se genero el error.
    seccion_v SMALLINT := 0;
    mensaje_v VARCHAR := 'Aun no ha generado el mensaje para esta respuesta.'; --SQLERRM

--  Variables de auditoria
    fecha_hora_v DATE := NOW();
    tipo_transaccion_v VARCHAR := 'ESCRITURA'; --Si es ESCRITURA, ELIMINACION, EDICION o LECTURA
    modulo_v VARCHAR := 'gestionHumana.Postulacion.cancelarPostulacion'; --Modulo.Controlador.Accion
    transaccion_v VARCHAR := null;
    resultado_transaccion_v VARCHAR := null;
    mensaje_auditoria_v VARCHAR := null;
    username_v VARCHAR := '';
    data_v TEXT := '';

    --  existencia
    existencia_talento_humano_id_v INT := 0;
    existencia_postulacion_id_v INT := 0;
    existencia_postulacion_eliminada_v INT := 0;
    existencia_usuario_ini_id_v INT := 0;
    existencia_motivo_eliminacion_id_v INT := 0;
    existencia_tipo_periodo_id_v INT := 0;
    captura_apertura_periodo_id_v INT := 0;


  BEGIN

    seccion_v := 1;

    SELECT COUNT(1) INTO existencia_talento_humano_id_v FROM gestion_humana.talento_humano WHERE id = talento_humano_id_vi;
    IF existencia_talento_humano_id_v = 0 THEN
      mensaje_v := 'El Talento Humano no se encuentra en la Base de Datos.';
      resultado_transaccion_v := 'alert';
      RAISE EXCEPTION 'El Talento Humano no se encuentra registrado en la Base de Datos.';
    END IF;

    seccion_v := 2;

    SELECT COUNT(1) INTO existencia_tipo_periodo_id_v FROM gestion_humana.apertura_periodo WHERE tipo_apertura_id = tipo_periodo_id_vi AND estatus = 'A';
    IF existencia_tipo_periodo_id_v = 0 THEN
      mensaje_v := 'El tipo de Apertura no se encuentra Aperturado.';
      resultado_transaccion_v := 'alert';
      RAISE EXCEPTION 'El tipo de Apertura no se encuentra Aperturado.';
    END IF;
    SELECT id INTO captura_apertura_periodo_id_v FROM gestion_humana.apertura_periodo WHERE tipo_apertura_id = tipo_periodo_id_vi AND estatus = 'A';

    SELECT COUNT(1) INTO existencia_postulacion_id_v FROM gestion_humana.postulacion WHERE id = postulacion_id_vi AND talento_humano_id = talento_humano_id_vi AND apertura_periodo_id = captura_apertura_periodo_id_v;
    IF existencia_postulacion_id_v = 0 THEN
      mensaje_v := 'La Postulación no se encuentra registrada en la Base de Datos.';
      resultado_transaccion_v := 'alert';
      RAISE EXCEPTION 'La Postulación no se encuentra registrada en la Base de Datos.';
    END IF;

    seccion_v := 3;

    SELECT COUNT(1) INTO existencia_usuario_ini_id_v FROM seguridad.usergroups_user WHERE id = usuario_ini_id_vi;
    IF existencia_usuario_ini_id_v = 0 THEN
      mensaje_v := 'El usuario no se encuentra registrado en la Base de Datos.';
      resultado_transaccion_v := 'alert';
      RAISE EXCEPTION 'El usuario no se encuentra registrado en la Base de Datos.';
    END IF;

    seccion_v := 4;

    SELECT COUNT(1) INTO existencia_motivo_eliminacion_id_v FROM gestion_humana.motivo_eliminacion_postulacion WHERE id = motivo_eliminacion_id_vi;
    IF existencia_motivo_eliminacion_id_v = 0 THEN
      mensaje_v := 'El motivo de la eliminacion de la Postulación seleccionado no se encuentra registrado en la Base de Datos.';
      resultado_transaccion_v := 'alert';
      RAISE EXCEPTION 'El motivo de la eliminacion de la Postulación seleccionado no se encuentra registrado en la Base de Datos.';
    END IF;

    seccion_v := 5;

    SELECT COUNT(1) INTO existencia_postulacion_eliminada_v FROM gestion_humana.postulacion_eliminada WHERE talento_humano_id = talento_humano_id_vi AND apertura_periodo_id = captura_apertura_periodo_id_v;
    IF existencia_postulacion_eliminada_v >= 2 THEN
      mensaje_v := 'Usted ya supero el limite de postulaciones que puede cancelar por este periodo.';
      resultado_transaccion_v := 'alert';
      RAISE EXCEPTION 'Usted ya supero el limite de postulaciones que puede cancelar por este periodo.';
    END IF;

    seccion_v := 6;

    INSERT INTO gestion_humana.postulacion_eliminada
    (
      id,
      talento_humano_id,
      condicion_nominal_id,
      periodo_evaluacion,
      cargo_evaluacion_id,
      dependencia_evaluacion_id,
      dependencia_evaluacion,
      supervisor_inmediato,
      rango_actuacion_id,
      puntuacion_obtenida,
      apertura_periodo_id,
      estado_id,
      dependencia_postulado_id,
      dependencia_general_postulado,
      clase_cargo_id,
      cargo_postulado_id,
      estatus,
      usuario_ini_id,
      fecha_ini,
      usuario_act_id,
      fecha_act,
      fecha_eli,
      puntuacion_desempeno,
      estatus_aprobado,
      codigo_constancia,
      documentos_consignado,
      observacion,
      puntuacion_prueba,
      horas_totales_cursos,
      autoridad_vigente_deingreso_id,
      estatus_anterior_aprobado,
      autoridad_vigente_degestionhumana_id,
      autoridad_vigente_derrhh_id,
      origen_validador,
      cedula_validador,
      nombre_validador,
      motivo_eliminacion_id,
      motivo_descripcion_eliminacion,
      usuario_id_eliminacion
    )
      (SELECT
         id,
         talento_humano_id,
         condicion_nominal_id,
         periodo_evaluacion,
         cargo_evaluacion_id,
         dependencia_evaluacion_id,
         dependencia_evaluacion,
         supervisor_inmediato,
         rango_actuacion_id,
         puntuacion_obtenida,
         apertura_periodo_id,
         estado_id,
         dependencia_postulado_id,
         dependencia_general_postulado,
         clase_cargo_id,
         cargo_postulado_id,
         estatus,
         usuario_ini_id,
         fecha_ini,
         usuario_act_id,
         fecha_act,
         fecha_eli,
         puntuacion_desempeno,
         estatus_aprobado,
         codigo_constancia,
         documentos_consignado,
         observacion,
         puntuacion_prueba,
         horas_totales_cursos,
         autoridad_vigente_deingreso_id,
         estatus_anterior_aprobado,
         autoridad_vigente_degestionhumana_id,
         autoridad_vigente_derrhh_id,
         origen_validador,
         cedula_validador,
         nombre_validador,
         motivo_eliminacion_id_vi,
         observacion_cancelar_postulacion_vi,
         usuario_ini_id_vi
       FROM gestion_humana.postulacion WHERE talento_humano_id = talento_humano_id_vi AND apertura_periodo_id = captura_apertura_periodo_id_v AND id = postulacion_id_vi);
    seccion_v := 7;
    DELETE FROM baremo.entrevista_ponderacion WHERE talento_humano_id = talento_humano_id_vi  AND postulacion_id = postulacion_id_vi;
    DELETE FROM baremo.entrevista_postulacion WHERE talento_humano_id = talento_humano_id_vi  AND postulacion_id = postulacion_id_vi;
    DELETE FROM gestion_humana.postulacion WHERE talento_humano_id = talento_humano_id_vi AND apertura_periodo_id = captura_apertura_periodo_id_v AND id = postulacion_id_vi;

    SELECT
    '{id:'||COALESCE(id::VARCHAR, '')||', talento_humano_id:'||COALESCE(talento_humano_id::VARCHAR, '')||', condicion_nominal_id:'||COALESCE(condicion_nominal_id::VARCHAR, '')||', periodo_evaluacion:"'||COALESCE(periodo_evaluacion::VARCHAR, '')||'", cargo_evaluacion_id:'||COALESCE(cargo_evaluacion_id::VARCHAR, '')||', dependencia_evaluacion_id:'||COALESCE(dependencia_evaluacion_id::VARCHAR, '')||', dependencia_evaluacion:"'||COALESCE(dependencia_evaluacion::VARCHAR, '')||'", supervisor_inmediato:"'||COALESCE(supervisor_inmediato::VARCHAR, '')||'", rango_actuacion_id:'||COALESCE(rango_actuacion_id::VARCHAR, '')||', puntuacion_obtenida:'||COALESCE(puntuacion_obtenida::VARCHAR, '')||', apertura_periodo_id:'||COALESCE(apertura_periodo_id::VARCHAR, '')||', estado_id:'||COALESCE(estado_id::VARCHAR, '')||', dependencia_postulado_id:'||COALESCE(dependencia_postulado_id::VARCHAR, '')||', dependencia_general_postulado:"'||COALESCE(dependencia_general_postulado::VARCHAR, '')||'", clase_cargo_id:'||COALESCE(clase_cargo_id::VARCHAR, '')||', cargo_postulado_id:'||COALESCE(cargo_postulado_id::VARCHAR, '')||', estatus:"'||COALESCE(estatus::VARCHAR, '')||'", usuario_ini_id:'||COALESCE(usuario_ini_id::VARCHAR, '')||', fecha_ini:"'||COALESCE(fecha_ini::VARCHAR, '')||'", usuario_act_id:'||COALESCE(usuario_act_id::VARCHAR, '')||', fecha_act:"'||COALESCE(fecha_act::VARCHAR, '')||'", fecha_eli:"'||COALESCE(fecha_eli::VARCHAR, '')||'", puntuacion_desempeno:'||COALESCE(puntuacion_desempeno::VARCHAR, '')||', estatus_aprobado:'||COALESCE(estatus_aprobado::VARCHAR, '')||', codigo_constancia:"'||COALESCE(codigo_constancia::VARCHAR, '')||'", documentos_consignado:"'||COALESCE(documentos_consignado::VARCHAR, '')||'", observacion:"'||COALESCE(observacion::VARCHAR, '')||'", puntuacion_prueba:'||COALESCE(puntuacion_prueba::VARCHAR, '')||', horas_totales_cursos:'||COALESCE(horas_totales_cursos::VARCHAR, '')||', autoridad_vigente_deingreso_id:'||COALESCE(autoridad_vigente_deingreso_id::VARCHAR, '')||', estatus_anterior_aprobado:'||COALESCE(estatus_anterior_aprobado::VARCHAR, '')||', autoridad_vigente_degestionhumana_id:'||COALESCE(autoridad_vigente_degestionhumana_id::VARCHAR, '')||', autoridad_vigente_derrhh_id:'||COALESCE(autoridad_vigente_derrhh_id::VARCHAR, '')||', origen_validador:"'||COALESCE(origen_validador::VARCHAR, '')||'", cedula_validador:'||COALESCE(cedula_validador::VARCHAR, '')||', nombre_validador:"'||COALESCE(nombre_validador::VARCHAR, '')||'", motivo_eliminacion_id:'||COALESCE(motivo_eliminacion_id::VARCHAR, '')||', motivo_descripcion_eliminacion:"'||COALESCE(motivo_descripcion_eliminacion::VARCHAR, '')||'", usuario_id_eliminacion:'||COALESCE(usuario_id_eliminacion::VARCHAR, '')||', fecha_eliminacion:"'||COALESCE(fecha_eliminacion::VARCHAR, '')||'" }'
    INTO data_v
    FROM gestion_humana.postulacion_eliminada WHERE talento_humano_id = talento_humano_id_vi AND apertura_periodo_id = captura_apertura_periodo_id_v AND id = postulacion_id_vi;

    mensaje_v := 'Se ha eliminado la Postulacion satisfactoriamente.';
    resultado_transaccion_v := 'success';
    seccion_v := 8;
    SELECT
      mensaje_v,
      seccion_v,
      resultado_transaccion_v
    --Campos
    INTO
      resultado;

    seccion_v := 9;
    --  AUDITORIA
    INSERT INTO
      auditoria.traza(
        fecha_hora,
        ip_maquina,
        tipo_transaccion,
        modulo,
        resultado_transaccion,
        descripcion,
        user_id,
        username,
        data
      ) VALUES (
      fecha_hora_v,
      ipaddress_vi,
      tipo_transaccion_v,
      modulo_v,
      resultado_transaccion_v,
      mensaje_auditoria_v,
      usuario_ini_id_vi,
      username_v,
      data_v
    );
    RETURN resultado;

    EXCEPTION

    WHEN OTHERS THEN
      transaccion_v := SQLSTATE;
      IF resultado_transaccion_v is null OR length(resultado_transaccion_v) = 0 THEN
        resultado_transaccion_v := 'error';
      END IF;
      mensaje_auditoria_v := 'ACTIBACION DE POSTULACION PARA ENTREVISTA: '||SQLERRM||' (ERROR NRO: '||SQLSTATE||') (Seccion: '||seccion_v||') (Resultado: '||transaccion_v||')';
      mensaje_v := SQLERRM;
      SELECT
      '{ tipo_periodo_id:'||COALESCE(tipo_periodo_id_vi::VARCHAR, '')||', postulacion_id:'||COALESCE(postulacion_id_vi::VARCHAR, '')||', talento_humano_id:'||COALESCE(talento_humano_id_vi::VARCHAR, '')||', usuario_ini_id:'||COALESCE(usuario_ini_id_vi::VARCHAR, '')||', motivo_eliminacion_id:'||COALESCE(motivo_eliminacion_id_vi::VARCHAR, '')||', observacion_cancelar_postulacion:"'||COALESCE(observacion_cancelar_postulacion_vi::VARCHAR, '')||'", ipaddress:'||COALESCE(ipaddress_vi::VARCHAR, '')||' }'
      INTO data_v;
      --  AUDITORIA
      INSERT INTO
        auditoria.traza(
          fecha_hora,
          ip_maquina,
          tipo_transaccion,
          modulo,
          resultado_transaccion,
          descripcion,
          user_id,
          username,
          data
        ) VALUES (
        fecha_hora_v,
        ipaddress_vi,
        tipo_transaccion_v,
        modulo_v,
        resultado_transaccion_v,
        mensaje_auditoria_v,
        usuario_ini_id_vi,
        username_v,
        data_v
      );

      SELECT
        mensaje_v,
        seccion_v,
        resultado_transaccion_v
      --Campos
      INTO
        resultado;

      RETURN resultado;

  END;$BODY$
LANGUAGE plpgsql VOLATILE
COST 100;