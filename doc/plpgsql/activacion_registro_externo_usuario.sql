CREATE OR REPLACE FUNCTION seguridad.activacion_registro_externo_usuario(
    activation_code_vi CHARACTER VARYING,
    username_vi CHARACTER VARYING,
    fecha_actual_vi DATE,
    modulo_vi CHARACTER VARYING,
    ipaddress_vi CHARACTER VARYING
) RETURNS record AS
$BODY$
DECLARE

--      Variables:

--      Debug:
        seccion_v INT := 1; --Seccion del error

--      Variables de Existencia:
        existencia_usuario SMALLINT := 0;

--      Datos del Usuario Necesarios para validar la activación de su cuenta
        codigo_activacion_v CHARACTER VARYING(50);
        fecha_activacion_v DATE;
        usuario_id_v BIGINT;
        status_usuario_v INTEGER;

        fecha_v TIMESTAMP WITHOUT TIME ZONE;

--      Variables de Mensaje:
        mensaje_v CHARACTER VARYING(300) := '';
        codigo_v CHARACTER VARYING(10) := '';
        estado_resultado_v CHARACTER VARYING(10) := '';

        data_v TEXT := '{}';

--      Variables de Resultado:
        resultado record;

BEGIN

    fecha_v := (NOW())::TIMESTAMP(0);

    seccion_v := 2;

--  Validar si ya esta registrado el usuario
    SELECT count(1) INTO existencia_usuario FROM seguridad.usergroups_user WHERE username = username_vi;

    IF existencia_usuario = 0 THEN

        mensaje_v := 'El código indicado no corresponde con ningún usuario en nuestra base de datos.';
        codigo_v := 'EDB001';
        estado_resultado_v :='error';

    ELSE

        seccion_v := 3;

        SELECT id, activation_code, activation_time::DATE, status INTO usuario_id_v, codigo_activacion_v, fecha_activacion_v, status_usuario_v FROM seguridad.usergroups_user WHERE username = username_vi;

        IF status_usuario_v = 4 THEN

            mensaje_v := 'El Usuario indicado ya se encuentra activo. Puede dirigirse al login e ingresar al sistema.';
            codigo_v := 'ADB002';
            estado_resultado_v :='alerta';

        ELSIF status_usuario_v NOT IN (1, 2, 3) THEN

            mensaje_v := 'El Código de Activación no corresponde con un usuario activo en el sistema.';
            codigo_v := 'EDB003';
            estado_resultado_v :='error';

        ELSIF status_usuario_v = 3 THEN

            mensaje_v := 'Este usuario ya ha solicitado un cambio de clave. Debe esperar el correo que le indique los pasos a seguir para el restablecimiento de la misma.';
            codigo_v := 'ADB004';
            estado_resultado_v :='alerta';

        ELSE

            IF codigo_activacion_v != activation_code_vi THEN

                mensaje_v := 'El Código de Activación es Inválido.';
                codigo_v := 'EDB005';
                estado_resultado_v := 'error';

            ELSIF NOT ((fecha_actual_vi::DATE - fecha_activacion_v::DATE) BETWEEN 0 AND 7) THEN

                mensaje_v := 'El Período de Activación ha culminado puede solicitar el re-envío del correo para la activación de sus cuenta haciendo click <a onClick="{{url}}/cod/{{username}}">aquí</a>.';
                codigo_v := 'EDB006';
                estado_resultado_v := 'error';

            ELSE

                seccion_v := 4;

                UPDATE seguridad.usergroups_user SET status = 4, date_act = fecha_v, user_act_id = usuario_id_v WHERE id = usuario_id_v;

                mensaje_v := 'Su cuenta ha sido activada exitosamente, puede dirigirse al login y acceder con su usuario y password.';
                codigo_v := 'S00000';
                estado_resultado_v :='exito';

            END IF;

        END IF;

    END IF;

    data_v := '{ usuario: { id: '||usuario_id_v::TEXT||', username: '||username_vi::TEXT||', status_antes_del_proceso: '||status_usuario_v::TEXT||', activation_code: '||codigo_activacion_v::TEXT||', activation_time: '||fecha_activacion_v::TEXT||', activation_code_recibido: '||activation_code_vi::TEXT||', activation_time_recibido: '||fecha_actual_vi::TEXT||'} }';

    SELECT codigo_v, estado_resultado_v, mensaje_v, usuario_id_v INTO resultado;
--  Ingresa los resultados a la traza de la auditoria.
    INSERT INTO auditoria.traza(fecha_hora, ip_maquina, tipo_transaccion, modulo, resultado_transaccion, descripcion, user_id, username, data) VALUES (fecha_v, ipaddress_vi, 'ESCRITURA', modulo_vi, estado_resultado_v, 'ACTIVACIÓN DE CUENTA DE USUARIO: '||mensaje_v, usuario_id_v, username_vi, data_v);

    RETURN resultado;

EXCEPTION

    WHEN OTHERS THEN
        codigo_v := SQLSTATE||'';
        estado_resultado_v := 'error';
        mensaje_v := 'Ha ocurrido un error '||SQLERRM||' (ERROR NRO: '||SQLSTATE||') (Seccion: '||seccion_v||') (Resultado: '||estado_resultado_v;
        RAISE NOTICE 'Ha ocurrido un error % (ERROR NRO: %) (Seccion: %)', SQLERRM, SQLSTATE, seccion_v;
        SELECT codigo_v, estado_resultado_v, mensaje_v, null INTO resultado;
--      Ingresa los resultados a la traza de la auditoria.
        INSERT INTO auditoria.traza(fecha_hora, ip_maquina, tipo_transaccion, modulo, resultado_transaccion, descripcion, user_id, username, data) VALUES (fecha_v, ipaddress_vi, 'ESCRITURA', modulo_vi, estado_resultado_v, 'ACTIVACIÓN DE CUENTA DE USUARIO: '||mensaje_v, usuario_id_v, username_vi, data_v);

    RETURN resultado;

END;$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;