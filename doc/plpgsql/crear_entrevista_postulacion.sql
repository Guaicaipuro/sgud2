-- Function: baremo.crear_entrevista_postulacion(bigint, bigint, bigint, text, smallint, text)

-- DROP FUNCTION baremo.crear_entrevista_postulacion(bigint, bigint, bigint, text, smallint, text);

CREATE OR REPLACE FUNCTION baremo.crear_entrevista_postulacion(
    postulacion_id_vi bigint,
    usuario_ini_id_vi bigint,
    usuario_act_id_vi bigint,
    respuestas_vi text,
    estatus_postulacion_vi smallint,
    observacion_vi text)
  RETURNS record AS
$BODY$
DECLARE

--  Variables:

--      Constantes
        const_postulacion_aprobada_v SMALLINT := 1;
        const_postulacion_reprobada_v SMALLINT := 5;

--      Variables de existencia
        existencia_postulacion_v INT := 0;
        existencia_postulacion_aprobada_v INT := 0;
        existencia_entrevista_postulacion INT := 0;

--      Variables de Captura:
        captura_entrevista_id_v BIGINT := null;
--          Tabla postulacion
        captura_talento_humano_id_v BIGINT = null;
        captura_apertura_periodo_id_v INT = null;
        captura_cargo_postulado_id_v BIGINT = null;
        captura_dependencia_postulado_id_v BIGINT = null;
--          Tabla Tipo Periodo
        captura_categoria_apertura_periodo_id_v SMALLINT = null;
--          Tabla Talento Humano
        captura_origen_v VARCHAR := null;
        captura_cedula_v INT := null;
        captura_nombre_apellido_v VARCHAR := null;
        captura_fecha_nacimiento_v DATE := null;
        captura_estado_id_v INT := null;
        captura_categoria_cargo_id_v INT := null;

--          Tabla User Analista
        captura_origen_analista_v VARCHAR := null;
        captura_cedula_analista_v INT := null;
        captura_nombre_apellido_analista_v VARCHAR := null;

--          Tabla cargo vacante
        captura_codigo_dependencia INT := null;
        captura_nombre_dependencia VARCHAR := null;
        captura_codigo_cargo_v INT := null;
        captura_nombre_cargo_v VARCHAR := null;
--          Condicion para concurso o merito
        captura_estatus_aprobado_v INT := null;
        captura_user_id_v BIGINT := null;

--      Informacion del sistema:
        modulo_v VARCHAR := 'gestionHumana.Postulacion.aprobar';
        fecha_actual_v DATE := NOW();
        ipaddress_v VARCHAR := inet_client_addr(); --IP que captura postgresql
        mensaje_auditoria_v VARCHAR;
        data_v TEXT;

--      Debug:
        seccion_v SMALLINT := null; --Seccion del error

--      Variables de Resultado:
        codigo_v VARCHAR;
        transaccion_v VARCHAR;
        mensaje_v VARCHAR;
        resultado record;

BEGIN
    seccion_v := 0;
    IF estatus_postulacion_vi = const_postulacion_reprobada_v THEN -- cuando el estatus de la postulacion es 2 es que no fue aprobada
        IF
            observacion_vi IS NULL OR char_length(trim(observacion_vi)) = 0
        THEN
            RAISE EXCEPTION 'Debe ingresar la razon por la cual la postulacion no fue realizada.';
            codigo_v := 'ES000';
        END IF;
    END IF;

    seccion_v := 1;
    SELECT COUNT(1) INTO existencia_postulacion_v FROM gestion_humana.postulacion WHERE id = postulacion_id_vi limit 1;
    IF existencia_postulacion_v = 0 THEN
        RAISE EXCEPTION 'Los datos suministrados no son validos.';
        codigo_v := 'EDB00';
    END IF;

    seccion_v := 2;

--  Por ser claves Foraneas no es necesario realizar una consulta COUNT de los siguientes selects, ya al poseer las otras tablas dependientes la data, significa que dichos valores existen.

--  Capturando los datos de la tabla de postulacion.
    SELECT talento_humano_id, apertura_periodo_id, cargo_postulado_id, dependencia_postulado_id, usuario_ini_id INTO captura_talento_humano_id_v, captura_apertura_periodo_id_v, captura_cargo_postulado_id_v, captura_dependencia_postulado_id_v, captura_user_id_v FROM gestion_humana.postulacion WHERE id = postulacion_id_vi limit 1;
    seccion_v := 3;
    SELECT user_id INTO captura_user_id_v FROM gestion_humana.talento_humano WHERE id = captura_talento_humano_id_v and estatus='A';
    IF usuario_ini_id_vi = captura_user_id_v THEN
      RAISE EXCEPTION 'Usted no esta Autorizado para Autoevaluarse.';
    END IF;

--  Capturando los datos de la tabla apertura_periodo
    SELECT tipo_apertura_id INTO captura_categoria_apertura_periodo_id_v FROM gestion_humana.apertura_periodo WHERE id = captura_apertura_periodo_id_v;
    seccion_v := 4;

--  Capturando los datos del analista validador
    SELECT origen, cedula, nombre||' '||apellido as nombre_apellido INTO captura_origen_analista_v, captura_cedula_analista_v, captura_nombre_apellido_analista_v FROM seguridad.usergroups_user WHERE id = usuario_act_id_vi;
    seccion_v := 4;

--  Capturando los datos de la tabla talento_humano
    SELECT origen, cedula, nombre||' '||apellido as nombre_apellido, fecha_nacimiento, estado_id, categoria_cargo_actual_id INTO captura_origen_v, captura_cedula_v, captura_nombre_apellido_v, captura_fecha_nacimiento_v, captura_estado_id_v, captura_categoria_cargo_id_v FROM gestion_humana.talento_humano WHERE id = captura_talento_humano_id_v;
    seccion_v := 5;
--  Captura los datos de la tabla cargo vacante
    SELECT coddenpen, nomdenpen, codnomin, descripcion INTO captura_codigo_dependencia, captura_nombre_dependencia, captura_codigo_cargo_v, captura_nombre_cargo_v FROM gestion_humana.cargo_vacante WHERE id = captura_cargo_postulado_id_v;
    seccion_v := 6;


--  Validar si no hay ya una entrevista de postulacion ya creada.

--  Cambiar la condicion "WHERE talento_humano_id = captura_talento_humano_id_v" a WHERE postulacion_id = postulacion_id_vi; ;
    SELECT COUNT(1) INTO existencia_entrevista_postulacion FROM baremo.entrevista_postulacion WHERE postulacion_id = postulacion_id_vi;
    IF existencia_entrevista_postulacion = 1 AND estatus_postulacion_vi = const_postulacion_aprobada_v THEN
    seccion_v := 7;

        UPDATE baremo.entrevista_postulacion  SET
            tipo_periodo_id = captura_categoria_apertura_periodo_id_v,
            postulacion_id = postulacion_id_vi,
            talento_humano_id = captura_talento_humano_id_v,
            categoria_cargo_id = captura_categoria_cargo_id_v,
            estado_id = captura_estado_id_v,
            origen_talento_humano = captura_origen_v,
            cedula_talento_humano = captura_cedula_v,
            nombre_apellido_talento_humano = captura_nombre_apellido_v,
            fecha_nacimiento = captura_fecha_nacimiento_v,
            cargo_postulado = captura_nombre_cargo_v,
            codigo_cargo_postulado = captura_codigo_cargo_v,
            dependencia_talento_humano = captura_nombre_dependencia,
            codigo_dependencia_talento_humano = captura_codigo_dependencia,
            usuario_ini_id = usuario_ini_id_vi,
            usuario_act_id = usuario_act_id_vi
        WHERE
            talento_humano_id = captura_talento_humano_id_v
        RETURNING id INTO captura_entrevista_id_v;
    --Cambiar los valores numericos a Constantes.
    ELSEIF estatus_postulacion_vi = const_postulacion_aprobada_v THEN
    seccion_v := 8;
        INSERT INTO baremo.entrevista_postulacion (
            tipo_periodo_id,
            postulacion_id,
            talento_humano_id,
            categoria_cargo_id,
            estado_id,
            origen_talento_humano,
            cedula_talento_humano,
            nombre_apellido_talento_humano,
            fecha_nacimiento,
            cargo_postulado,
            codigo_cargo_postulado,
            dependencia_talento_humano,
            codigo_dependencia_talento_humano,
            usuario_ini_id,
            usuario_act_id
        ) VALUES (
            captura_categoria_apertura_periodo_id_v,
            postulacion_id_vi,
            captura_talento_humano_id_v,
            captura_categoria_cargo_id_v,
            captura_estado_id_v,
            captura_origen_v,
            captura_cedula_v,
            captura_nombre_apellido_v,
            captura_fecha_nacimiento_v,
            captura_nombre_cargo_v,
            captura_codigo_cargo_v,
            captura_nombre_dependencia,
            captura_codigo_dependencia,
            usuario_ini_id_vi,
            usuario_act_id_vi
        ) RETURNING id INTO captura_entrevista_id_v;
    END IF;

    --  Validar si la postulacion ya se aprobo.
SELECT COUNT(1) INTO existencia_postulacion_aprobada_v FROM gestion_humana.postulacion WHERE id = postulacion_id_vi AND estatus_aprobado != 0 limit 1;

--  Validar el estado y la existencia de la entrevista en la base de datos.
--      + estatus_postulacion_vi
--          0 -> En Espera
--          1 -> Aprobado
--
IF existencia_postulacion_aprobada_v = 1 THEN
    RAISE EXCEPTION 'La consignación de documentos para esta postulación ya fue realizada.';
    codigo_v := 'EDB01';
ELSEIF captura_entrevista_id_v is null AND estatus_postulacion_vi = const_postulacion_aprobada_v THEN
    RAISE EXCEPTION 'No se pudo realizar la postulacion, Contacto con el administrador.';
    codigo_v := 'EDB02';
ELSE
    seccion_v := 9;
    UPDATE gestion_humana.postulacion SET
        documentos_consignado = respuestas_vi,
        estatus_aprobado = estatus_postulacion_vi,
        observacion = observacion_vi,
        origen_validador = captura_origen_analista_v,
        cedula_validador = captura_cedula_analista_v,
        nombre_validador = captura_nombre_apellido_analista_v
    WHERE id = postulacion_id_vi AND estatus_aprobado = 0;
    codigo_v := 'S0000';
    transaccion_v := 'success';
    mensaje_v := '<p>Se ha registrado la postulacion de forma exitosa.</p>';
END IF;



    SELECT codigo_v, transaccion_v, mensaje_v, captura_entrevista_id_v, captura_origen_v, captura_cedula_v, NULL::SMALLINT INTO resultado;
    data_v := '{origen:"'||captura_origen_v||'", cedula:'||captura_cedula_v||', postulacionId:'||postulacion_id_vi||', estatus:"'||estatus_postulacion_vi||'"}';
--  Ingresa los resultados a la traza de la auditoria.
    INSERT INTO auditoria.traza(
        fecha_hora,
        ip_maquina,
        tipo_transaccion,
        modulo,
        resultado_transaccion,
        descripcion,
        user_id,
        username,
        data
    ) VALUES (
        fecha_actual_v,
        ipaddress_v,
        'ESCRITURA',
        modulo_v,
        transaccion_v,
        'ACTIVACION DE POSTULACION PARA ENTREVISTA: '||mensaje_auditoria_v, usuario_ini_id_vi,
        'admin',
         data_v
    );

    RETURN resultado;

EXCEPTION

    WHEN OTHERS THEN
        codigo_v := SQLSTATE||'';
        transaccion_v := 'error';
        mensaje_v := SQLERRM;
        mensaje_auditoria_v := SQLERRM||' (ERROR NRO: '||SQLSTATE||') (Seccion: '||seccion_v||') (Resultado: '||transaccion_v;
        RAISE NOTICE 'Ha ocurrido un error % (ERROR NRO: %) (Seccion: %)', SQLERRM, SQLSTATE, seccion_v;

        SELECT codigo_v, transaccion_v, mensaje_v, null::bigint, null::varchar, null::int, seccion_v INTO resultado;
--  Ingresa los resultados a la traza de la auditoria.
        INSERT INTO auditoria.traza(fecha_hora, ip_maquina, tipo_transaccion, modulo, resultado_transaccion, descripcion, user_id, username, data) VALUES (fecha_actual_V, ipaddress_v, 'ESCRITURA', modulo_v, transaccion_v, 'ACTIBACION DE POSTULACION PARA ENTREVISTA: '||mensaje_auditoria_v, usuario_ini_id_vi, 'admin', '');

    RETURN resultado;

END;$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;
ALTER FUNCTION baremo.crear_entrevista_postulacion(bigint, bigint, bigint, text, smallint, text)
  OWNER TO postgres;