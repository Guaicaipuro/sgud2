-- Function: gestion_humana.insertar_postulacion_talento_humano(integer, integer, character varying, integer, integer, character varying, character varying, integer, integer, integer, integer, integer, character varying, integer, integer, integer, timestamp without time zone, integer, timestamp without time zone, text, text)

-- DROP FUNCTION gestion_humana.insertar_postulacion_talento_humano(integer, integer, character varying, integer, integer, character varying, character varying, integer, integer, integer, integer, integer, character varying, integer, integer, integer, timestamp without time zone, integer, timestamp without time zone, text, text);

CREATE OR REPLACE FUNCTION gestion_humana.insertar_postulacion_talento_humano(talento_humano_id_vi integer, condicion_nominal_id_vi integer, periodo_evaluacion_vi character varying, cargo_evaluacion_id_vi integer, dependencia_evaluacion_id_vi integer, dependencia_evaluacion_vi character varying, supervisor_inmediato_vi character varying, rango_actuacion_id_vi integer, puntuacion_obtenida_vi integer, apertura_periodo_id_vi integer, estado_id_vi integer, dependencia_postulado_id_vi integer, dependencia_general_postulado_vi character varying, clase_cargo_id_vi integer, cargo_postulado_id_vi integer, usuario_ini_id_vi integer, fecha_ini_vi timestamp without time zone, usuario_act_id_vi integer, fecha_act_vi timestamp without time zone, data_vi text, tipo_periodo_vi text)
  RETURNS record AS
$BODY$
DECLARE

--  Informacion del sistema:
    modulo_v VARCHAR := 'modulo.gestionHumana.Postulacion'; --log auditoria.traza
    ipaddress_v VARCHAR := inet_client_addr(); --IP que captura postgresql
    tipo_transaccion_v VARCHAR := 'ESCRITURA';
    descripcion_v VARCHAR := 'Esta funcion tiene por objeto validar la existencia de una postulacion antes de insertar una nueva';
    user_id_v BIGINT := usuario_ini_id_vi;
    username_vi VARCHAR := null;

--  Variables Constantes
    const_concurso_publico_id_v SMALLINT := 1;
    const_sistema_merito_id_v SMALLINT := 2;

--  Variables de existencia
    existencia_talento_humano_v INT := 0;
    existencia_postulacion_v INT := 0;
    existencia_experiencia_laboral_v INT := 0;
    existencia_referencia_personal_v INT := 0;
    existencia_estudios_realizados_v INT := 0;

--  Captura de cantidad
    count_cargo_vacante_v INT := 0;
    count_cargo_vacante_total_v INT := 0;

--  Variables de captura
    captura_postulacion_id_v INT := null;
    captura_cantidad_maxima_condicion_v VARCHAR := null;
    captura_tipo_periodo_id_v SMALLINT := null;
    captura_periodo_id_v INT := null;
    captura_minimo_referencias_v INT := 0;

--  Variables de exception
    seccion_v SMALLINT := 0;

--  Variables de resultado
    estado_resultado_v varchar := 'error';
    mensaje_v varchar := 'Ha ocurrido un error en el sistema.';
    codigo_v varchar := 'E00000';
    resultado_v record;
    BEGIN

    IF (apertura_periodo_id_vi=const_concurso_publico_id_v)THEN
        captura_cantidad_maxima_condicion_v := 'CANTIDAD_MAXIMA_DE_POSTULADOS_A_CP';
        captura_minimo_referencias_v := 2;
    ELSEIF(apertura_periodo_id_vi=const_sistema_merito_id_v) THEN
        captura_cantidad_maxima_condicion_v := 'CANTIDAD_MAXIMA_DE_POSTULADOS_A_SM';
    END IF;


    SELECT COUNT(1) INTO count_cargo_vacante_v FROM gestion_humana.postulacion WHERE cargo_postulado_id = cargo_postulado_id_vi AND apertura_periodo_id = apertura_periodo_id_vi AND estatus = 'A';

    SELECT valor_num INTO count_cargo_vacante_total_v FROM sistema.configuracion WHERE nombre = captura_cantidad_maxima_condicion_v;
    IF count_cargo_vacante_v >= count_cargo_vacante_total_v THEN
        seccion_v = 0;
        estado_resultado_v := 'error';
        mensaje_v := 'El Cargo que Usted Selecciono ya Supero el Número de Postulaciones Disponible, Seleccione Otro Cargo.';
        codigo_v := 'ACONF0';
    ELSE 
        SELECT COUNT(1) INTO existencia_talento_humano_v FROM gestion_humana.talento_humano WHERE id = talento_humano_id_vi AND estatus = 'A';
        IF existencia_talento_humano_v = 0 THEN
            seccion_v = 1;
            estado_resultado_v := 'error';
            mensaje_v := 'Usted no Puede Realizar la Postulacion al Sistema de Mérito.';
            codigo_v := 'ADB000';
        ELSE
            IF tipo_periodo_vi = 'merito' THEN 
                tipo_periodo_vi := 'Sistema de Merito';
                SELECT COUNT(1) INTO existencia_experiencia_laboral_v FROM gestion_humana.experiencia_laboral WHERE talento_humano_id = talento_humano_id_vi;
            ELSEIF tipo_periodo_vi = 'concurso' THEN
                tipo_periodo_vi := 'Concurso Publico';
                existencia_experiencia_laboral_v := 1;
            END IF;

            SELECT COUNT(1) INTO existencia_estudios_realizados_v FROM gestion_humana.estudio_realizado WHERE talento_humano_id = talento_humano_id_vi;
            IF existencia_estudios_realizados_v = 0 THEN
                seccion_v = 1;
                estado_resultado_v := 'error';
                mensaje_v := 'Debe registrar por lo menos un (1) Estudio Realizado para poder realizar la Postulación en el '||tipo_periodo_vi||'.';
                codigo_v := 'ADB000';
            ELSE
                SELECT COUNT(1) INTO existencia_postulacion_v FROM gestion_humana.postulacion WHERE talento_humano_id = talento_humano_id_vi AND apertura_periodo_id = apertura_periodo_id_vi AND estatus = 'A';
                seccion_v = 2;
                IF existencia_postulacion_v = 0 THEN
                    seccion_v = 3;
                    IF existencia_experiencia_laboral_v = 0 THEN 
                        seccion_v = 4;
                        estado_resultado_v := 'error';
                        mensaje_v := 'Debe registrar por lo menos una (1) Experiencia Laboral para poder realizar la Postulación en el '||tipo_periodo_vi||'.';
                        codigo_v := 'ADB000';
                    ELSE
                        SELECT COUNT(1) INTO existencia_referencia_personal_v FROM gestion_humana.referencia_personal WHERE talento_humano_id = talento_humano_id_vi AND estatus = 'A';
                        IF existencia_referencia_personal_v < captura_minimo_referencias_v THEN
                            seccion_v = 5;
                            estado_resultado_v := 'error';
                            mensaje_v := 'Debe registrar al menos dos (1) Referencias Personales para poder realizar la Postulación en el '||tipo_periodo_vi||'.';
                            codigo_v := 'ADB001';
                        ELSE
                            seccion_v = 6;
                            INSERT INTO gestion_humana.postulacion (
                                talento_humano_id,
                                condicion_nominal_id,
                                periodo_evaluacion,
                                cargo_evaluacion_id,
                                dependencia_evaluacion_id,
                                dependencia_evaluacion,
                                supervisor_inmediato,
                                rango_actuacion_id,
                                apertura_periodo_id,
                                estado_id,
                                dependencia_postulado_id,
                                dependencia_general_postulado,
                                clase_cargo_id,
                                cargo_postulado_id,
                                usuario_ini_id,
                                fecha_ini,
                                usuario_act_id,
                                fecha_act, 
                                puntuacion_desempeno
                            ) VALUES (
                                talento_humano_id_vi,
                                condicion_nominal_id_vi,
                                periodo_evaluacion_vi,
                                cargo_evaluacion_id_vi,
                                dependencia_evaluacion_id_vi,
                                dependencia_evaluacion_vi,
                                supervisor_inmediato_vi,
                                rango_actuacion_id_vi,                                
                                apertura_periodo_id_vi,
                                estado_id_vi,
                                dependencia_postulado_id_vi,
                                dependencia_general_postulado_vi,
                                clase_cargo_id_vi,
                                cargo_postulado_id_vi,
                                usuario_ini_id_vi,
                                fecha_ini_vi,
                                usuario_act_id_vi,
                                fecha_act_vi, 
                                puntuacion_obtenida_vi
                            ) RETURNING id INTO captura_postulacion_id_v;
                            estado_resultado_v := 'success';
                            mensaje_v := 'Se ha registrado la postulacion de forma exitosa, Sí desea ingresar al modulo de la entrevista puede hacerlo haciendo click en el boton "Entrevistar".';
                            codigo_v := 'SDB000';
                        END IF;
                    END IF;
                ELSE
                    seccion_v = 7;
                    estado_resultado_v := 'error';
                    mensaje_v := 'Usted ya realizo una postulacion en este periodo del '||tipo_periodo_vi||'.';
                    codigo_v := 'ADB002';
                END IF;
            END IF;
        END IF;
    END IF;
    seccion_v = 8;
    INSERT INTO auditoria.traza(fecha_hora, ip_maquina, tipo_transaccion, modulo, resultado_transaccion, descripcion, user_id, username, data) VALUES (fecha_ini_vi, ipaddress_v, tipo_transaccion_v, modulo_v, estado_resultado_v, descripcion_v||mensaje_v, usuario_ini_id_vi, username_vi, data_vi);
    SELECT codigo_v::varchar, estado_resultado_v, mensaje_v, captura_postulacion_id_v, null::SMALLINT INTO resultado_v;
    RETURN resultado_v;

EXCEPTION
    WHEN OTHERS THEN
        codigo_v := SQLSTATE||'';
        estado_resultado_v := 'error';
        mensaje_v := 'Ha ocurrido un error '||SQLERRM||' (ERROR NRO: '||SQLSTATE||') (Seccion: '||seccion_v||') (Resultado: '||estado_resultado_v;
        RAISE NOTICE 'Ha ocurrido un error % (ERROR NRO: %) (Seccion: %)', SQLERRM, SQLSTATE, seccion_v;

        SELECT codigo_v::varchar, estado_resultado_v, mensaje_v, null::INT, seccion_v INTO resultado_v;
--  Ingresa los resultados a la traza de la auditoria.
        INSERT INTO auditoria.traza(fecha_hora, ip_maquina, tipo_transaccion, modulo, resultado_transaccion, descripcion, user_id, username, data) VALUES (fecha_ini_vi, ipaddress_v, tipo_transaccion_v, modulo_v, estado_resultado_v, descripcion_v||mensaje_v, usuario_ini_id_vi, username_vi, data_vi);

    RETURN resultado_v;

END;$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;

  /*
SELECT * FROM gestion_humana.insertar_postulacion_talento_humano(
                    157249::INTEGER, -- id del talento humano
                    1::INTEGER, --
                    '2015-1 : 01/01/2015 al 30/06/2015'::VARCHAR,
                    366::INTEGER,
                    83::INTEGER,
                    'DIV DE ARCHIVO DE PERSONAL'::VARCHAR,
                   'pedro flores'::VARCHAR,
                    4::INTEGER,
                    450::INTEGER,
                    2::INTEGER,
                   21::INTEGER,
                    99::INTEGER,
                   'DIV DE ARCHIVO DE PERSONAL'::VARCHAR,
                    5::INTEGER,
                    34431::INTEGER,
                   1::INTEGER,
                   '2015-09-27 05:37:56'::TIMESTAMP,
                    1::INTEGER,
                   '2015-09-27 05:37:56'::TIMESTAMP,
                    'hola'::TEXT,
                    'merito'::TEXT
                    ) AS f(
                        codigo_resultado VARCHAR,
                        estado_resultado VARCHAR,
                        mensaje VARCHAR,
                        postulacion_id INTEGER,
                        seccion SMALLINT
                    );

  */