-- APP_SEGURO_PRODUCCION (DATA CON TODO CORRECTO QUE NO SE TOCARA EN EL 115)

CREATE OR REPLACE FUNCTION mirror.bus_servicios_tpersona_jubilados()
  RETURNS trigger AS
$BODY$
DECLARE

--Variables 
categoria_ingreso_v INT;
nombre_v VARCHAR;
apellido_v VARCHAR;
verificado_saime_v VARCHAR;
tipo_nomina_id_v INT := 2;
fecha_actual_v TIMESTAMP WITH TIME ZONE := NOW();
condicion_v VARCHAR := 'Jubilado';
condicion_nominal_id_v INT := 2;
fecha_actual_contratado_v TIMESTAMP WITH TIME ZONE := fecha_actual_v;
usuario_id_v INT := 2;
condicion_actual_id_v INT := 3;
captura_istatus_v VARCHAR := 'A';
fecha_nacimiento_errada_v SMALLINT := 0;


--	Variables de EXCEPTION
descripcion_v TEXT := 'Bus de servicio con Nómina que permite la transferencia de los datos entre la tabla dbjubilados.tpersona de la base de datos de nomina y mirror.nm_jubilados_tpersona de app_seguros';
nombre_ktr_v VARCHAR := 'TRANS_MIRROR_JUBILADOS_TPERSONA';
nombre_kjb_v VARCHAR := 'a';
observaciones_v TEXT := null;
seccion_v TEXT := 'EXITO';	

--Variables Array
array_cobertura_v VARCHAR[];
array_tpersona_v VARCHAR[];

--Variables de estado
estado_cobertura_v VARCHAR := '';
tpersona_cobertura_v VARCHAR := 0;

--Variables Indice
indice_cobertura_v INT := 1;
indice_final_cobertura_v INT := 0;
indice_tpersona_v INT := 1;
indice_final_tpersona_v INT := 0;


--Variables de captura
captura_ccedula_v INT := null;
captura_tnacionalidad_v VARCHAR := null;
captura_primer_nombre_v VARCHAR := ''; 
captura_segundo_nombre_v VARCHAR := ''; 
captura_primer_apellido_v VARCHAR := ''; 
captura_segundo_apellido_v VARCHAR := '';
captura_centidad_v VARCHAR := null;
captura_id_estado_v INT := null;
captura_categoria_ingreso_id_v INT := 2;
captura_categoria_cargo_actual_v VARCHAR := null;
captura_categoria_cargo_actual_id_v INT := null;
captura_cargo_nominal_id_v INT := null;
captura_cuenta_banco_v VARCHAR := null;
captura_tipo_personal_v VARCHAR := null;
captura_cargo_v VARCHAR := null;
captura_dependencia_v VARCHAR := null;
captura_fecha_nacimiento_v DATE;
captura_fecha_nacimiento_saime_v DATE;
captura_sexo_saime_v VARCHAR := null;
captura_sexo_v VARCHAR := null;
captura_cobertura_id_v INT := null;
captura_tipo_pasivo_v VARCHAR := null;
captura_nombre_estado VARCHAR := null;

--Variables de existencia
existencia_cedula_v INT := 0;
existencia_cedula_saime_v INT := 0;
existencia_co_edo_asap_v INT := 0;
existencia_categoria_ingreso_v INT := 0;
existencia_ccargo_v INT := 0;
existencia_cargo_nominal_v INT := 0;
existencia_tipo_cuenta_id_v INT := 0;
existencia_tpersona_v INT := 0;
existencia_cdependencia_v INT := 0;
existencia_talento_humano_v INT := 0;
existencia_nm_jubilados_v INT := 0;
existencia_nm_jubilados_istatus_v INT := 0;


BEGIN

--Ejecutar cuando se realiza un INSERT o un UPDATE.
  IF (TG_OP = 'INSERT' OR TG_OP = 'UPDATE') THEN
--(----------------------------------------------------------------------------------------------------------------------------------------------------
--  Seteando variables que se usaran para buscar los datos cuando sea un INSERT
    IF (TG_OP = 'INSERT') THEN
    	captura_ccedula_v := NEW.ccedula;
    	captura_tnacionalidad_v := NEW.tnacionalidad;
--  Seteando variables que se usaran para buscar los datos cuando sea un UPDATE
    ELSEIF (TG_OP = 'UPDATE') THEN
    	captura_ccedula_v := OLD.ccedula;
    	captura_tnacionalidad_v := OLD.tnacionalidad;
    END IF;
--)----------------------------------------------------------------------------------------------------------------------------------------------------


--(----------------------------------------------------------------------------------------------------------------------------------------------------
	SELECT COUNT(1) INTO existencia_cedula_saime_v FROM mirror.saime WHERE cedula = captura_ccedula_v AND origen = captura_tnacionalidad_v;
	IF (existencia_cedula_saime_v = 1) THEN 

		SELECT 'Si' INTO verificado_saime_v;

		SELECT 
			primer_nombre, 
			segundo_nombre, 
			primer_apellido, 
			segundo_apellido,
			fecha_nacimiento,
			sexo
		INTO 
			captura_primer_nombre_v, 
			captura_segundo_nombre_v, 
			captura_primer_apellido_v, 
			captura_segundo_apellido_v,
			captura_fecha_nacimiento_saime_v,
			captura_sexo_saime_v

		FROM mirror.saime WHERE cedula = captura_ccedula_v AND origen = captura_tnacionalidad_v;
		SELECT 
			captura_primer_nombre_v||' '||captura_segundo_nombre_v, captura_primer_apellido_v||' '||captura_segundo_apellido_v 
		INTO 
			nombre_v, apellido_v;

		IF(NEW.fnacimiento is null) THEN
			RAISE NOTICE 'NEW.fnacimiento is null';
			IF (captura_fecha_nacimiento_saime_v = '0001-01-01') THEN
				RAISE NOTICE 'captura_fecha_nacimiento_saime_v = "0001-01-01"';
				SELECT '1800-01-01' INTO captura_fecha_nacimiento_v;
				SELECT 1 INTO fecha_nacimiento_errada_v;
			ELSE
				captura_fecha_nacimiento_v = captura_fecha_nacimiento_saime_v;
			END IF;
		ELSE
			captura_fecha_nacimiento_v = NEW.fnacimiento;
		END IF;
	ELSE
		SELECT 'No' INTO verificado_saime_v;
		SELECT NEW.dnombre, NEW.dnombre INTO nombre_v, apellido_v;

		IF(NEW.fnacimiento is null) THEN
			RAISE NOTICE 'NEW.fnacimiento is null';
			SELECT '1800-01-01' INTO captura_fecha_nacimiento_v;
			SELECT 1 INTO fecha_nacimiento_errada_v;
		ELSE
			captura_fecha_nacimiento_v = NEW.fnacimiento;
		END IF;
	END IF;


--	Remplazar ? por las ñ en el nombre
	SELECT 
		regexp_replace(nombre_v, '\?', 'ñ')
	INTO 
		nombre_v
	WHERE
		nombre_v ~* '[^a-zA-Z0-9áÁéÉíÍóÓúÚñÑ ]';

--	Remplazar ? por las ñ en el apellido
	SELECT 
		regexp_replace(apellido_v, '\?', 'ñ')  
	INTO 
		apellido_v
	WHERE
		apellido_v  ~* '[^a-zA-Z0-9áÁéÉíÍóÓúÚñÑ ]';

--)----------------------------------------------------------------------------------------------------------------------------------------------------
	SELECT COUNT(1) FROM mirror.nm_jubilados_tjubilados INTO existencia_nm_jubilados_v WHERE cedula = captura_ccedula_v AND nacionalidad = captura_tnacionalidad_v LIMIT 1 ;
	IF (existencia_nm_jubilados_v > 0) THEN
		SELECT COUNT(1) FROM mirror.nm_jubilados_tjubilados INTO existencia_nm_jubilados_istatus_v WHERE cedula = captura_ccedula_v AND nacionalidad = captura_tnacionalidad_v AND istatus = 'A' LIMIT 1 ;
		IF (existencia_nm_jubilados_istatus_v > 0) THEN
	--(----------------------------------------------------------------------------------------------------------------------------------------------------
			SELECT --Cambiar el substrim por un remplace string de jubilado y pensionado a ''
				ARRAY_AGG(upper(substring(TRIM((STRING_TO_ARRAY(dtipo_persona, ' ') )[2] ), 1, 1))), array_length(ARRAY_AGG(upper(substring(TRIM((STRING_TO_ARRAY(dtipo_persona, ' ') )[2] ), 1, 1))), 1), ARRAY_AGG(dtipo_persona), array_length(ARRAY_AGG(dtipo_persona), 1)
			INTO 
				array_cobertura_v, indice_final_cobertura_v, array_tpersona_v, indice_final_tpersona_v
			FROM 
				mirror.nm_jubilados_tjubilados
			WHERE 
				cedula = captura_ccedula_v AND nacionalidad = captura_tnacionalidad_v AND istatus = 'A' LIMIT 1 ;
		--por ahora va Rapido

			FOR indice_cobertura_v IN 1.. indice_final_cobertura_v LOOP

				FOR indice_tpersona_v IN 1.. indice_final_tpersona_v LOOP
					--RAISE NOTICE 'valor: %', array_tpersona_v[indice_tpersona_v];
				
					IF( (STRING_TO_ARRAY(array_tpersona_v[indice_tpersona_v], ' '))[1] = 'Jubilado') THEN
						RAISE NOTICE 'Jubilado';

						SELECT 'J' INTO captura_tipo_pasivo_v;
						
						SELECT 'Jubilado' INTO tpersona_cobertura_v;

					ELSEIF( (STRING_TO_ARRAY(array_tpersona_v[indice_tpersona_v], ' '))[1] = 'Pensionado' AND tpersona_cobertura_v != 'Jubilado') THEN 
						RAISE NOTICE 'Pensionado';

						SELECT 'P' INTO captura_tipo_pasivo_v;
					END IF;

					SELECT 
						trim(to_char(nestado, '00')), ncuenta_banco, sexo
					INTO
						captura_centidad_v, captura_cuenta_banco_v, captura_sexo_v
					FROM 
						mirror.nm_jubilados_tjubilados
					WHERE 
						cedula = captura_ccedula_v AND nacionalidad = captura_tnacionalidad_v AND istatus = 'A' AND dtipo_persona = array_tpersona_v[indice_tpersona_v] LIMIT 1;


				END LOOP;


		--(----------------------------------------------------------------------------------------------------------------------------------------------------
--  			Determinar el id del estado segun el codigo_ini
--  			obtener el id del estado
				SELECT COUNT(1) INTO existencia_co_edo_asap_v FROM catastro.estado WHERE co_edo_asap = captura_centidad_v;
				IF (existencia_co_edo_asap_v = 1) THEN 
					SELECT id, nombre INTO captura_id_estado_v, captura_nombre_estado FROM catastro.estado WHERE co_edo_asap = captura_centidad_v;
				END IF;
		--)----------------------------------------------------------------------------------------------------------------------------------------------------


		--		Obtener el id de la cobertura para seguros:	
				SELECT count(1) INTO existencia_tpersona_v FROM gestion_humana.categoria_cargo_nominal WHERE upper(substring(nombre, 1, 1)) = array_cobertura_v[indice_cobertura_v];	
				IF (existencia_tpersona_v = 1) THEN
		--		Siempre Le da prioridad a Docentes por lo que si pasa por alguna condicion de nivel superior. ignora las inferiores 
		--			Nivel: 3
					IF(array_cobertura_v[indice_cobertura_v] = 'D' ) THEN
						SELECT id, id, nombre INTO captura_cobertura_id_v, captura_categoria_cargo_actual_id_v, captura_tipo_personal_v FROM gestion_humana.categoria_cargo_nominal WHERE upper(substring(nombre, 1, 1)) = array_cobertura_v[indice_cobertura_v];
						SELECT array_cobertura_v[indice_cobertura_v] INTO estado_cobertura_v;

		--			Nivel: 2
					ELSEIF (array_cobertura_v[indice_cobertura_v] =  'O' AND estado_cobertura_v != 'D') THEN
						SELECT id, id, nombre INTO captura_cobertura_id_v, captura_categoria_cargo_actual_id_v, captura_tipo_personal_v FROM gestion_humana.categoria_cargo_nominal WHERE upper(substring(nombre, 1, 1)) = array_cobertura_v[indice_cobertura_v];
						SELECT array_cobertura_v[indice_cobertura_v] INTO estado_cobertura_v;

		--			Nivel: 1
					ELSEIF (array_cobertura_v[indice_cobertura_v] =  'A' AND estado_cobertura_v != 'D' AND estado_cobertura_v != 'O' ) THEN
						SELECT id, id, nombre INTO captura_cobertura_id_v, captura_categoria_cargo_actual_id_v, captura_tipo_personal_v FROM gestion_humana.categoria_cargo_nominal WHERE upper(substring(nombre, 1, 1)) = array_cobertura_v[indice_cobertura_v];
						SELECT array_cobertura_v[indice_cobertura_v] INTO estado_cobertura_v;
					END IF;

				END IF;		
			
			END LOOP;
		ELSE
		RAISE EXCEPTION 'La cedula: "%" en tjubilados no se encuentra activa.', NEW.ccedula;
		END IF;
	ELSE
		RAISE EXCEPTION 'No se pudo encontrar la cedula: "%" en tjubilados', NEW.ccedula;
	END IF;
--)----------------------------------------------------------------------------------------------------------------------------------------------------
	IF (verificado_saime_v = 'Si') THEN
		captura_sexo_v := captura_sexo_saime_v;
	END IF;


--(----------------------------------------------------------------------------------------------------------------------------------------------------
			SELECT COUNT(1) INTO existencia_talento_humano_v FROM gestion_humana.talento_humano WHERE cedula = captura_ccedula_v AND origen = captura_tnacionalidad_v;
			IF (existencia_talento_humano_v = 0) THEN
			
				INSERT INTO gestion_humana.talento_humano 
				(
					origen,
					cedula,
					nombre,
					apellido,
					sexo,
					fecha_nacimiento,
					estado_id,
					fecha_ingreso,
					condicion_actual_id, 
					categoria_cargo_actual_id,
					cargo_actual_id,
					tipo_nomina_id,
					tipo_personal,
					condicion,
					verificado_saime,
					numero_cuenta,
					usuario_ini_id,
					usuario_act_id,
					fecha_ini,
					fecha_act,
					estatus,
					cobertura_id,
					tipo_pasivo,
					fecha_nacimiento_errada,
					estado_dependencia,
					estado_dependencia_id

				) VALUES (
					NEW.tnacionalidad, --origen
					NEW.ccedula, --cedula
					nombre_v, --nombre
					apellido_v, --apellido
					captura_sexo_v, --sexo
					captura_fecha_nacimiento_v, --fecha_nacimiento,
					captura_id_estado_v, --estado_id,
					NEW.fingresoap, --fecha_ingreso,
					condicion_actual_id_v, --condicion_actual_id, 
					captura_categoria_cargo_actual_id_v, --categoria_cargo_actual_id,
					captura_cargo_nominal_id_v, --cargo_actual_id,
					tipo_nomina_id_v, --tipo_nomina_id,
					captura_tipo_personal_v, --tipo_personal,
					condicion_v, --condicion,
					verificado_saime_v, --verificado_saime,
					captura_cuenta_banco_v, --numero_cuenta,
					usuario_id_v, --usuario_ini_id,
					usuario_id_v, --usuario_act_id,
					fecha_actual_v, --fecha_ini,
					fecha_actual_v, --fecha_act,
					captura_istatus_v, --estatus,
					captura_cobertura_id_v, --cobertura_id
					captura_tipo_pasivo_v, --tipo_pasivo
					fecha_nacimiento_errada_v,
					captura_nombre_estado,
					captura_id_estado_v
				);
			ELSE
				UPDATE gestion_humana.talento_humano  
				SET 
					origen = NEW.tnacionalidad, 
					cedula = NEW.ccedula, 
					nombre = nombre_v, 
					apellido = apellido_v,
					sexo = captura_sexo_v,
					fecha_nacimiento = captura_fecha_nacimiento_v,
					fecha_ingreso = NEW.fingresoap,
					condicion_actual_id = condicion_actual_id_v, 
					categoria_cargo_actual_id = captura_categoria_cargo_actual_id_v,
					cargo_actual_id = captura_cargo_nominal_id_v,
					tipo_nomina_id = tipo_nomina_id_v,
					tipo_personal = captura_tipo_personal_v,
					condicion = condicion_v,
					verificado_saime = verificado_saime_v,
					numero_cuenta = captura_cuenta_banco_v,
					usuario_ini_id = usuario_id_v,
					usuario_act_id = usuario_id_v,
					fecha_ini = fecha_actual_v,
					fecha_act_nomina_jubilado = fecha_actual_v,
					estatus = captura_istatus_v,
					cobertura_id = captura_cobertura_id_v,
					fecha_nacimiento_errada = fecha_nacimiento_errada_v,
					tipo_pasivo = captura_tipo_pasivo_v,
					estado_dependencia_id = captura_id_estado_v,
					estado_dependencia = captura_nombre_estado
				 WHERE cedula = captura_ccedula_v AND origen = captura_tnacionalidad_v;
			END IF;
--)----------------------------------------------------------------------------------------------------------------------------------------------------
	END IF;

	PERFORM etls.captura_log_estado(descripcion_v, nombre_ktr_v, nombre_kjb_v, observaciones_v, seccion_v);

	RETURN NEW;

EXCEPTION
    WHEN OTHERS THEN

		seccion_v := 'EXCEPTION';
		observaciones_v := 'Cedula: '||NEW.ccedula||' ('||SQLERRM||')';
		PERFORM etls.captura_log_estado(descripcion_v, nombre_ktr_v, nombre_kjb_v, observaciones_v, seccion_v);

    RETURN NEW;



END;
$BODY$
LANGUAGE plpgsql VOLATILE
COST 100;

/*
CREATE TRIGGER mirror_bus_servicios_tpersona
BEFORE INSERT OR UPDATE
ON mirror.nm_jubilados_tpersona
FOR EACH ROW
EXECUTE PROCEDURE mirror.bus_servicios_tpersona_jubilados();

*/