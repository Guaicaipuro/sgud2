COMMENT ON COLUMN gestion_humana.postulacion.estatus_aprobado
  IS 'Este campo contiene los estados de la postulacion';
ALTER TABLE gestion_humana.postulacion
  ADD COLUMN estatus_anterior_aprobado smallint;
ALTER TABLE gestion_humana.postulacion
  ADD CONSTRAINT th_postulacion_estatus_anterior_aprobado_fk FOREIGN KEY (estatus_anterior_aprobado) REFERENCES gestion_humana.estatus_postulacion (id) ON UPDATE NO ACTION ON DELETE NO ACTION;
COMMENT ON COLUMN gestion_humana.postulacion.estatus_aprobado IS 'Estatus actual aprobado de la postulación';
COMMENT ON COLUMN gestion_humana.postulacion.estatus_anterior_aprobado IS 'Estatus anterior aprobado de la postulación';
