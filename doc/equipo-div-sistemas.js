// https://javascriptobfuscator.com/Javascript-Obfuscator.aspx
$("#desarrolladores-info").on("dblclick", function(){
    $.gritter.add({
        title: 'División de Sistemas del MPPE',
        text: '<b>Líder de Proyecto</b><br/>- José Gabriel González<br/><br/>\n\
               <b>Equipo de Desarrollo</b><br/>- José Gabriel González<br/>- Daniel Ruíz<br/>- Pleiber Rosendo<br/>- Jonathan Cardozo<br/>- Moises Urbano<br/><br/>\n\
               <b>Equipo de Documentación</b><br/>- Robins Rengel<br/>- Aimeth España<br/>- Johanna Páez<br/>- Yurbanis Cedeño<br/><br/>\n\
               <b>Equipo Funcional (Ingreso y Clasificación)</b><br/>- Yimery Soto<br/>- Glimar Guillen<br/>- Analia Ramirez',
        class_name: 'gritter-error',
        time: 15000
    });
});
